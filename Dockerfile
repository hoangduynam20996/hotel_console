# Build stage
FROM node:16-alpine AS BUILD_IMAGE

WORKDIR /app/react-app

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]