import { httpApi } from '@app/api/http.api';

export const getBookings = async (payload: any): Promise<any> => {
  return await httpApi
    .get<any>(
      `/admin/v1/booking?currentPage=${payload.values.currentPage}&limitPage=${payload.values.limitPage}${
        payload.values.bookingId ? `&bookingId=${payload.values.bookingId}` : ''
      }${payload.values.bookingCode ? `&bookingCode=${payload.values.bookingCode}` : ''}${
        payload.values.roomCode ? `&pinCode=${payload.values.roomCode}` : ''
      }${payload.values.checkInDate ? `&checkInDate=${payload.values.checkInDate}` : ''}${
        payload.values.checkOutDate ? `&checkOutDate=${payload.values.checkOutDate}` : ''
      }${payload.values.status ? `&statusBooking=${payload.values.status}` : ''}${
        payload.values.bookingDate ? `&bookingDate=${payload.values.bookingDate}` : ''
      }${payload.values.bookingDateTo ? `&bookingDateTo=${payload.values.bookingDateTo}` : ''}${
        payload.values.hotelName ? `&hotelName=${payload.values.hotelName}` : ''
      }${payload.values.roomName ? `&roomName=${payload.values.roomName}` : ''}${
        payload.values.nameContact ? `&nameContact=${payload.values.nameContact}` : ''
      }${payload.values.phoneNumber ? `&phoneNumber=${payload.values.phoneNumber}` : ''}${
        payload.values.statusPayment ? `&statusPayment=${payload.values.statusPayment}` : ''
      }`,
    )
    .then(({ data }) => data);
  // return await httpApi.get<any>(
  //     `/admin/v1/booking?currentPage=${payload.values.currentPage}&limitPage=${payload.values.limitPage}`
  // )
  //     .then(({ data }) => data)
};

export const getBooking = async (payload: any): Promise<any> => {
  return await httpApi.get<any>(`/member/v1/booking-detail/${payload.values.bookingId}`).then(({ data }) => data);
};

export const changeStatusBooking = async (payload: any): Promise<any> => {
  return await httpApi
    .put<any>(`/admin/v1/booking/${payload.values.bookingId}/${payload.values.status}`)
    .then(({ data }) => data);
};

export const changeStatusPayment = async (payload: any): Promise<any> => {
  return await httpApi
    .put<any>(`/admin/v1/payment/${payload.values.bookingId}/${payload.values.statusChange}`)
    .then(({ data }) => data);
};
