import { httpApi } from './http.api';
import { PermissionGroupModel } from '@app/domain/UserModel';

export interface PermissionGroupsRequest {
  token: string;
}

export interface PermissionGroupsResponse {
  groups: PermissionGroupModel[];
}

export const getPermissionGroups = async (payload: any): Promise<PermissionGroupsResponse | any> => {
  return await httpApi
    .get<PermissionGroupsResponse>(`/admin/v1/group?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}`)
    .then(({ data }) => data);
};

export const addUserForGroups = async (payload: any): Promise<PermissionGroupsResponse | any> => {
  return await httpApi.post<PermissionGroupsResponse>('/admin/v1/group/user', payload.values).then(({ data }) => data);
};

export const addPermissionGroup = (data: any): Promise<any> =>
  httpApi
    .post('/admin/v1/group', data)
    .then(({ data }) => data)
    .catch((err) => {
      return err;
    });
