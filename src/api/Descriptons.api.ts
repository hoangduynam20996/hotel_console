import { languages } from '@app/constants/languages';
import { httpApi } from './http.api';
import { DescriptionModel } from '@app/domain/DescriptionModel';
import axios, { AxiosRequestConfig } from 'axios';

export interface DescriptionsReponses {
  descriptions: DescriptionModel[];
}
export interface DescriptionRequest {
  languageCode: string;
  parentId: string;
}

export interface DescriptionsReponse {
  description: DescriptionModel;
}

export const getDescription = async (payload: any): Promise<DescriptionsReponses | any> => {
  const lang = localStorage.getItem('lng');
  const config: AxiosRequestConfig = {
    headers: {
      language: `${lang}`,
    },
  };
  return await httpApi
    .get<DescriptionsReponses>(
      `/admin/v1/description/?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}`,
      config,
    )
    .then(({ data }) => data);
};

export const getDescriptionByParent = async (payload: any): Promise<DescriptionsReponse | any> => {
  return await httpApi
    .get<DescriptionsReponse>(`/admin/v1/description/des/${payload?.parentId}?language=${payload.languageCode}`)
    .then(({ data }) => data);
};

export const creatDescription = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/description/`, payload).then(({ data }) => data);
};

export const UpdateDescription = async (payload: any): Promise<DescriptionsReponse | any> => {
  return await httpApi
    .put<DescriptionsReponse>(`/admin/v1/description/${payload?.parentId}`, payload)
    .then(({ data }) => data);
};

export const DeleteDescription = (payload: any): Promise<DescriptionsReponse | any> =>
  httpApi.delete(`/admin/v1/description/${payload.id}`).then(({ data }) => data);

export const changStatusDes = async (payload: any): Promise<any> => {
  console.log('aaaaaaaaa', payload);
  httpApi.put(`/admin/v1/description/change/${payload.values.parentId}`, payload.values).then(({ data }) => data);
};
