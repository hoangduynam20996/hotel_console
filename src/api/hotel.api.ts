import { httpApi } from '@app/api/http.api';
import { HotelModel } from '@app/domain/HotelModel';
import axios, { AxiosRequestConfig } from 'axios';
export interface HotelRequest {
  id: string;
  languageCode: string;
}

export interface HotelResponse {
  hotel: HotelModel;
}

export interface HotelsResponse {
  hotels: HotelModel[];
}

export const getHotels = async (payload: any): Promise<HotelsResponse | any> => {
  return await httpApi
    .get<HotelsResponse>(
      `/admin/v1/hotel?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}&status=${payload.status}`,
    )
    .then(({ data }) => data);
};

export const getHotel = async (payload: HotelRequest): Promise<HotelResponse | any> => {
  return await httpApi.get<HotelResponse>(`/admin/v1/hotel/${payload?.hotelId}`).then(({ data }) => data);
};

export const getHotelContact = async (payload: any): Promise<any> => {
  return await httpApi.get(`/admin/v1/hotel-contact/${payload.values.id}`).then(({ data }) => data);
};

export const changeHotelStatus = async (payload: any): Promise<any> => {
  return await httpApi
    .put(`/admin/v1/hotel/${payload.values.hotelId}/${payload.values.status}`)
    .then(({ data }) => data);
};
export const getServicesAmenity = async (payload: any): Promise<any> => {
  return await httpApi.get(`/admin/v1/service-amenity`).then(({ data }) => data);
};

export const getHotelTypes = async (payload: any): Promise<any> => {
  return await httpApi.get(`/admin/v1/hotel-type?status=ACTIVE&currentPage=1&limitPage=8`).then(({ data }) => data);
};

export const getHotelImages = async (payload: any): Promise<any> => {
  return await httpApi
    .get<any>(
      `/admin/v1/hotel-image/${payload.values.hotelId}?currentPage=${payload.values.currentPage}&limitPage=${payload.values.limitPage}`,
    )
    .then(({ data }) => data);
};

export const createHotel = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/hotel`, payload.values).then(({ data }) => data);
};

export const createHotelContact = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/hotel-contact`, payload.values).then(({ data }) => data);
};

export const updateHotel = async (payload: any): Promise<any> => {
  console.log(payload.values);

  return await httpApi.put(`/admin/v1/hotel/update-owner`, payload.values).then(({ data }) => data);
};

export const updateHotelContact = async (payload: any): Promise<any> => {
  return await httpApi.put(`/admin/v1/hotel-contact/${payload.values.id}`, payload.values).then(({ data }) => data);
};

export const addHotelInfo = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/hotel/${payload.values.id}/info`, payload.values).then(({ data }) => data);
};

export const getProvinces = async (payload: any): Promise<any> => {
  return await httpApi.get(`/public/v1/province`).then(({ data }) => data);
};
