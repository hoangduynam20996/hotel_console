import { httpApi } from '@app/api/http.api';

export interface ImageRequest {
  targetId: string;
  images: string[];
}

export const uploadImages = async (payload: ImageRequest): Promise<any> => {
  return await httpApi.post(`/admin/v1/hotel-image/cms`, payload).then(({ data }) => data);
};

export const deleteImage = async (id: string): Promise<any> => {
  return await httpApi.delete(`/admin/v1/hotel-image/${id}`).then(({ data }) => data);
};

export const getHotelImage = async (payload: any): Promise<any> => {
  return await httpApi
    .get<any>(
      `/admin/v1/hotel-image/${payload.values.hotelId}?currentPage=${payload.values.currentPage}&limitPage=${payload.values.limitPage}`,
    )
    .then(({ data }) => data);
};
