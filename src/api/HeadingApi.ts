import { httpApi } from '@app/api/http.api';

import { HeadingModel } from '@app/domain/HeadingModel';
import axios, { AxiosRequestConfig } from 'axios';

export interface HeadingRequest {
  parentId: string;
  languageCode: string;
}

export interface HeadingResponse {
  headings: HeadingModel[];
}

export interface HeadingResponses {
  heading: HeadingModel;
}

export const getHeadings = async (payload: any): Promise<HeadingResponse | any> => {
  const lang = localStorage.getItem('lng');

  const config: AxiosRequestConfig = {
    headers: {
      language: `${lang}`,
    },
  };
  return await httpApi
    .get<HeadingResponse>(
      `/admin/v1/heading/?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}`,
      config,
    )
    .then(({ data }) => data);
};

export const deleteHeadings = (payload: any): Promise<HeadingResponses | any> =>
  httpApi.delete<HeadingResponses>(`/admin/v1/heading/${payload.id}`).then(({ data }) => data);

export const updateHeadings = async (payload: any): Promise<any> => {
  return await httpApi.put(`/admin/v1/heading/${payload?.parentId}`, payload).then(({ data }) => data);
};

export const createHeadings = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/heading/`, payload).then(({ data }) => data);
};

export const getHeadingParent = async (payload: HeadingRequest): Promise<HeadingResponses | any> => {
  return await httpApi
    .get<HeadingResponses>(`/admin/v1/heading/parent/${payload?.parentId}?language=${payload.languageCode}`)
    .then(({ data }) => data);
};

export const changeStatus = async (payload: any): Promise<HeadingResponse | any> => {
  console.log('aaaaaaaaa', payload);
  return await httpApi
    .put(`/admin/v1/heading/change/${payload.values.parentId}`, payload.values)
    .then(({ data }) => data);
};
