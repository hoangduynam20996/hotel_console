import { httpApi } from '@app/api/http.api';
import { RoomModel } from '@app/domain/RoomModel';

export interface RoomRequest {
  values: Values;
}

export interface Values {
  roomId: string;
  hotelId: string;
  languageCode: string;
  roomName: string;
  currentPage: number;
  limitPage: number;
}

export interface RoomsResponse {
  rooms: RoomModel[];
}

export interface RoomResponse {
  room: RoomModel;
}

export const getRooms = async (payload: RoomRequest): Promise<RoomsResponse | any> => {
  return await httpApi
    .get<RoomsResponse>(
      `/admin/v1/room?hid=${payload.values.hotelId}${
        payload.values.roomName ? `&roomName=${payload.values.roomName}` : ''
      }&status=${payload.values.status}&currentPage=${payload.values.currentPage}&limitPage=${
        payload.values.limitPage
      }`,
    )
    .then(({ data }) => data);
};

export const getRoom = async (payload: any): Promise<RoomResponse | any> => {
  return await httpApi.get<RoomResponse>(`/admin/v1/room/${payload.values.roomId}`).then(({ data }) => data);
};

export const getRoomTypes = async (payload: any): Promise<any | any> => {
  return await httpApi.get<any>(`/admin/v1/room-type`).then(({ data }) => data);
};

export const getRoomServiceMain = async (payload: any): Promise<any | any> => {
  return await httpApi.get<any>(`/admin/v1/service-amenity`).then(({ data }) => data);
};

export const getRoomServiceAmenityRoom = async (payload: any): Promise<any | any> => {
  return await httpApi.get<any>(`/admin/v1/service-amenity-room/${payload.values.roomId}`).then(({ data }) => data);
};

export const getRoomServiceSub = async (payload: any): Promise<any | any> => {
  return await httpApi.get<any>(`/admin/v1/room-service?type=SUB`).then(({ data }) => data);
};

export const getPolicyByRoomId = async (payload: any): Promise<any | any> => {
  return await httpApi.get<any>(`/admin/v1/room-policy?roomId=${payload.values.roomId}`).then(({ data }) => data);
};

export const createRoom = async (payload: any): Promise<RoomResponse | any> => {
  return await httpApi.post<RoomResponse>(`/admin/v1/room/cms`, payload.values).then(({ data }) => data);
};

export const createRoomPolicy = async (payload: any): Promise<any | any> => {
  return await httpApi.post<any>(`/admin/v1/room-policy`, payload.values).then(({ data }) => data);
};

export const updateServiceAmenityRoom = async (payload: any): Promise<RoomResponse | any> => {
  return await httpApi
    .put<RoomResponse>(`/admin/v1/service-amenity-room/cms/${payload.values.roomId}`, payload.values)
    .then(({ data }) => data);
};

export const updateRoom = async (payload: RoomRequest): Promise<RoomResponse | any> => {
  return await httpApi
    .put<RoomResponse>(`/admin/v1/room/${payload.values.roomId}`, payload.values)
    .then(({ data }) => data);
};

export const updateRoomPolicy = async (payload: any): Promise<any | any> => {
  return await httpApi.put<any>(`/admin/v1/room-policy`, payload.values).then(({ data }) => data);
};

export const deleteRoom = async (payload: RoomRequest): Promise<RoomResponse | any> => {
  return await httpApi.delete<RoomResponse>(`/admin/v1/room/${payload.values.roomId}`).then(({ data }) => data);
};

export const changeRoomStatus = async (payload: RoomRequest): Promise<RoomResponse | any> => {
  return await httpApi.put(`/admin/v1/room/${payload.values.roomId}`, payload.values).then(({ data }) => data);
};
