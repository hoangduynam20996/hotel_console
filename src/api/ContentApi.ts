import { httpApi } from '@app/api/http.api';
import { TitleContentModel } from '@app/domain/TitleContentModel';
import axios, { AxiosRequestConfig } from 'axios';

export interface ContentRequest {
  parentId: string;
  languageCode: string;
}

export interface ContentReponses {
  contents: TitleContentModel[];
}

export interface ContentResponse {
  content: TitleContentModel[];
}

export const getContents = async (payload: any): Promise<ContentResponse | any> => {
  const lang = localStorage.getItem('lng');
  const config: AxiosRequestConfig = {
    headers: {
      language: `${lang}`,
    },
  };
  return await httpApi
    .get<ContentReponses>(
      `/admin/v1/content/?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}`,
      config,
    )
    .then(({ data }) => data);
};

export const getContentByParent = async (payload: ContentRequest): Promise<ContentResponse | any> => {
  console.log(payload);
  return await httpApi
    .get<ContentResponse>(`/admin/v1/content/content/${payload?.parentId}?language=${payload.languageCode}`)
    .then(({ data }) => data);
};
export const deleteContent = async (payload: any): Promise<any> => {
  return await httpApi.delete<ContentResponse>(`/admin/v1/content/${payload.id}`).then(({ data }) => data);
};

export const updateContents = async (payload: any): Promise<any> => {
  return await httpApi.put(`/admin/v1/content/${payload?.parentId}`, payload).then(({ data }) => data);
};

export const createContent = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/content/`, payload).then(({ data }) => data);
};

export const changStatusContent = async (payload: any): Promise<any> => {
  console.log('aaaaaaaa', payload);
  return await httpApi
    .put(`/admin/v1/content/change/${payload.values.parentId}`, payload.values)
    .then(({ data }) => data);
};
