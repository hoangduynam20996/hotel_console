import { httpApi } from './http.api';
import { GuestRequestModel } from '@app/domain/GuestRequestModel';

export interface GuestRequest {
  id: string;
}

export interface GuestResponse {
  guest: GuestRequestModel;
}

export interface GuestRequestsResponse {
  guests: GuestRequestModel[];
}

export const getGuestRequests = (request: any): Promise<GuestRequestsResponse | any> =>
  httpApi
    .get<GuestRequestsResponse>(`/admin/v1/guest?currentPage=${request.currentPage}&limitPage=${request.limitPage}`)
    .then(({ data }) => data);

export const getGuestRequest = (request: any): Promise<GuestResponse | any> =>
  httpApi.get<GuestResponse>(`/admin/v1/guest/${request.id}`).then(({ data }) => data);

export const updateStatusGuestRequest = (request: any): Promise<GuestResponse | any> =>
  httpApi.put<GuestResponse>(`/admin/v1/guest/${request.id}`, request).then(({ data }) => data);

export const deleteGuestRequest = (request: any): Promise<GuestResponse | any> =>
  httpApi.delete<GuestResponse>(`/admin/v1/guest/${request.id}`).then(({ data }) => data);
