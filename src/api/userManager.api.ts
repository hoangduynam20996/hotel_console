import { httpApi } from './http.api';
import { UserModel } from '@app/domain/UserModel';

export interface UsersRequest {
  token: string;
}

export interface UsersResponse {
  users: UserModel[];
}

export interface UserResponse {
  user: UserModel;
}

export const getUsers = (payload: any): Promise<UsersResponse | any> =>
  httpApi
    .get<UsersResponse>(
      `/admin/v1/user?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}${
        payload.status ? `&status=${payload.status}` : ''
      }${payload.isHotelOwner ? `&isHotelOwner=${payload.isHotelOwner === 'ACTIVE' ? true : false}` : ''}`,
    )
    .then(({ data }) => data);

export const getUser = (payload: any): Promise<UserResponse | any> =>
  httpApi.get<UserResponse>(`/admin/v1/user/${payload.id}`).then(({ data }) => data);

export const addUser = (payload: any): Promise<UserResponse | any> =>
  httpApi.post<UserResponse>(`/public/v1/user/register/${false}`, payload.values).then(({ data }) => data);

export const activeUser = (payload: any): Promise<UserResponse | any> =>
  httpApi.post<UserResponse>(`/admin/v1/user/${payload.id}`).then(({ data }) => data);

export const deleteUser = (payload: any): Promise<UserResponse | any> =>
  httpApi.delete<UserResponse>(`/admin/v1/user/${payload.id}`).then(({ data }) => data);

export const updateUser = (payload: any): Promise<UserResponse | any> =>
  httpApi.put<UserResponse>(`/admin/v1/user`, payload.values).then(({ data }) => data);
