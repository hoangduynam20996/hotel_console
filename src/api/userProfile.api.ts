import axios from 'axios';
import { UserModel } from '@app/domain/UserModel';
import { httpApi } from './http.api';

export interface ProfileRequest {
  token: string;
}

export interface ProfileResponse {
  user: UserModel;
}

export const getProfileUser = (): Promise<ProfileResponse | any> =>
  httpApi.get<ProfileResponse>('/member/v1/user/profile').then(({ data }) => data);

export const getUserPermission = (): Promise<any> =>
  httpApi.get<ProfileResponse>('/member/v1/user/permissions').then(({ data }) => data);
