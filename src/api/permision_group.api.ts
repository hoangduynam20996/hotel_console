import { httpApi } from '@app/api/http.api';
// import './mocks/auth.api.mock';
import { UserModel } from '@app/domain/UserModel';

export interface AuthData {
  email: string;
  password: string;
}

export interface SignUpRequest {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface ResetPasswordRequest {
  email: string;
}

export interface SecurityCodePayload {
  code: string;
}

export interface NewPasswordData {
  newPassword: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface LoginResponse {
  user: UserModel;
  data: {
    jwtToken: string;
  };
}

export const getPermissionGroup = (): Promise<undefined> =>
  httpApi
    .get(`/admin/v1/group`)
    .then(({ data }) => data)
    .catch((err) => {
      return err;
    });

export const addPermissionGroup = (data: any): Promise<undefined> =>
  httpApi
    .post('/admin/v1/group', data)
    .then(({ data }) => data)
    .catch((err) => {
      return err;
    });
