import { httpApi } from '@app/api/http.api';

export interface BebRoomRequest {
  values: Values;
}

export interface Values {
  id: string;
}

export interface BebRoomsResponse {
  bebtypes: [];
}

export const getBebTypes = async (payload: any): Promise<BebRoomsResponse | any> => {
  return await httpApi.get<BebRoomsResponse>(`/admin/v1/beb-type`).then(({ data }) => data);
};
