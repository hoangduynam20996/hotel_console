import axios from 'axios';
import { UserModel } from '@app/domain/UserModel';
import { httpApi } from './http.api';

export interface ProfileRequest {
  token: string;
}

export interface Root {
  code: string;
  status: number;
  message: string;
  responseTime: number;
  data: Data;
}

export interface Data {
  userId: string;
  email: string;
  gender: string;
  dateOfBirth: string;
  fullName: string;
  phoneNumber: string;
  address: string;
  isDeleted: boolean;
  jwtToken: string;
  roles: Role[];
  permissions: Permission[];
  permissionGroups: PermissionGroup[];
}

export interface Role {
  roleId: string;
  roleName: string;
  roleCode: string;
  isDeleted: boolean;
  permissions: any;
}

export interface Permission {
  permissionId: string;
  permissionCode: string;
  isDeleted: boolean;
  description: string;
}

export interface PermissionGroup {
  permissionGroupId: string;
  permissionGroupName: string;
  isDeleted: boolean;
  description: string;
  permissions: any;
}

export interface groupPermissionRequest {
  groupId: string;
  permissionIds: PermissionCode[];
}

export interface PermissionCode {
  permissionCode: string;
}

export const getPermissionGroup = (): Promise<PermissionGroup | any> =>
  httpApi.get<PermissionGroup>('/admin/v1/permission-group').then(({ data }) => data);

export const getAllPermission = (data: any): Promise<PermissionGroup | any> =>
  httpApi
    .get<PermissionGroup>(`/admin/v1/permission?currentPage=${data.currentPage}&limitPage=${data.pageSize}`)
    .then(({ data }) => data);

export const addPermission = (request: any): Promise<PermissionGroup | any> =>
  httpApi.put<PermissionGroup>('/admin/v1/group/permissions', request).then(({ data }) => data);

export const getPermissionGroupById = (id: string): Promise<PermissionGroup | any> =>
  httpApi.get<PermissionGroup>(`/admin/v1/group/${id}`).then(({ data }) => data);
