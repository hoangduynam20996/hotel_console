import axios from 'axios';
import { AxiosError } from 'axios';
import { ApiError } from '@app/api/ApiError';
import { readToken } from '@app/services/localStorage.service';

let isApiCalledByAction = false;

export const setIsApiCalledByAction = (value: boolean) => {
  isApiCalledByAction = value;
};

export const httpApi = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

httpApi.interceptors.request.use((config) => {
  config.headers = { ...config.headers, Authorization: `Bearer ${readToken()}`, 'Content-Type': 'application/json' };
  return config;
});

httpApi.interceptors.response.use(undefined, (error: AxiosError) => {
  if (error.response?.status === 401) {
    localStorage.clear();
    window.location.href = '/auth/login';
  }
  if (error.response?.status === 403 && !isApiCalledByAction) {
    window.location.href = '/forbidden';
  }
  throw new ApiError<ApiErrorData>(error.response?.data.message || error.message, error.response?.data);
});

export interface ApiErrorData {
  message: string;
}
