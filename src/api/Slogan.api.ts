import { SloganModel } from '@app/domain/sloganModel';
import { httpApi } from './http.api';
import axios, { AxiosRequestConfig } from 'axios';

export interface SloganRequest {
  parentId: string;
  languageCode: string;
}
export interface SloganReponses {
  slogans: SloganModel[];
}
export interface SloganReponse {
  slogan: SloganModel;
}

export const getSlogans = async (payload: any): Promise<SloganReponses | any> => {
  const lang = localStorage.getItem('lng');
  const config: AxiosRequestConfig = {
    headers: {
      language: `${lang}`,
    },
  };
  return await httpApi
    .get(`/admin/v1/slogan/?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}`, config)
    .then(({ data }) => data);
};

export const getSloganParent = async (payload: SloganRequest): Promise<SloganReponse | any> => {
  console.log(payload);
  return await httpApi
    .get(`/admin/v1/slogan/slogan/${payload?.parentId}?language=${payload.languageCode}`)
    .then(({ data }) => data);
};

export const createSlogan = async (payload: any): Promise<SloganReponses | any> => {
  return await httpApi.post(`/admin/v1/slogan/`, payload).then(({ data }) => data);
};

export const updateSlogan = async (payload: any): Promise<SloganReponse | any> => {
  return await httpApi.put(`/admin/v1/slogan/${payload.parentId}`, payload).then(({ data }) => data);
};

export const deleteSlogan = async (payload: any): Promise<any> => {
  return await httpApi.delete(`/admin/v1/slogan/${payload.id}`).then(({ data }) => data);
};

export const changeStatusSlogan = async (payload: any): Promise<any> => {
  return httpApi.put(`/admin/v1/slogan/change/${payload.values.parentId}`, payload.values).then(({ data }) => data);
};
