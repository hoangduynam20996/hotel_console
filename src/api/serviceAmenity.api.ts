import { httpApi } from '@app/api/http.api';

export const getServicesAmenity = async (payload: any): Promise<any> => {
  return await httpApi.get(`/admin/v1/service-amenity`).then(({ data }) => data);
};

export const createServicesAmenity = async (payload: any): Promise<any> => {
  return await httpApi.post(`/admin/v1/service-amenity`, payload.values).then(({ data }) => data);
};

export const updateServicesAmenity = async (payload: any): Promise<any> => {
  return await httpApi
    .put(`/admin/v1/service-amenity/${payload.values.serviceAmenityId}`, payload.values)
    .then(({ data }) => data);
};

export const deleteServicesAmenity = async (payload: any): Promise<any> => {
  return await httpApi.delete(`/admin/v1/service-amenity/${payload.values.serviceAmenityId}`).then(({ data }) => data);
};
