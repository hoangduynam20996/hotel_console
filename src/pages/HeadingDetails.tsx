import { httpApi } from '@app/api/http.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { doCreateHeadings, doGetHeading, doGetHeadings, doUpdateHeadings } from '@app/store/slices/Headingslice';
import { Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useNavigate, useParams } from 'react-router-dom';

interface HeadingDetailPops {}

interface FileItem {
  uid: string | number;
  title: string;
  url: string;
  name: string;
}

type FieldType = {
  urlHeadingVI?: string;
  urlHeadingEN?: string;
  languageCodeVI?: string; // Language code for Vietnamese
  languageCodeEN?: string; // Language code for English
  titleHeadingVI?: string;
  titleHeadingEN?: string;
  isDeleted?: boolean;
  currency?: string;
  createdBy?: string;
  lastModifiedBy?: string;
  createdDate?: string;
  lastModifiedDate?: string;
};

export const HeadingDetails: React.FC<HeadingDetailPops> = (props) => {
  const { parentId } = useParams();
  const [form] = Form.useForm();
  const nivagate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { heading, languageCode } = useSelector((state: any) => state.heading);

  React.useEffect(() => {
    if (parentId) {
      dispatch(doGetHeading({ parentId, languageCode, t }));
    } else {
      form.resetFields();
    }
  }, [dispatch, parentId]);

  React.useEffect(() => {
    if (heading && parentId) {
      heading.map((item: any) => {
        item.languageCode === 'VI'
          ? form.setFieldsValue({
              urlHeadingVI: item.urlHeading,
              languageCodeVI: item.languageCode,
              titleHeadingVI: item.titleHeading,
            })
          : form.setFieldsValue({
              urlHeadingEN: item.urlHeading,
              languageCodeEN: item.languageCode,
              titleHeadingEN: item.titleHeading,
            });
      });
      console.log('record', heading);
    }
  }, [heading, form]);

  const onFinish = async (values: any) => {
    console.log(values);
    const payload = {
      languageRequests: [
        {
          languageCode: 'VI',
          urlHeading: values.urlHeadingVI,
          titleHeading: values.titleHeadingVI,
        },

        {
          languageCode: 'EN',
          urlHeading: values.urlHeadingEN,
          titleHeading: values.titleHeadingEN,
        },
      ],
    };

    if (parentId) {
      await dispatch(doUpdateHeadings({ parentId, ...payload, t }));
    } else {
      await dispatch(doCreateHeadings({ ...payload, t }));
    }

    await nivagate('/manage/headings');
  };

  return (
    <>
      <div className="flex justify-between item-center uppercase">
        <h3 className="text-2xl font-bold">{t('heading_details.heading')}</h3>
        <Link to="/manage/headings" className="text-blue-500">
          {t('heading_details.back')}
        </Link>
      </div>
      <br />
      <Form
        name="basic"
        form={form}
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        className="uppercase"
      >
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('heading_details.language')}
            name="languageCodeVI"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="Việt Nam"
          >
            <Input defaultValue="VI" disabled />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Language"
            name="languageCodeEN"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="English"
          >
            <Input defaultValue="EN" disabled />
          </Form.Item>
        </div>
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('heading_details.title_headingdl')}
            name="titleHeadingVI"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Title Heading"
            name="titleHeadingEN"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
        </div>

        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('heading_details.url_heading')}
            name="urlHeadingVI"
            rules={[{ required: true, message: 'Please input link page!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Link"
            name="urlHeadingEN"
            rules={[{ required: true, message: 'Please input link page!' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <Form.Item>
          <BaseButton type="primary" htmlType="submit" className="uppercase">
            {parentId ? t('heading_details.button.update') : t('heading_details.button.create')}
          </BaseButton>
        </Form.Item>
      </Form>
    </>
  );
};

export default HeadingDetails;
