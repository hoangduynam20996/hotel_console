import React, { useEffect, useState } from 'react';
import { Input, Checkbox, Table, Modal, Switch, Form } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { doChangStatusContent, doDeleteContents, doGetContents } from '@app/store/slices/ContentSlice';
import { Link } from 'react-router-dom';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

type ContentPops = {};

const titleContent: React.FC<ContentPops> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [data, setData] = React.useState<any>([]);

  const lang = localStorage.getItem('lng');
  const { contents, loading, totalPages, currentPage, limitPage } = useSelector((state: any) => state.content);

  const handleDelete = (item: any) => {
    console.log(' parentId: item.parentId : ', item?.id);
    dispatch(doDeleteContents({ id: item?.id, t }));
    setData(contents);
  };

  React.useEffect(() => {
    dispatch(
      doGetContents({
        currentPage,
        limitPage,
        t,
      }),
    );
  }, [dispatch, currentPage, limitPage, lang]);

  React.useEffect(() => {
    const result = contents.map((item: any, index: number) => {
      return {
        index: -(-(currentPage - 1) * limitPage - (index + 1)),
        id: item.parentId,
        key: item.contentId,
        createdBy: item.createdBy,
        descriptionContent: item.descriptionContent,
        languageCode: item.languageCode,
        headingContent: item.headingContent,
        lastModifiedBy: item.lastModifiedBy,
        createdDateL: item.createdDate,
        titleContent: item.titleContent,
        status: item.status,
        edit: false,
      };
    });
    setData(result);
  }, [contents]);

  const columns = [
    {
      title: t('titleContent.content_heading'),
      dataIndex: 'headingContent',
      key: 'headingContent',
    },
    {
      title: t('titleContent.title'),
      dataIndex: 'titleContent',
      key: 'titleContent',
    },

    {
      title: t('titleContent.language'),
      dataIndex: 'languageCode',
      key: 'languageCode',
    },
    {
      title: t('titleContent.description'),
      dataIndex: 'descriptionContent',
      key: 'descriptionContent',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text: string, record: any) => (
        <div className="flex justify-between">
          {record.status === 'ACTIVE' ? (
            <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}

          <Switch
            className={`ml-2 ${record.status === 'ACTIVE' ? 'bg-blue-500' : 'bg-gray-500'}`}
            checked={record.status === 'ACTIVE' ? true : false}
            loading={loading}
            onChange={(e) => {
              dispatch(
                doChangStatusContent({
                  values: {
                    parentId: record.id,
                    status: e ? 'ACTIVE' : 'UN_ACTIVE',
                  },
                  t,
                }),
              );
            }}
          />
        </div>
      ),
    },

    {
      title: t('titleContent.funciton'),
      dataIndex: 'function',
      key: 'function',
      render: (value: string, index: any) => (
        <div className="flex justify-center items-center gap-4">
          <PermissionChecker hasPermissions={['content_update']}>
            <Link to={`/manage/content/content/${index.id}`} type="ghost">
              <BaseButton type="ghost">
                <EditOutlined />
              </BaseButton>
            </Link>
          </PermissionChecker>
          <PermissionChecker hasPermissions={['content_delete']}>
            <BaseButton
              type="primary"
              htmlType="submit"
              onClick={() => {
                handleDelete(index);
              }}
            >
              <DeleteOutlined />
            </BaseButton>
          </PermissionChecker>
        </div>
      ),
    },
  ];
  return (
    <div className="uppercase">
      <h3 className="text-2xl font-bold capitalize">{t('titleContent.content')}</h3>
      <br />
      <div className="flex flex-wrap md:flex-nowrap items-center">
        <div className="flex flex-wrap md:flex-nowrap items-center w-full gap-4">
          <div className="w-full flex flex-nowrap gap-5">
            <PermissionChecker hasPermissions={['content_write']}>
              <div className="ml-auto">
                <Link to={'/manage/content/add/'}>
                  <BaseButton type="primary">{t('titleContent.button.addcontent')}</BaseButton>
                </Link>
              </div>
            </PermissionChecker>
          </div>
        </div>
      </div>
      <Table
        dataSource={data}
        columns={columns}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(doGetContents({ currentPage: page, limitPage: pageSize, t: t }));
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
    </div>
  );
};
export default titleContent;
