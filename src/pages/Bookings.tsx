import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { doGetBookings } from '@app/store/slices/bookingSlice';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate, useParams, useLocation } from 'react-router-dom';
import BookingList from './uiComponentsPages/dataDisplay/BookingList';
import FilterBookingPage from './uiComponentsPages/forms/FilterBookingPage';
import DetailBooking from './uiComponentsPages/dataDisplay/DetailBooking';

export const Bookings: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { currentPage, limitPage } = useSelector((state: any) => state.booking);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);

  const params: Record<string, string> = {};
  searchParams.forEach((value, key) => {
    params[key] = value;
  });

  useEffect(() => {
    dispatch(
      doGetBookings({
        values: {
          bookingId: params.bookingId,
          bookingCode: params.bookingCode,
          roomCode: params.roomCode,
          roomName: params.roomName,
          checkInDate: params.checkInDate,
          checkOutDate: params.checkOutDate,
          status: params.status,
          bookingDate: params.bookingDate,
          bookingDateTo: params.bookingDateTo,
          hotelName: params.hotelName,
          nameContact: params.nameContact,
          phoneNumber: params.phoneNumber,
          statusPayment: params.statusPayment,
          currentPage,
          limitPage,
        },
      }),
    );
  }, [
    dispatch,
    currentPage,
    limitPage,
    params.bookingId,
    params.bookingCode,
    params.roomCode,
    params.phoneNumber,
    params.roomName,
    params.checkInDate,
    params.checkOutDate,
    params.status,
    params.bookingDate,
    params.bookingDateTo,
    params.hotelName,
    params.nameContact,
    params.statusPayment,
  ]);

  return (
    <>
      <h3 className="text-2xl font-bold capitalize">Booking List</h3>
      <div className="flex flex-wrap md:flex-nowrap items-center my-3">
        <div className="flex flex-wrap items-center w-full">
          <FilterBookingPage params={params} />
        </div>
      </div>
      <BookingList />
    </>
  );
};

export default Bookings;
