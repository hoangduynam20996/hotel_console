import { doChangeStatus, doDeleteSlogan, doGetSlogan } from '@app/store/slices/sloganSlice';
import React, { useTransition } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Switch, Table } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Link } from 'react-router-dom';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { doDeleteContents } from '@app/store/slices/ContentSlice';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

type SloganProp = {};

const Slogan: React.FC<SloganProp> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [data, setData] = React.useState<any>([]);
  const { slogans, loading, totalPages, currentPage, limitPage } = useSelector((state: any) => state.slogan);

  const lang = localStorage.getItem('lng');
  React.useEffect(() => {
    dispatch(
      doGetSlogan({
        currentPage,
        limitPage,
        t,
      }),
    );
  }, [dispatch, currentPage, limitPage, lang]);

  const handleDelete = (item: any) => {
    dispatch(doDeleteSlogan({ id: item?.id, t }));
    setData(slogans);
  };

  React.useEffect(() => {
    const load = slogans.map((item: any, index: number) => {
      return {
        index: -(-(currentPage - 1) * limitPage - (index + 1)),
        id: item.parentId,
        languageCode: item.languageCode,
        titleSlogan: item.titleSlogan,
        quote: item.quote,
        createdBy: item.createdBy,
        lastModifiedBy: item.lastModifiedBy,
        createdDate: item.createdDatem,
        lastModifiedDate: item.lastModifiedDate,
        status: item.status,
        edit: false,
      };
    });
    setData(load);
  }, [slogans]);

  const columns = [
    {
      title: t('slogan.quote'),
      dataIndex: 'quote',
      key: 'quote',
    },

    {
      title: t('slogan.language'),
      dataIndex: 'languageCode',
      key: 'languageCode',
    },

    {
      title: t('slogan.tiltle_slogan'),
      dataIndex: 'titleSlogan',
      key: 'titleSlogan',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text: string, record: any) => (
        <div className="flex justify-between">
          {record.status === 'ACTIVE' ? (
            <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}

          <Switch
            className={`ml-2 ${record.status === 'ACTIVE' ? 'bg-blue-500' : 'bg-gray-500'}`}
            checked={record.status === 'ACTIVE' ? true : false}
            loading={loading}
            onChange={(e) => {
              dispatch(
                doChangeStatus({
                  values: {
                    parentId: record.id,
                    status: e ? 'ACTIVE' : 'UN_ACTIVE',
                  },
                  t,
                }),
              );
            }}
          />
        </div>
      ),
    },

    {
      title: t('slogan.funciton'),
      dataIndex: 'function',
      key: 'function',
      render: (value: string, index: any) => (
        <div className="flex justify-center items-center gap-4">
          <PermissionChecker hasPermissions={['content_update']}>
            <Link to={`/manage/slogan/${index.id}`} type="ghost">
              <BaseButton type="ghost">
                <EditOutlined />
              </BaseButton>
            </Link>
          </PermissionChecker>
          <PermissionChecker hasPermissions={['content_delete']}>
            <BaseButton
              type="primary"
              htmlType="submit"
              onClick={() => {
                handleDelete(index);
              }}
            >
              <DeleteOutlined />
            </BaseButton>
          </PermissionChecker>
        </div>
      ),
    },
  ];
  return (
    <div className="uppercase">
      <h3 className="text-2xl font-bold capitalize">{t('slogan.slogann')}</h3>
      <br />
      <div className="flex flex-wrap md:flex-nowrap items-center">
        <div className="flex flex-wrap md:flex-nowrap items-center w-full gap-4">
          <div className="w-full flex flex-nowrap gap-5">
            <PermissionChecker hasPermissions={['content_write']}>
              <div className="ml-auto">
                <Link to={'/manage/slogans/add/'}>
                  <BaseButton type="primary">{t('slogan.button.add_slogan')}</BaseButton>
                </Link>
              </div>
            </PermissionChecker>
          </div>
        </div>
      </div>
      <Table
        dataSource={data}
        columns={columns}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(doGetSlogan({ currentPage: page, limitPage: pageSize, t: t }));
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
    </div>
  );
};

export default Slogan;
