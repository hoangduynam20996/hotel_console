import React, { useEffect, useState } from 'react';
import { Input, Modal, Form, Select, Space, Radio } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { doGetPolicyByRoomId, doCreateRoomPolicy, doUpdateRoomPolicy } from '@app/store/slices/roomSlice';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

interface FormRoomPolicyProps {
  isModalOpen?: boolean;
  setIsModalOpen?: (state: boolean) => void;
  roomId?: string;
}

export const FormRoomPolicy: React.FC<FormRoomPolicyProps> = ({ isModalOpen, setIsModalOpen, roomId }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { policy } = useSelector((state: any) => state.room);

  useEffect(() => {
    if (roomId) {
      dispatch(doGetPolicyByRoomId({ values: { roomId }, t }));
    }
  }, [roomId]);

  const policyTypes: Record<string, string> = {
    'Free cancellation': 'c32d9631-2256-48d3-ac91-2a1dd533bf67',
    'Cancellation without refund': 'f24d9449-0d25-46f6-8342-ce4f6a42f1a3',
    'Cancellation costs': 'b11d6be5-6ed8-4f09-9932-457a77478948',
  };

  useEffect(() => {
    if (policy) {
      const policyEN = policy?.find((info: any) => info.languageCode === 'EN');
      const policyVI = policy?.find((info: any) => info.languageCode === 'VI');
      form.setFieldsValue({
        typeId: policyTypes[policyEN?.type],
        languageCodeEN: policyEN?.languageCode,
        cancelDataEN: policyEN?.cancelData,
        regulationEN: policyEN?.regulation,
        languageCodeVI: policyVI?.languageCode,
        cancelDataVI: policyVI?.cancelData,
        regulationVI: policyVI?.regulation,
      });
      if (policy.length === 0) {
        form.setFieldsValue({
          typeId: '',
          languageCodeEN: 'EN',
          cancelDataEN: '',
          regulationEN: '',
          languageCodeVI: 'VI',
          cancelDataVI: '',
          regulationVI: '',
        });
      }
    } else {
      form.resetFields();
      form.setFieldsValue({
        typeId: '',
        languageCodeEN: 'EN',
        cancelDataEN: '',
        regulationEN: '',
        languageCodeVI: 'VI',
        cancelDataVI: '',
        regulationVI: '',
      });
    }
  }, [policy]);

  const handleCancel = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const onFinish = (values: any) => {
    values.roomId = roomId;
    values.roomPolicyInfoRequests = [
      {
        language: values.languageCodeVI,
        cancelData: values.cancelDataVI,
        regulation: values.regulationVI,
      },
      {
        language: values.languageCodeEN,
        cancelData: values.cancelDataEN,
        regulation: values.regulationEN,
      },
    ];
    if (policy.length === 0) {
      dispatch(doCreateRoomPolicy({ values, t }));
    } else {
      dispatch(doUpdateRoomPolicy({ values, t }));
    }
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    // form.resetFields();
  };

  return (
    <Modal title={'Room Policy'} open={isModalOpen} onOk={onFinish} onCancel={handleCancel} footer={null} width={1000}>
      <Form layout="vertical" form={form} autoComplete="off" onFinish={onFinish}>
        <Form.Item name="typeId" label="" rules={[{ required: true, message: 'Please input the name!' }]}>
          <Radio.Group>
            <Space direction="vertical">
              <Radio value="c32d9631-2256-48d3-ac91-2a1dd533bf67">Free cancellation</Radio>
              <Radio value="f24d9449-0d25-46f6-8342-ce4f6a42f1a3">Cancellation without refund</Radio>
              <Radio value="b11d6be5-6ed8-4f09-9932-457a77478948">Cancellation costs</Radio>
            </Space>
          </Radio.Group>
        </Form.Item>
        <div className="grid grid-cols-2 gap-3">
          <div>
            <Form.Item
              name="languageCodeEN"
              label="Language"
              rules={[{ required: true, message: 'Please input field!' }]}
            >
              <Select
                options={[
                  { value: 'VI', label: 'Việt Nam' },
                  { value: 'EN', label: 'English' },
                ]}
                disabled
              />
            </Form.Item>
            <Form.Item
              name="cancelDataEN"
              label="Cancel Data"
              rules={[{ required: true, message: 'Please input the field!' }]}
            >
              <Input.TextArea />
            </Form.Item>
            <Form.Item
              name="regulationEN"
              label="Regulation"
              rules={[{ required: true, message: 'Please input the field!' }]}
            >
              <Input.TextArea />
            </Form.Item>
          </div>
          <div>
            <Form.Item
              name="languageCodeVI"
              label="Language"
              rules={[{ required: true, message: 'Please input field!' }]}
            >
              <Select
                options={[
                  { value: 'VI', label: 'Việt Nam' },
                  { value: 'EN', label: 'English' },
                ]}
                disabled
              />
            </Form.Item>
            <Form.Item
              name="cancelDataVI"
              label="Cancel Data"
              rules={[{ required: true, message: 'Please input the field!' }]}
            >
              <Input.TextArea />
            </Form.Item>
            <Form.Item
              name="regulationVI"
              label="Regulation"
              rules={[{ required: true, message: 'Please input the field!' }]}
            >
              <Input.TextArea />
            </Form.Item>
          </div>
        </div>
        <PermissionChecker hasPermissions={['room_create_policy', 'room_update_policy']}>
          <Form.Item>
            <div className="flex justify-center items-center gap-4">
              <BaseButton type="ghost" onClick={handleCancel}>
                Cancel
              </BaseButton>
              <BaseButton type="primary" htmlType="submit">
                Save
              </BaseButton>
            </div>
          </Form.Item>
        </PermissionChecker>
      </Form>
    </Modal>
  );
};

export default FormRoomPolicy;
