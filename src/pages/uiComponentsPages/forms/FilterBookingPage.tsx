import React from 'react';
import dayjs from 'dayjs';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { DatePicker, Form, Input, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

interface FilterBookingPageProps {
  params?: any;
}

const FilterBookingPage: React.FC<FilterBookingPageProps> = ({ params }) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const formatDate = (date: any) => {
    return dayjs(date).format('YYYY-MM-DD');
  };

  React.useEffect(() => {
    form.setFieldsValue({
      bookingId: params?.bookingId || '',
      bookingCode: params?.bookingCode || '',
      roomCode: params?.roomCode || '',
      roomName: params?.roomName || '',
      phoneNumber: params?.phoneNumber || '',
      checkInDate: params?.checkInDate ? dayjs(params.checkInDate) : null,
      checkOutDate: params?.checkOutDate ? dayjs(params.checkOutDate) : null,
      status: params?.status || null,
      bookingDate: params?.bookingDate ? dayjs(params.bookingDate) : null,
      bookingDateTo: params?.bookingDateTo ? dayjs(params.bookingDateTo) : null,
      hotelName: params?.hotelName || '',
      nameContact: params?.nameContact || '',
      statusPayment: params?.statusPayment || null,
    });
  }, [form]);

  const onFinish = (values: any) => {
    const filteredValues: any = {};
    if (values.checkInDate) {
      values.checkInDate = formatDate(values.checkInDate);
    }
    if (values.checkOutDate) {
      values.checkOutDate = formatDate(values.checkOutDate);
    }
    if (values.bookingDate) {
      values.bookingDate = formatDate(values.bookingDate);
    }
    if (values.bookingDateTo) {
      values.bookingDateTo = formatDate(values.bookingDateTo);
    }
    Object.keys(values).forEach((key) => {
      if (values[key]) {
        filteredValues[key] = values[key];
      }
    });
    const queryParams = new URLSearchParams(filteredValues).toString();
    // Redirect to new URL with query params
    navigate(`?${queryParams}`);
  };

  const onReset = () => {
    form.resetFields();
    navigate('');
  };

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  return (
    <>
      <Form layout="inline" className="flex flex-col gap-4" onFinish={onFinish} form={form} autoComplete="off">
        <div className="filter-booking flex flex-wrap justify-between gap-5">
          <Form.Item name="checkInDate">
            <DatePicker
              placeholder="Check in date"
              format="DD-MM-YYYY"
              inputReadOnly={true}
              style={{
                width: '234px',
              }}
            />
          </Form.Item>
          <Form.Item name="checkOutDate">
            <DatePicker
              placeholder="Check out date"
              format="DD-MM-YYYY"
              inputReadOnly={true}
              style={{
                width: '234px',
              }}
            />
          </Form.Item>
          <Form.Item name="status">
            <Select
              placeholder="Booking status"
              allowClear
              style={{
                width: '234px',
              }}
              options={[
                { label: 'Chờ xác nhận', value: 'STATUS_BOOKING_PENDING' },
                { label: 'Đã hủy', value: 'STATUS_BOOKING_CANCEL' },
                { label: 'Đã xác nhận', value: 'STATUS_BOOKING_SUCCESS' },
              ]}
            />
          </Form.Item>
          <Form.Item name="statusPayment" className="m-0">
            <Select
              placeholder="Payment status"
              allowClear
              style={{
                width: '234px',
              }}
              options={[
                { label: 'Đã thanh toán', value: 'STATUS_PAYMENT_PAID' },
                { label: 'Chưa thanh toán', value: 'STATUS_PAYMENT_UN_PAID' },
              ]}
            />
          </Form.Item>
          <Form.Item name="bookingDate">
            <DatePicker
              placeholder="Booking date"
              format="DD-MM-YYYY"
              inputReadOnly={true}
              style={{
                width: '234px',
              }}
            />
          </Form.Item>
          <Form.Item name="bookingDateTo">
            <DatePicker
              placeholder="Booking date to"
              format="DD-MM-YYYY"
              inputReadOnly={true}
              style={{
                width: '234px',
              }}
            />
          </Form.Item>
          <Form.Item name="bookingCode">
            <Input onBlur={handleBlur('bookingCode')} placeholder="Booking code" />
          </Form.Item>
          <Form.Item name="roomCode">
            <Input onBlur={handleBlur('roomCode')} placeholder="Room code" />
          </Form.Item>
          <Form.Item name="hotelName">
            <Input onBlur={handleBlur('hotelName')} placeholder="Hotel name" />
          </Form.Item>
          <Form.Item className="mr-3" name="roomName">
            <Input onBlur={handleBlur('roomName')} placeholder="Room name" />
          </Form.Item>
          <Form.Item name="nameContact">
            <Input onBlur={handleBlur('nameContact')} placeholder="Contact name" />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            rules={[
              {
                pattern: /^(\+[0-9]{11}|0[0-9]{9})$/,
                message: t('user.modal.message.phone_message1'),
              },
            ]}
          >
            <Input type="number" onBlur={handleBlur('phoneNumber')} placeholder="Phone Number" />
          </Form.Item>
        </div>
        <div className="flex justify-center items-center gap-4">
          <BaseButton type="primary" htmlType="submit">
            Search
          </BaseButton>
          <BaseButton type="ghost" onClick={onReset}>
            Reset
          </BaseButton>
        </div>
      </Form>
    </>
  );
};

export default FilterBookingPage;
