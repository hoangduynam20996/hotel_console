import React, { useEffect, useState } from 'react';
import { Input, Modal, Form, Select } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { doGetHotelContact, doCreateHotelContact, doUpdateHotelContact } from '@app/store/slices/hotelSlice';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

interface AddHotelContactProps {
  isModalOpen?: boolean;
  setIsModalOpen?: (state: boolean) => void;
  hotelId?: string;
  hotelContactId?: string;
}

export const AddHotelContact: React.FC<AddHotelContactProps> = ({
  isModalOpen,
  setIsModalOpen,
  hotelId,
  hotelContactId,
}) => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { hotelContact } = useSelector((state: any) => state.hotel);

  useEffect(() => {
    if (hotelContactId) {
      dispatch(doGetHotelContact({ values: { id: hotelContactId }, t }));
    }
  }, [dispatch, hotelContactId]);

  useEffect(() => {
    if (hotelContact) {
      form.setFieldsValue({
        representativeName: hotelContact.representativeName,
        phoneNumber: hotelContact.phoneNumber,
        email: hotelContact.email,
        facebookLink: hotelContact.facebookLink,
        zaloLink: hotelContact.zaloLink,
      });
    } else {
      form.resetFields();
    }
  }, [hotelContact]);

  const showModal = () => {
    if (setIsModalOpen) {
      setIsModalOpen(true);
    }
    form.resetFields();
  };

  const handleOk = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const handleCancel = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const onFinish = (values: any) => {
    if (hotelContactId) {
      dispatch(doUpdateHotelContact({ values: { ...values, id: hotelContactId }, t }));
    } else {
      dispatch(doCreateHotelContact({ values: { ...values, hotelId }, t }));
    }
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  return (
    <Modal title={'Hotel contact'} open={isModalOpen} onOk={onFinish} onCancel={handleCancel} footer={null}>
      <Form
        form={form}
        onFinish={onFinish}
        layout="vertical"
        name="form_in_modal"
        initialValues={{ modifier: 'public' }}
      >
        <Form.Item
          name="representativeName"
          label="Representative Name"
          rules={[{ required: true, message: 'Please input the hotel name!' }]}
        >
          <Input onBlur={handleBlur('representativeName')} />
        </Form.Item>
        {/* phoneNumber */}
        <Form.Item
          name="phoneNumber"
          label="Phone Number"
          rules={[{ required: true, message: 'Please input the phone number!' }]}
        >
          <Input onBlur={handleBlur('phoneNumber')} />
        </Form.Item>
        {/* email */}
        <Form.Item
          name="email"
          label="Email"
          rules={[
            {
              required: true,
              message: 'Please input the email!',
            },
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
          ]}
        >
          <Input onBlur={handleBlur('email')} />
        </Form.Item>
        {/* facebookLink */}
        <Form.Item
          name="facebookLink"
          label="Facebook Link"
          rules={[
            {
              required: true,
              message: 'Please input the facebook link!',
            },
            {
              type: 'url',
              message: 'The input is not valid URL!',
            },
          ]}
        >
          <Input onBlur={handleBlur('facebookLink')} />
        </Form.Item>
        {/* zaloLink */}
        <Form.Item
          name="zaloLink"
          label="Zalo Link"
          rules={[
            {
              required: true,
              message: 'Please input the facebook link!',
            },
            {
              type: 'url',
              message: 'The input is not valid URL!',
            },
          ]}
        >
          <Input onBlur={handleBlur('zaloLink')} />
        </Form.Item>
        <PermissionChecker hasPermissions={['hotel_create_contact', 'hotel_update_contact']}>
          <Form.Item>
            <div className="flex justify-center items-center gap-4">
              <BaseButton type="ghost" onClick={handleCancel}>
                Cancel
              </BaseButton>
              <BaseButton type="primary" htmlType="submit">
                Save
              </BaseButton>
            </div>
          </Form.Item>
        </PermissionChecker>
      </Form>
    </Modal>
  );
};

export default AddHotelContact;
