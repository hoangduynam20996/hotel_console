import { PlusOutlined } from '@ant-design/icons';
import { httpApi } from '@app/api/http.api';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { deleteImageById, uploadImage } from '@app/store/slices/imageSlice';
import {
  doCreateRoom,
  doGetRoom,
  doGetRoomServiceAmenityRoom,
  doUpdateRoom,
  doUpdateServiceAmenityRoom,
  setRoomId,
} from '@app/store/slices/roomSlice';
import type { TabsProps } from 'antd';
import { Form, Input, InputNumber, Modal, Select, Tabs, Upload, UploadFile, message } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import ServiceAmenityForm from './ServiceAmenityForm';

interface AddRoomProps {
  isModalOpen?: boolean;
  setIsModalOpen?: (state: boolean) => void;
  hotelId?: string;
  language?: string;
  roomId?: string;
}

interface FileItem {
  id: string | number;
  name: string;
  status: string;
  url: string;
}

export const AddRoom: React.FC<AddRoomProps> = ({ isModalOpen, setIsModalOpen, hotelId, language, roomId }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { images } = useSelector((state: any) => state.image);
  const { room, roomTypes, roomServiceAmenityRoom } = useSelector((state: any) => state.room);
  const [fileList, setFileList] = React.useState<FileItem[]>([]);
  const { id } = useParams();
  useEffect(() => {
    if (roomId) {
      dispatch(doGetRoom({ values: { roomId }, t }));
      dispatch(doGetRoomServiceAmenityRoom({ values: { roomId }, t }));
    }
  }, [roomId]);

  useEffect(() => {
    if (roomId && room) {
      form.setFieldsValue({
        id: room?.id,
        roomName: room?.roomName,
        roomNameCustom: room?.roomNameCustom,
        roomType: room?.roomTypeName,
        quantityRoom: room?.quantityRoom,
        pricePerNight: room?.priceRoom,
        bedName: room?.bedName,
        maxOccupancy: room?.maxOccupancy,
        roomArea: room?.roomArea,
        status: room?.status,
      });
    } else {
      form.resetFields();
    }
  }, [roomId, room, isModalOpen]);

  useEffect(() => {
    if (images) {
      setFileList(
        images.map((image: any, index: number) => ({
          id: image.id,
          name: image?.name,
          status: 'done',
          url: image?.urlImage,
        })),
      );
    } else {
      setFileList([]);
    }
  }, [images]);

  const showModal = () => {
    if (setIsModalOpen) {
      setIsModalOpen(true);
    }
    form.resetFields();
  };

  const handleOk = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const handleCancel = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
    dispatch(setRoomId(''));
  };

  const onFinish = (values: any) => {
    console.log('Success:', values);
    const roomType = roomTypes.filter((roomType) => roomType.name === values.roomType);
    values.roomTypeId = roomType[0].id;
    values.roomId = roomId;
    values.hotelId = hotelId;

    if (setIsModalOpen) {
      dispatch(roomId ? doUpdateRoom({ values, t }) : doCreateRoom({ values, t }));
      setIsModalOpen(false);
    }
    form.resetFields();
    dispatch(setRoomId(''));
  };

  const onUpdate = (data: any, serviceAmenities: []) => {
    const dataService = serviceAmenities.map((id) => ({
      id: id,
    }));
    const values = {
      roomId: roomId,
      ...data,
      serviceAndAmenityId: dataService,
    };
    if (setIsModalOpen) {
      dispatch(doUpdateServiceAmenityRoom({ values, t }));
    }
    setIsModalOpen(false);
    dispatch(setRoomId(''));
  };

  // Upload
  const handleDeleteImage = (id: string) => {
    dispatch(deleteImageById(id));
    setFileList(fileList.filter((file: any) => file.id !== id));
  };

  const handleUploadImage = (file: any) => {
    const formData = new FormData();
    formData.append('images', file);
    formData.append('hid', id);
    dispatch(uploadImage(formData));
  };

  const uploadButton = (
    <button style={{ border: 0, background: 'none' }} type="button">
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </button>
  );

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  const items: TabsProps['items'] | any = [
    {
      key: '1',
      label: 'Informations Room',
      children: (
        <Form
          form={form}
          onFinish={onFinish}
          layout="vertical"
          name="form_in_modal"
          initialValues={{ modifier: 'public' }}
        >
          <div className="grid grid-cols-2 gap-3">
            <Form.Item
              name="roomName"
              label="Room Name"
              className="w-full"
              rules={[{ required: true, message: 'Please input the room name!' }]}
            >
              <Input onBlur={handleBlur('name')} />
            </Form.Item>
            <Form.Item name="roomNameCustom" label="Name Custom" className="w-full">
              <Input onBlur={handleBlur('nameCustom')} />
            </Form.Item>
          </div>
          <div className="grid grid-cols-2 gap-3">
            <Form.Item name="roomType" label="Type" rules={[{ required: true, message: 'Please input the address!' }]}>
              <Select>
                {roomTypes?.map((type: any) => (
                  <Select.Option key={type.id} value={type.name}>
                    {type.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="pricePerNight"
              label="Price"
              rules={[{ required: true, message: 'Please input the price!' }]}
            >
              <InputNumber
                type="number"
                controls={false}
                style={{
                  width: '100%',
                }}
              />
            </Form.Item>
          </div>
          <Form.Item
            name="roomArea"
            label="Room Area"
            rules={[{ required: true, message: 'Please input the Room Area!' }]}
          >
            <InputNumber
              type="number"
              controls={false}
              style={{
                width: '100%',
              }}
            />
          </Form.Item>
          <Form.Item
            name="quantityRoom"
            label="Quantity Room"
            rules={[{ required: true, message: 'Please input the quantity room!' }]}
          >
            <InputNumber
              type="number"
              controls={false}
              style={{
                width: '100%',
              }}
            />
          </Form.Item>
          <Form.Item
            name="maxOccupancy"
            label="Max Occupancy"
            rules={[{ required: true, message: 'Please input the occupancy!' }]}
          >
            <InputNumber
              type="number"
              controls={false}
              style={{
                width: '100%',
              }}
            />
          </Form.Item>
          <Form.Item
            name="bedName"
            label="Bed Name"
            rules={[{ required: true, message: 'Please input the bed name!' }]}
          >
            <Input onBlur={handleBlur('bedName')} />
          </Form.Item>
          {roomId && (
            <Form.Item<FieldType> name="status">
              <Select
                style={{ width: 120 }}
                options={[
                  { value: 'ACTIVE', label: 'ACTIVE' },
                  { value: 'NO_ACTIVE', label: 'NO ACTIVE' },
                ]}
              />
            </Form.Item>
          )}

          <PermissionChecker hasPermissions={['room_create_info', 'room_update_info']}>
            <Form.Item>
              <div className="flex justify-center items-center gap-4">
                <BaseButton type="ghost" onClick={handleCancel}>
                  Cancel
                </BaseButton>
                <BaseButton type="primary" htmlType="submit">
                  Save
                </BaseButton>
              </div>
            </Form.Item>
          </PermissionChecker>
        </Form>
      ),
    },
    roomId
      ? {
          key: '2',
          label: 'Service amenity',
          children: (
            <div>
              {roomServiceAmenityRoom && (
                <ServiceAmenityForm service={roomServiceAmenityRoom} onUpdate={onUpdate} onCancel={handleCancel} />
              )}
            </div>
          ),
        }
      : null,
    roomId
      ? {
          key: '3',
          label: 'Images Room',
          children: (
            <Form
              form={form}
              onFinish={onFinish}
              layout="vertical"
              name="form_in_modal"
              initialValues={{ modifier: 'public' }}
            >
              <PermissionChecker hasPermissions={['hotel_update_info']}>
                <Form.Item
                  label="Images"
                  name="images"
                  rules={[{ required: true, message: 'Please input your images!' }]}
                >
                  <Upload
                    action={(file: File) => {
                      handleUploadImage(file);
                    }}
                    beforeUpload={(file: File) => {
                      const acceptedTypes = ['image/jpeg', 'image/png'];
                      const isFileTypeAccepted = acceptedTypes.includes(file.type);
                      if (!isFileTypeAccepted) {
                        message.error('Only JPG and PNG files are allowed!');
                      }
                      return isFileTypeAccepted;
                    }}
                    onRemove={(file: any) => {
                      handleDeleteImage(file.id);
                    }}
                    headers={{
                      'Content-Type': 'multipart/form-data',
                    }}
                    maxCount={10}
                    fileList={fileList as UploadFile<any>[]}
                    multiple
                    listType="picture-card"
                  >
                    {uploadButton}
                  </Upload>
                </Form.Item>
              </PermissionChecker>
            </Form>
          ),
        }
      : undefined,
  ];

  return (
    <Modal
      title={roomId ? 'Edit Room' : 'Add Room'}
      open={isModalOpen}
      onOk={onFinish}
      onCancel={handleCancel}
      footer={null}
      width={1200}
    >
      <Tabs defaultActiveKey="1" items={items} tabPosition="left" />
    </Modal>
  );
};

export default AddRoom;
