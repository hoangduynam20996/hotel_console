import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Form, Input, Select } from 'antd';
import React from 'react';

interface FilterRoomsPageProps {
  hotelId?: string;
}

type FieldType = {
  roomName: string;
};

export const FilterRoomsPage: React.FC<FilterRoomsPageProps> = ({
  hotelId,
  status,
  setStatus,
  roomName,
  setRoomName,
}) => {
  const [form] = Form.useForm();

  React.useEffect(() => {
    form.setFieldsValue({
      roomName: roomName,
      status: status,
    });
  }, [form, status, roomName]);

  const onFinish = (values: any) => {
    setRoomName(values.roomName);
    setStatus(values.status);
  };

  return (
    <>
      <Form
        layout="inline"
        className="flex flex-wrap md:flex-nowrap gap-1"
        onFinish={onFinish}
        form={form}
        autoComplete="off"
      >
        <Form.Item<FieldType> name="roomName">
          <Input placeholder="Room name" />
        </Form.Item>
        <Form.Item<FieldType> name="status">
          <Select
            style={{ width: 120 }}
            options={[
              { value: 'ACTIVE', label: 'ACTIVE' },
              { value: 'NO_ACTIVE', label: 'NO ACTIVE' },
            ]}
          />
        </Form.Item>

        <BaseButton type="ghost" htmlType="submit">
          Search
        </BaseButton>
      </Form>
    </>
  );
};

export default FilterRoomsPage;
