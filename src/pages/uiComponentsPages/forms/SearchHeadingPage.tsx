import React from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Form, Input, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'react-i18next';
import { setTitleHeading, setLanguageCode, setCurrentPage } from '@app/store/slices/Headingslice';

interface SearchHeadingPops {}

type FieldType = {
  titleHeading: string;
  languageCode: string;
};

export const SearchHeadingPage: React.FC<SearchHeadingPops> = (props) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  React.useEffect(() => {
    form.setFieldsValue({
      titleHeading: '',
      languageCode: 'EN',
    });
  }, [form]);

  const onFinish = (values: any) => {
    console.log('  sdasdasdadasdasdasd', values);
    dispatch(setCurrentPage(1));
    dispatch(setTitleHeading(values.titleHeading));
    dispatch(setLanguageCode(values.languageCode));
  };

  return (
    <>
      <Form
        layout="inline"
        className="flex flex-wrap md:flex-nowrap flex-nowrap gap-1"
        onFinish={onFinish}
        form={form}
        autoComplete="off"
      >
        <Form.Item<FieldType> name="titleHeading">
          <Input placeholder="Title Heading" />
        </Form.Item>
        <Form.Item<FieldType> name="languageCode">
          <Select
            style={{ width: 120 }}
            options={[
              { value: 'VI', label: 'Việt Nam' },
              { value: 'EN', label: 'English' },
            ]}
          />
        </Form.Item>
        <BaseButton type="ghost" htmlType="submit">
          Search
        </BaseButton>
      </Form>
    </>
  );
};

export default SearchHeadingPage;
