import React, { useEffect, useState } from 'react';
import { Input, Modal, Form, Select } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { doAddHotelInfo, doGetProvinces } from '@app/store/slices/hotelSlice';

interface AddHotelInfoProps {
  isModalOpen?: boolean;
  setIsModalOpen?: (state: boolean) => void;
  hotelId?: string;
  language?: string;
}

export const AddHotelInfo: React.FC<AddHotelInfoProps> = ({ isModalOpen, setIsModalOpen, hotelId, language }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { provinces } = useSelector((state: any) => state.hotel);

  useEffect(() => {
    if (hotelId) {
      dispatch(doGetProvinces({ values: { id: hotelId }, t }));
    }
  }, [dispatch, hotelId]);

  if (language === 'VI') form.setFieldsValue({ languageCode: 'EN' });
  if (language === 'EN') form.setFieldsValue({ languageCode: 'VI' });

  const showModal = () => {
    if (setIsModalOpen) {
      setIsModalOpen(true);
    }
    form.resetFields();
  };

  const handleOk = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const handleCancel = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const onFinish = (values: any) => {
    values.id = hotelId;
    dispatch(doAddHotelInfo({ values, t }));
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
    form.resetFields();
  };

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  return (
    <Modal title={'Language setting'} open={isModalOpen} onOk={onFinish} onCancel={handleCancel} footer={null}>
      <Form
        form={form}
        onFinish={onFinish}
        layout="vertical"
        name="form_in_modal"
        initialValues={{ modifier: 'public' }}
      >
        <Form.Item
          name="languageCode"
          label="Language"
          rules={[{ required: true, message: 'Please select language!' }]}
        >
          <Select
            disabled
            options={[
              { value: 'VI', label: 'Việt Nam' },
              { value: 'EN', label: 'English' },
            ]}
          />
        </Form.Item>
        <Form.Item name="name" label="Hotel Name" rules={[{ required: true, message: 'Please input the hotel name!' }]}>
          <Input onBlur={handleBlur('name')} />
        </Form.Item>
        <Form.Item name="address" label="Address" rules={[{ required: true, message: 'Please input the address!' }]}>
          <Input onBlur={handleBlur('address')} />
        </Form.Item>
        <Form.Item name="city" label="City" rules={[{ required: true, message: 'Please input the city!' }]}>
          <Select showSearch>
            {provinces.map((province: any) => (
              <Select.Option key={province.provinceId} value={province.province}>
                {province.province}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item>
          <div className="flex justify-center items-center gap-4">
            <BaseButton type="ghost" onClick={handleCancel}>
              Cancel
            </BaseButton>
            <BaseButton type="primary" htmlType="submit">
              Save
            </BaseButton>
          </div>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default AddHotelInfo;
