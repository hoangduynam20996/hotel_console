import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import {
  doCreateServiceAmenity,
  doDeleteServiceAmenity,
  doUpdateServiceAmenityRoom,
} from '@app/store/slices/serviceAmenitySlice';
import { Checkbox, Form, Input, Modal, Select } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

export const AddServiceAmenityRoom: React.FC<any> = ({ isModalOpen, setIsModalOpen }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const { servicesAmenity } = useSelector((state: any) => state.serviceAmenity);
  const [serviceAmenityId, setServiceAmenityId] = React.useState('');

  const onFinish = (data: any) => {
    if (serviceAmenityId) {
      const values = {
        ...data,
        serviceAmenityId,
      };
      dispatch(doUpdateServiceAmenityRoom({ values, t }));
    } else {
      const values = {
        ...data,
      };
      dispatch(doCreateServiceAmenity({ values, t }));
      handleReset();
    }
  };

  const handleCancel = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
    }
  };

  const options = servicesAmenity.map((service) => ({
    label: service.name,
    value: service.id,
  }));

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  const handleChangeServiceAmenityRoom = (e) => {
    setServiceAmenityId(e);
  };

  useEffect(() => {
    if (serviceAmenityId) {
      const service = servicesAmenity.find((service) => service.id === serviceAmenityId);
      form.setFieldsValue({
        name: service.name,
        type: service.type,
        description: service.description,
        status: service.status,
      });
    } else {
      form.setFieldsValue({
        name: '',
        type: '',
        description: '',
        status: 'ACTIVE',
      });
    }
  }, [serviceAmenityId]);

  const handleReset = () => {
    setServiceAmenityId('');
    form.setFieldsValue({
      name: '',
      type: '',
      description: '',
      status: 'ACTIVE',
    });
  };

  const handleDelete = () => {
    if (serviceAmenityId) {
      dispatch(doDeleteServiceAmenity({ values: { serviceAmenityId }, t }));
      setServiceAmenityId('');
    }
  };

  return (
    <Modal
      title={'Service Amenity Room'}
      open={isModalOpen}
      onOk={onFinish}
      onCancel={handleCancel}
      footer={null}
      width={1200}
    >
      <Form
        form={form}
        onFinish={onFinish}
        layout="vertical"
        name="form_in_modal"
        initialValues={{ modifier: 'public' }}
      >
        <div className="flex flex-row justify-between gap-4">
          <Form.Item label="Service amenity room names" className="w-full">
            <Select
              showSearch
              className="w-full"
              placeholder="Search amenity room"
              optionFilterProp="children"
              filterOption={(input, option) => (option?.label ?? '').includes(input)}
              filterSort={(optionA, optionB) =>
                (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
              }
              options={options}
              // onDeselect={handleDeselectServiceAmenityRoom}
              onSelect={handleChangeServiceAmenityRoom}
            ></Select>
          </Form.Item>

          <div className="flex flex-col gap-2 w-full">
            <Form.Item
              name="name"
              label="Name"
              className="w-full"
              rules={[{ required: true, message: 'Please input the service name!' }]}
            >
              <Input onBlur={handleBlur('name')} />
            </Form.Item>
            <Form.Item
              name="type"
              label="Type"
              className="w-full"
              rules={[{ required: true, message: 'Please input the service type!' }]}
            >
              <Input onBlur={handleBlur('type')} />
            </Form.Item>
            <Form.Item name="description" label="Description" className="w-full">
              <Input onBlur={handleBlur('description')} />
            </Form.Item>
            <Form.Item<FieldType> name="status">
              <Select
                style={{ width: 120 }}
                options={[
                  { value: 'ACTIVE', label: 'ACTIVE' },
                  { value: 'NO_ACTIVE', label: 'NO ACTIVE' },
                ]}
              />
            </Form.Item>
            <div className="flex gap-4 justify-center items-center">
              <PermissionChecker hasPermissions={['room_create_info', 'room_update_info']}>
                <Form.Item className="mb-0">
                  <div className="flex justify-center items-center gap-4">
                    <BaseButton type="ghost" onClick={handleReset}>
                      Reset
                    </BaseButton>
                    <BaseButton type="ghost" onClick={handleCancel}>
                      Cancel
                    </BaseButton>
                    <BaseButton type="primary" htmlType="submit">
                      Save
                    </BaseButton>
                  </div>
                </Form.Item>
              </PermissionChecker>
              {/* <PermissionChecker
                                        hasPermissions={['room_delete_info']}
                                    >
                                                <BaseButton disabled={!serviceAmenityId} type='primary' onClick={handleDelete}>
                                                    Delete
                                                </BaseButton>
                                    </PermissionChecker> */}
            </div>
          </div>
        </div>
      </Form>
    </Modal>
  );
};
