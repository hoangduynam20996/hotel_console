import React, { useState, useEffect } from 'react';
import { Form, Button, Space, InputNumber, Checkbox, Select } from 'antd';
import { useSelector } from 'react-redux';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

const { Option } = Select;

const ServiceAmenityForm = ({ service, onUpdate, onCancel }) => {
  const [form] = Form.useForm();
  const { servicesAmenity } = useSelector((state: any) => state.serviceAmenity);
  const [serviceAmenityRoomNames, setServiceAmenityRoomNames] = useState([]);
  const [defaultValues, setDefaultValues] = useState([]);
  const [showChildrenFields, setShowChildrenFields] = useState(false);
  const [showAdultFields, setShowAdultFields] = useState(false);

  useEffect(() => {
    if (service) {
      form.setFieldsValue({
        extraBed: service.extraBed,
        childrenSleepInCribs: service.childrenSleepInCribs,
        childrenOld: service.childrenOld,
        priceGuestAdults: service.priceGuestAdults,
        priceGuestChildren: service.priceGuestChildren,
        typeOfGuestAdults: service.typeOfGuestAdults,
        typeOfGuestChildren: service.typeOfGuestChildren,
        serviceAmenityRoomNames: service.serviceAmenityRoomNames,
      });
      setServiceAmenityRoomNames(service.serviceAmenityRoomNames);
      setShowChildrenFields(service.typeOfGuestChildren);
      setShowAdultFields(service.typeOfGuestAdults);
    }
  }, [service]);

  const options = servicesAmenity.map((service) => ({
    label: service.name,
    value: service.id,
  }));

  useEffect(() => {
    if (serviceAmenityRoomNames) {
      const defaultValues = serviceAmenityRoomNames.map((service) => service.id);
      setDefaultValues(defaultValues);
    }
  }, [serviceAmenityRoomNames]);

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  const handleTypeOfGuestChildrenChange = (checked: boolean) => {
    setShowChildrenFields(checked);
  };

  const handleTypeOfGuestAdultsChange = (checked: boolean) => {
    setShowAdultFields(checked);
  };

  const handleChangeServiceAmenityRoom = (selectedValues) => {
    setDefaultValues((prev) => [...prev, selectedValues]);
  };

  const handleDeselectServiceAmenityRoom = (deselectedValue) => {
    const updatedValues = defaultValues.filter((value) => value !== deselectedValue);
    setDefaultValues(updatedValues);
  };

  return (
    <div className="w-full">
      <Form
        form={form}
        onFinish={(values) => {
          onUpdate(values, defaultValues);
        }}
        layout="vertical"
      >
        <Form.Item
          label="Extra bed"
          name="extraBed"
          className="w-full"
          rules={[{ required: true, message: 'Please input the Extra Bed!' }]}
        >
          <InputNumber className="w-full" onBlur={handleBlur('extraBed')} />
        </Form.Item>
        <Form.Item label="Children sleep in cribs" name="childrenSleepInCribs" valuePropName="checked">
          <Checkbox />
        </Form.Item>
        <Form.Item label="Type of guest children" name="typeOfGuestChildren" valuePropName="checked">
          <Checkbox onChange={(e) => handleTypeOfGuestChildrenChange(e.target.checked)} />
        </Form.Item>
        {showChildrenFields && (
          <>
            <Form.Item label="Children old" className="w-full" name="childrenOld">
              <InputNumber className="w-full" />
            </Form.Item>
            <Form.Item label="Price for guest children" className="w-full" name="priceGuestChildren">
              <InputNumber className="w-full" />
            </Form.Item>
          </>
        )}
        <Form.Item label="Type of guest adults" name="typeOfGuestAdults" valuePropName="checked">
          <Checkbox onChange={(e) => handleTypeOfGuestAdultsChange(e.target.checked)} />
        </Form.Item>
        {showAdultFields && (
          <Form.Item label="Price for guest adults" className="w-full" name="priceGuestAdults">
            <InputNumber className="w-full" />
          </Form.Item>
        )}
        <Form.Item label="Service amenity room names">
          <Select
            mode="multiple"
            style={{ width: '100%' }}
            defaultValue={defaultValues}
            value={defaultValues}
            placeholder="select service amenity room"
            options={options}
            onDeselect={handleDeselectServiceAmenityRoom}
            onSelect={handleChangeServiceAmenityRoom}
          ></Select>
        </Form.Item>
        <PermissionChecker hasPermissions={['room_create_info', 'room_update_info']}>
          <Space className="flex justify-center">
            <Button type="primary" htmlType="submit">
              Save
            </Button>
            <Button onClick={onCancel}>Cancel</Button>
          </Space>
        </PermissionChecker>
      </Form>
    </div>
  );
};

export default ServiceAmenityForm;
