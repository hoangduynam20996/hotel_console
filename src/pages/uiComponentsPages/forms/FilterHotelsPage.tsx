import React from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Form, Input, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { setStatus, setHotelName, setCurrentPage } from '@app/store/slices/hotelSlice';
import { useTranslation } from 'react-i18next';

interface FilterHotelsPageProps {
  // Define the props for your config component here
}

type FieldType = {
  hotelName: string;
  status: boolean;
};

export const FilterHotelsPage: React.FC<FilterHotelsPageProps> = (props) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  React.useEffect(() => {
    form.setFieldsValue({
      hotelName: '',
      status: 'ACTIVE',
    });
  }, [form]);

  const onFinish = (values: any) => {
    dispatch(setCurrentPage(1));
    dispatch(setHotelName(values.hotelName));
    dispatch(setStatus(values.status));
  };

  return (
    <>
      <Form
        layout="inline"
        className="flex flex-wrap md:flex-nowrap gap-1"
        onFinish={onFinish}
        form={form}
        autoComplete="off"
      >
        <Form.Item<FieldType> name="hotelName">
          <Input placeholder={t('columns.hotelName')} />
        </Form.Item>
        <Form.Item<FieldType> name="status">
          <Select
            style={{ width: 120 }}
            options={[
              { value: 'ACTIVE', label: 'ACTIVE' },
              { value: 'NO_ACTIVE', label: 'NO ACTIVE' },
            ]}
          />
        </Form.Item>

        <BaseButton type="ghost" htmlType="submit">
          {t('columns.searchhotel')}
        </BaseButton>
      </Form>
    </>
  );
};

export default FilterHotelsPage;
