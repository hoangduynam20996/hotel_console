import React from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Table } from 'antd';
import {
  doGetBookings,
  doGetBooking,
  doChangeStatusBooking,
  doChangeStatusPayment,
} from '@app/store/slices/bookingSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import DetailBooking from './DetailBooking';

interface RoomListProps {}

export const BookingList: React.FC<RoomListProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { bookings, totalPages, currentPage, limitPage, loading } = useSelector((state: any) => state.booking);
  const [data, setData] = React.useState<any[]>([]);
  const [isModalOpenDetail, setIsModalOpenDetail] = React.useState(false);
  const [bookingId, setBookingId] = React.useState('');
  const formatDate = (date: any) => {
    return dayjs(date).format('DD-MM-YYYY');
  };
  const formatter = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
    minimumFractionDigits: 0,
    currencyDisplay: 'symbol',
  });

  const StatusType: Record<string, string> = {
    STATUS_BOOKING_PENDING: 'Chờ xác nhận',
    STATUS_BOOKING_CANCEL: 'Đã hủy',
    STATUS_BOOKING_PENDING: 'Đã thanh toán',
    STATUS_BOOKING_SUCCESS: 'Đã xác nhận',
    STATUS_PAYMENT_PAID: 'Đã thanh toán',
    STATUS_PAYMENT_UN_PAID: 'Chưa thanh toán',
  };

  console.log(bookings);

  React.useEffect(() => {
    setData(
      bookings.map((booking: any, index: number) => {
        return {
          index: -(-(currentPage - 1) * limitPage - (index + 1)),
          key: booking.bookingId,
          status: booking.status,
          bookingCode: booking.bookingCode,
          checkInDate: formatDate(booking.checkInDate),
          checkOutDate: formatDate(booking.checkOutDate),
          bookingDate: booking.bookingDate,
          quantityRoomOrder: booking.quantityRoomOrder,
          pinCode: booking.pinCode,
          numberOfNights: booking.numberOfNights,
          hotel: booking.hotel,
          createdBy: booking.fullName,
          customerName: booking.fullName,
          customerPhoneNumber: booking.phoneNumber,
          customerMail: booking.email,
          customerRequirement: booking.customerRequirement,
          paymentId: booking.payment.paymentId,
          paymentDate: booking.payment.paymentDate,
          paymentStatus: booking.payment.status,
          totalPayment: formatter.format(booking.totalPrice),
        };
      }),
    );
  }, [bookings]);

  const columns = [
    {
      title: '',
      dataIndex: 'column1',
      key: 'column1',
      render: (text: string, record: any) => {
        return (
          <div className="flex h-36 flex-col items-start justify-start">
            <h3 className="font-semibold">Đơn hàng #{record.index}</h3>
            <p>Trạng thái đơn hàng: {StatusType[record.status]}</p>
            <p>Mã đặt phòng: {record.bookingCode}</p>
            <p>Người tạo: {record.createdBy}</p>
            <p>Ngày đặt phòng: {formatDate(record.bookingDate)}</p>
          </div>
        );
      },
    },
    {
      title: '',
      dataIndex: 'column2',
      key: 'column2',
      render: (text: string, record: any) => {
        return (
          <div className="flex h-36 flex-col items-start justify-start">
            <h3 className="font-semibold">Thông tin nhận phòng</h3>
            <p>Số phòng: {record.pinCode}</p>
            <p>Khách sạn: {record.hotel.hotelName}</p>
            <p>Địa chỉ: {record.hotel.hotelAddress}</p>
          </div>
        );
      },
    },
    {
      title: '',
      dataIndex: 'column2',
      key: 'column2',
      render: (text: string, record: any) => {
        return (
          <div className="flex h-36 flex-col items-start justify-start">
            <h3 className="font-semibold">Thông tin liên hệ</h3>
            <p>Họ tên: {record.customerName}</p>
            <p>Điện thoại: {record.customerPhoneNumber}</p>
            <p>Email: {record.customerMail}</p>
            <PermissionChecker hasPermissions={['admin_update']}>
              {record.paymentStatus !== 'STATUS_PAYMENT_PAID' && (
                <BaseButton
                  type="primary"
                  size="small"
                  className="ml-auto mt-2"
                  onClick={() => {
                    dispatch(
                      doChangeStatusPayment({
                        values: { bookingId: record.key, statusChange: 'STATUS_PAYMENT_PAID' },
                        t,
                      }),
                    );
                  }}
                >
                  Xác nhận thanh toán
                </BaseButton>
              )}
            </PermissionChecker>
          </div>
        );
      },
    },
    {
      title: '',
      dataIndex: 'column2',
      key: 'column2',
      render: (text: string, record: any) => {
        return (
          <div className="flex h-36 flex-col items-start justify-start">
            <h3 className="font-semibold">Thông tin thanh toán</h3>
            <p>Trạng thái: {record.paymentStatus}</p>
            <p>Ngày thanh toán: {record.paymentDate ? formatDate(record.paymentDate) : null}</p>
            <p>Số tiền: {record.totalPayment}</p>
            <div className="flex justify-between w-full">
              <PermissionChecker hasPermissions={['admin_update']}>
                {record.status !== 'STATUS_BOOKING_SUCCESS' && (
                  <BaseButton
                    type="ghost"
                    size="small"
                    className="mt-2"
                    onClick={() => {
                      dispatch(
                        doChangeStatusBooking({
                          values: { bookingId: record.key, status: 'STATUS_BOOKING_SUCCESS' },
                          t,
                        }),
                      );
                    }}
                  >
                    Xác nhận
                  </BaseButton>
                )}
                {record.status !== 'STATUS_BOOKING_CANCEL' && (
                  <BaseButton
                    type="ghost"
                    size="small"
                    className="mt-2"
                    onClick={() => {
                      dispatch(
                        doChangeStatusBooking({
                          values: { bookingId: record.key, status: 'STATUS_BOOKING_CANCEL' },
                          t,
                        }),
                      );
                    }}
                  >
                    Hủy
                  </BaseButton>
                )}
              </PermissionChecker>
              <BaseButton
                type="link"
                size="small"
                className="mt-2"
                onClick={() => {
                  dispatch(doGetBooking({ values: { bookingId: record.key }, t }));
                  setBookingId(record.key);
                  setIsModalOpenDetail(true);
                }}
              >
                Xem chi tiết
              </BaseButton>
            </div>
          </div>
        );
      },
    },
  ];
  return (
    <>
      <Table
        dataSource={data}
        columns={columns}
        loading={loading}
        scroll={{ x: 1400 }}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(doGetBookings({ currentPage: page, limitPage: pageSize }));
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
      <DetailBooking
        bookingId={bookingId}
        setBookingId={setBookingId}
        isModalOpen={isModalOpenDetail}
        setIsModalOpen={setIsModalOpenDetail}
      />
    </>
  );
};

export default BookingList;
