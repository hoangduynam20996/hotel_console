import React from 'react';
import { Modal, Descriptions, Badge, Image } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

interface DetailBookingProps {
  bookingId: string;
  setBookingId: (state: string) => void;
  isModalOpen?: boolean;
  setIsModalOpen?: (state: boolean) => void;
}

export const DetailBooking: React.FC<DetailBookingProps> = ({
  bookingId,
  setBookingId,
  isModalOpen,
  setIsModalOpen,
}) => {
  const { t } = useTranslation();
  const { booking, bookings } = useSelector((state: any) => state.booking);

  const [filterBookings, setFilterBookings] = React.useState({});

  React.useEffect(() => {
    setFilterBookings(...bookings.filter((item) => item.bookingId === bookingId));
  }, [bookings, bookingId]);

  const showModal = () => {
    if (setIsModalOpen) {
      setIsModalOpen(true);
    }
  };

  const handleOk = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
      setBookingId('');
    }
  };

  const handleCancel = () => {
    if (setIsModalOpen) {
      setIsModalOpen(false);
      setBookingId('');
    }
  };

  return (
    <Modal title={'Booking Detail'} open={isModalOpen} onCancel={handleCancel} footer={null} width={1300}>
      {isModalOpen && (
        <Descriptions bordered>
          <Descriptions.Item label="Thông tin" span={4}>
            <div className="flex flex-row justify-between gap-2">
              <div>
                <p>
                  <span className="font-semibold">Mã đặt phòng: </span>
                  {booking[0]?.bookingCode}
                </p>
                <p>
                  <span className="font-semibold">Tên khách sạn: </span>
                  {filterBookings?.hotel?.hotelName}
                </p>
                <p>
                  <span className="font-semibold">Địa chỉ: </span>
                  {filterBookings?.hotel?.hotelAddress}
                </p>
                <p>
                  <span className="font-semibold">Số điện thoại: </span>
                  {booking[0]?.hotelPhoneNumber}
                </p>
              </div>

              <Image src={filterBookings?.hotel?.hotelImage} className="object-cover w-[120px] h-[120px]" />
            </div>
          </Descriptions.Item>
          <Descriptions.Item label="Thông tin lưu trú" span={4}>
            <div className="flex justify-start items-center gap-8">
              <div>
                <p>
                  <span className="font-semibold">Họ tên: </span>
                  {filterBookings?.fullName}
                </p>
                <p>
                  <span className="font-semibold">Điện thoại: </span>
                  {filterBookings?.phoneNumber}
                </p>
                <p>
                  <span className="font-semibold">Email: </span>
                  {filterBookings?.email}
                </p>
              </div>
            </div>
          </Descriptions.Item>
          <Descriptions.Item label="Phòng đặt" span={4}>
            {booking &&
              booking.map((room: any, index: number) => {
                return (
                  <div className="mb-4">
                    <span className="font-semibold">Thông tin phòng:</span>
                    <br />
                    <p>
                      <span className="font-semibold">Tên phòng: </span>
                      {room?.roomName}
                    </p>
                    <p>
                      <span className="font-semibold">Sức chứa: </span>
                      {room?.quantityAdult && room?.quantityAdult} người lớn, {room?.quantityChild} trẻ em
                    </p>
                    <p>
                      <span className="font-semibold">Loại giường: </span>
                      {room?.bedType}
                    </p>

                    <span className="font-semibold">Dịch vụ phòng:</span>
                    <br />
                    <div className="inline">
                      {room?.serviceAmenityRooms?.map((service: any, index: number) => {
                        return (
                          <div className="mr-1">
                            <span>+ {service?.serviceAmenityRoomName}</span>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
          </Descriptions.Item>
        </Descriptions>
      )}
    </Modal>
  );
};

export default DetailBooking;
