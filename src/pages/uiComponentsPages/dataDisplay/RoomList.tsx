import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BasePopconfirm } from '@app/components/common/BasePopconfirm/BasePopconfirm';
import { RoomModel } from '@app/domain/RoomModel';
import {
  doDeleteRoom,
  doGetRoomTypes,
  doGetRooms,
  doUpdateRoom,
  setLanguageCode,
  setRoomId,
  setRoomIdPolicy,
} from '@app/store/slices/roomSlice';
import { Switch, Table } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { FormRoomPolicy } from '../forms/FormRoomPolicy';

interface RoomListProps {
  isModalOpen: boolean;
  setIsModalOpen: (state: boolean) => void;
  hotelId?: string;
}

export const RoomList: React.FC<RoomListProps> = ({ isModalOpen, setIsModalOpen, hotelId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const {
    rooms,
    roomId,
    roomTypes,
    roomIdPolicy,
    loading,
    totalPages,
    currentPage,
    limitPage,
    languageCode,
    roomName,
  } = useSelector((state: any) => state.room);
  const [data, setData] = React.useState<any[]>([]);
  const [isModalOpenRoomPolicy, setIsModalOpenRoomPolicy] = React.useState(false);
  const queryParams = new URLSearchParams(location.search);
  const language = queryParams.get('language');
  const formatter = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
    minimumFractionDigits: 0,
    currencyDisplay: 'symbol',
  });

  useEffect(() => {
    dispatch(doGetRoomTypes({}));
  }, []);

  const getRoomTypeId = (roomName: string) => {
    const roomTypeId = roomTypes.filter((rt) => rt?.name === roomName);
    return roomTypeId[0]?.id;
  };

  React.useEffect(() => {
    dispatch(setLanguageCode(language));
    if (!roomTypes) return;
    setData(
      rooms.map((room: any) => {
        return {
          ...room,
          key: room.id,
          roomId: room.id,
          roomTypeId: getRoomTypeId(room?.roomTypeName),
          roomName: room?.roomName,
          roomNameCustom: room?.roomNameCustom,
          roomType: room?.roomTypeName,
          status: room.status,
          bedName: room.bedName,
          description: room?.description,
          totalBed: room?.singleBedQuantity,
          acreage: room?.roomArea,
          capacity: room?.maxOccupancy,
          quantityRoom: room?.quantityRoom,
          pricePerNight: room?.priceRoom || 0,
        };
      }),
    );
  }, [rooms, roomTypes]);

  const confirm = (e: React.MouseEvent<HTMLElement>, roomId: string) => {
    dispatch(doDeleteRoom({ values: { roomId }, t }));
  };

  const columns = [
    {
      title: 'Room Name',
      dataIndex: 'roomName',
      key: 'roomName',
    },
    {
      title: 'Room Type',
      dataIndex: 'roomType',
      key: 'roomType',
    },
    {
      title: 'RoomName Custom',
      dataIndex: 'roomNameCustom',
      key: 'roomNameCustom',
    },
    {
      title: 'Information Room',
      dataIndex: 'informationRoom',
      key: 'informationRoom',
      render: (_: any, item: any) => {
        return (
          <div className="flex flex-col justify-start items-start">
            <p>Acreage: {item.acreage}m2</p>
            <p>Max Occupancy: {item.capacity}</p>
          </div>
        );
      },
    },
    {
      title: 'Acreage',
      dataIndex: 'roomArea',
      key: 'roomArea',
    },
    {
      title: 'Quantity Room',
      dataIndex: 'quantityRoom',
      key: 'quantityRoom',
    },
    {
      title: 'Price Per Night',
      dataIndex: 'pricePerNight',
      key: 'pricePerNight',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (status: string, record: RoomModel) => (
        <div className="flex justify-between">
          {status === 'ACTIVE' ? (
            <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}
          <PermissionChecker hasPermissions={['room_update_status']}>
            <Switch
              className={`ml-2 ${record.status === 'ACTIVE' ? 'bg-blue-500' : 'bg-gray-500'}`}
              checked={record.status === 'ACTIVE' ? true : false}
              loading={loading}
              onChange={(e) => {
                const data = {
                  roomId: record.roomId,
                  roomTypeId: record?.roomTypeId,
                  pricePerNight: record.pricePerNight,
                  roomName: record.roomName,
                  roomNameCustom: record.roomNameCustom,
                  quantityRoom: record.quantityRoom,
                  roomArea: record.acreage,
                  maxOccupancy: record.capacity,
                  bedName: record.bedName,
                  status: e ? 'ACTIVE' : 'NO_ACTIVE',
                };
                dispatch(
                  doUpdateRoom({
                    values: data,
                    t,
                  }),
                );
              }}
            />
          </PermissionChecker>
        </div>
      ),
    },
    {
      title: 'Function',
      dataIndex: 'function',
      key: 'function',
      render: (_: any, item: any) => (
        <div className="flex justify-center items-center gap-4">
          <BaseButton
            type="ghost"
            onClick={() => {
              dispatch(setRoomId(item.roomId));
              setIsModalOpen(true);
            }}
          >
            Detail
          </BaseButton>
          <PermissionChecker hasPermissions={['room_delete']}>
            <BasePopconfirm
              title="Are you sure delete the room?"
              onConfirm={(e?: React.MouseEvent<HTMLElement>) => {
                if (e) {
                  confirm(e, item.roomId);
                }
              }}
              okText="Yes"
              cancelText="No"
            >
              <BaseButton type="primary">Delete</BaseButton>
            </BasePopconfirm>
          </PermissionChecker>
          {/* <PermissionChecker
                        hasPermissions={['room_view_policy', 'room_create_policy', 'room_update_policy']}
                    >
                        <BaseButton
                            type="ghost"
                            onClick={() => {
                                setIsModalOpenRoomPolicy(true);
                                dispatch(setRoomIdPolicy(item.roomId));
                            }}
                        >
                            Policy
                        </BaseButton>
                    </PermissionChecker> */}
        </div>
      ),
    },
  ];
  return (
    <>
      <Table
        dataSource={data}
        columns={columns}
        loading={loading}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(
              doGetRooms({ values: { hotelId, currentPage: page, limitPage: pageSize, languageCode, roomName }, t }),
            );
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
      <FormRoomPolicy
        isModalOpen={isModalOpenRoomPolicy}
        setIsModalOpen={setIsModalOpenRoomPolicy}
        roomId={roomIdPolicy}
      />
    </>
  );
};

export default RoomList;
