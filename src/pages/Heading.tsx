import React, { useEffect, useState } from 'react';
import { Input, Checkbox, Table, Modal, Switch, Form, Badge, Select } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { doChangeStatus, doDeleteHeadings, doGetHeadings, doUpdateHeadings } from '@app/store/slices/Headingslice';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import dayjs from 'dayjs';
type HeadingProps = {};

const Heading: React.FC<HeadingProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [data, setData] = React.useState<any>([]);
  const { headings, loading, totalPages, currentPage, titleHeading, languageCode, limitPage } = useSelector(
    (state: any) => state.heading,
  );

  const lng = localStorage.getItem('lng');

  const formatDate = (date: any) => {
    return dayjs(date).format('DD-MM-YYYY');
  };

  React.useEffect(() => {
    dispatch(
      doGetHeadings({
        currentPage,
        limitPage,
        t,
      }),
    );
  }, [dispatch, titleHeading, languageCode, currentPage, limitPage, lng]);

  React.useEffect(() => {
    const result = headings.map((item: any, index: number) => {
      return {
        index: -(-(currentPage - 1) * limitPage - (index + 1)),
        id: item.parentId,
        key: item.headingId,
        createdBy: item.createdBy,
        urlHeading: item.urlHeading,
        language: item.languageCode,
        titleHeading: item.titleHeading,
        lastModifiedBy: item.lastModifiedBy,
        createdDateL: item.createdDate,
        status: item.status,
        edit: false,
      };
    });
    setData(result);
  }, [headings]);

  const handleDelete = (item: any) => {
    dispatch(doDeleteHeadings({ id: item?.id, t }));
    setData(headings);
  };

  const handleChange = (value: string, item: any) => {
    dispatch(doChangeStatus({ id: item.id, status: value }));
    item.status = value;
    item.edit = false;
    setData([...data]);
  };

  const columns = [
    {
      title: t('headingcontent.title_heading'),
      dataIndex: 'titleHeading',
      key: 'titleHeading',
    },
    {
      title: t('headingcontent.language'),
      dataIndex: 'language',
      key: 'language',
    },

    {
      title: t('headingcontent.createDate'),
      dataIndex: 'createdDateL',
      key: 'createdDateL',
      render: (value: any) => {
        return formatDate(value);
      },
    },

    {
      title: t('headingcontent.url_heading'),
      dataIndex: 'urlHeading',
      key: 'urlHeading',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text: string, record: any) => (
        <div className="flex justify-between">
          {record.status === 'ACTIVE' ? (
            <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}

          <Switch
            className={`ml-2 ${record.status === 'ACTIVE' ? 'bg-blue-500' : 'bg-gray-500'}`}
            checked={record.status === 'ACTIVE' ? true : false}
            loading={loading}
            onChange={(e) => {
              dispatch(
                doChangeStatus({
                  values: {
                    parentId: record.id,
                    status: e ? 'ACTIVE' : 'UN_ACTIVE',
                  },
                  t,
                }),
              );
            }}
          />
        </div>
      ),
    },

    {
      title: t('headingcontent.funciton'),
      dataIndex: 'function',
      key: 'function',
      render: (text: string, record: any) => (
        <div className="flex justify-center items-center gap-4 uppercase">
          <PermissionChecker hasPermissions={['content_update']}>
            <Link to={`/manage/heading/parent/${record.id}`} type="ghost">
              <BaseButton type="ghost">
                <EditOutlined />
              </BaseButton>
            </Link>
          </PermissionChecker>
          <PermissionChecker hasPermissions={['content_delete']}>
            <BaseButton
              type="primary"
              htmlType="submit"
              onClick={() => {
                handleDelete(record);
              }}
            >
              <DeleteOutlined />
            </BaseButton>
          </PermissionChecker>
        </div>
      ),
    },
  ];

  return (
    <div className="uppercase">
      <h3 className="text-2xl font-bold uppercase">{t('headingcontent.heading')}</h3>
      <br />
      <div className="flex flex-wrap md:flex-nowrap items-center">
        <div className="flex flex-wrap md:flex-nowrap items-center w-full gap-4">
          <div className="w-[280px]"></div>
          <div className="w-full flex flex-nowrap gap-5">
            <PermissionChecker hasPermissions={['content_write']}>
              <div className="ml-auto">
                <Link to={'/manage/heading/add'}>
                  <BaseButton className="uppercase" type="primary">
                    {t('headingcontent.button.addheading')}
                  </BaseButton>
                </Link>
              </div>
            </PermissionChecker>
          </div>
        </div>
      </div>
      <Table
        dataSource={data}
        columns={columns}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(doGetHeadings({ currentPage: page, limitPage: pageSize, t: t }));
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
    </div>
  );
};
export default Heading;
