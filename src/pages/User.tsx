import React, { useEffect, useState } from 'react';
import { Input, Checkbox, Table, Modal, Switch, Form } from 'antd';
import {
  ClusterOutlined,
  EditOutlined,
  HomeOutlined,
  LockOutlined,
  MailOutlined,
  PhoneOutlined,
  ReloadOutlined,
  SettingOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { doGetUsers, doAddUser, doActiveUser, doDeleteUser, doUpdateUser } from '@app/store/slices/userSlice';
import { PermissionChecker } from '@app/components/auth/PermissionChecker/PermissionChecker';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const User = () => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const paramsActive = new URLSearchParams(location.search).get('active') || 'ACTIVE';
  const paramsIsHotelOwner = new URLSearchParams(location.search).get('isHotelOwner') || 'ACTIVE';
  const { users, permissions, loading, totalPages, currentPage, limitPage } = useSelector((state: any) => state.user);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const [dataSource, setDataSource] = useState([]);
  const [email, setEmail] = useState('');
  const [userFilter, setUserFilter] = useState([]);

  useEffect(() => {
    permissions.forEach((element: { permissionCode: string }) => {
      (element.permissionCode === 'admin_read' || element.permissionCode === 'admin_master') &&
        dispatch(doGetUsers({ currentPage, limitPage, status: paramsActive, isHotelOwner: paramsIsHotelOwner, t }));
    });
  }, [dispatch, currentPage, limitPage]);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  const onFinish = (values: any) => {
    dispatch(doAddUser({ values, t }));
    setIsModalOpen(false);
    form.resetFields();
    setIsButtonDisabled(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    form.resetFields();
    setIsButtonDisabled(true);
  };

  const handleChangeSearch = (value: any) => {
    setEmail(value);
  };

  const onChangeChecked = (value: boolean) => {
    const status = value ? 'ACTIVE' : 'NO_ACTIVE';

    const queryParams = new URLSearchParams(location.search);
    queryParams.set('active', status);
    const params = queryParams.get('active');

    navigate(`${location.pathname}?${queryParams.toString()}`);

    dispatch(doGetUsers({ currentPage, limitPage, status: params, isHotelOwner: paramsIsHotelOwner, t: t }));
  };

  const onChangeCheckedHotelOwner = (value: boolean) => {
    const status = value ? 'ACTIVE' : 'NO_ACTIVE';
    const queryParams = new URLSearchParams(location.search);
    queryParams.set('isHotelOwner', status);
    const params = queryParams.get('isHotelOwner');

    navigate(`${location.pathname}?${queryParams.toString()}`);
    dispatch(doGetUsers({ currentPage, limitPage, status: paramsActive, isHotelOwner: params, t: t }));
  };

  const handleFindUserByEmail = () => {
    if (!email) {
      const data = users.map((user: any, index: number) => {
        return {
          key: -(-(currentPage - 1) * limitPage - (index + 1)),
          id: user?.id,
          firstName: user?.firstName,
          lastName: user?.lastName,
          email: user?.email,
          phone: user?.phoneNumber,
          status: user?.status,
        };
      });
      setDataSource(data);
      return;
    }

    const result = users.filter((item: any) => {
      return item.email.includes(email);
    });

    const data = result.map((user: any, index: number) => {
      return {
        key: -(-(currentPage - 1) * limitPage - (index + 1)),
        id: user?.id,
        firstName: user?.firstName,
        lastName: user?.lastName,
        email: user?.email,
        phone: user?.phoneNumber,
        status: user?.status,
      };
    });
    setDataSource(data);
  };

  useEffect(() => {
    dispatch(doGetUsers({ currentPage, limitPage, status: paramsActive, isHotelOwner: paramsIsHotelOwner, t: t }));
  }, []);

  useEffect(() => {
    const data = users.map((user: any, index: number) => {
      return {
        key: -(-(currentPage - 1) * limitPage - (index + 1)),
        id: user?.id,
        firstName: user?.firstName,
        lastName: user?.lastName,
        email: user?.email,
        phone: user?.phoneNumber,
        status: user?.status,
      };
    });
    setDataSource(data);
  }, [users, currentPage, limitPage]);

  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: t('user.firstName'),
      dataIndex: 'firstName',
      key: 'firstName',
    },
    {
      title: t('user.lastName'),
      dataIndex: 'lastName',
      key: 'lastName',
    },
    {
      title: t('user.phone'),
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: t('user.status'),
      dataIndex: 'status',
      key: 'status',
      render: (text: string, record: any) => (
        <span className="flex items-center justify-between">
          {record.status === 'ACTIVE' ? (
            <span className="ms-3 text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="ms-3 text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}
          <PermissionChecker hasPermissions={['admin_update']}>
            <Switch
              className={`ml-2 ${record.status === 'NO_ACTIVE' ? 'bg-gray-500' : 'bg-blue-500'}`}
              checked={record.status === 'ACTIVE' ? true : false}
              onChange={(e) => {
                const values = {
                  userId: record.id,
                  status: e ? 'ACTIVE' : 'NO_ACTIVE',
                };
                dispatch(doUpdateUser({ values, t }));
              }}
              loading={loading}
            />
          </PermissionChecker>
        </span>
      ),
    },
    {
      title: t('user.action'),
      key: 'action',

      render: (text: any, record: any) => {
        return (
          <div className="flex gap-5">
            <PermissionChecker hasPermissions={['admin_write']}>
              <Link to={`user-group/${record.id}`}>
                <BaseButton type="ghost" icon={<ClusterOutlined />} />
              </Link>
            </PermissionChecker>
            {/* <BaseButton
            type="primary"
            icon={<EditOutlined />}
          /> */}
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <h3 className="text-2xl font-bold capitalize">{t('user.user')}</h3>
      <div>
        <h6 className="text-base text-sky-700">{t('user.find')}</h6>
        <div className="flex flex-wrap md:flex-nowrap items-center">
          <div className="w-full">
            <Input
              value={email}
              onChange={(e) => handleChangeSearch(e.target.value)}
              placeholder={t('user.input.placeholder_user')}
            />
          </div>
          <div className="flex flex-row gap-4 items-center w-full">
            <div className="w-full px-3">
              <Checkbox
                checked={paramsActive === 'ACTIVE' ? true : false}
                onChange={(e) => onChangeChecked(e.target.checked)}
              />
              <span className="ml-2">{t('user.input.checked')}</span>
            </div>
            <div className="w-full px-3">
              <Checkbox
                checked={paramsIsHotelOwner === 'ACTIVE' ? true : false}
                onChange={(e) => onChangeCheckedHotelOwner(e.target.checked)}
              />
              <span className="ml-2">Is Hotel Owner</span>
            </div>
          </div>
          <div className="w-full flex flex-nowrap gap-5">
            <BaseButton type="primary" className="ml-auto" onClick={handleFindUserByEmail}>
              {t('user.button.find_user')}
            </BaseButton>
            <div className="border-solid border-[1px] border-zinc-400 my-3"></div>
            <PermissionChecker hasPermissions={['admin_write']}>
              <BaseButton type="default" onClick={showModal}>
                {t('user.button.add_user')}
              </BaseButton>
              <div className="border-solid border-[1px] border-zinc-400 my-3"></div>
            </PermissionChecker>
            <ReloadOutlined
              className="text-2xl"
              onClick={() =>
                dispatch(
                  doGetUsers({ currentPage, limitPage, status: paramsActive, isHotelOwner: paramsIsHotelOwner, t }),
                )
              }
            />
          </div>
        </div>
        <PermissionChecker hasPermissions={['admin_read']}>
          <Table
            className="mt-4"
            dataSource={dataSource}
            columns={columns}
            loading={loading}
            pagination={{
              showSizeChanger: true,
              pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
              defaultPageSize: limitPage,
              defaultCurrent: currentPage,
              total: totalPages * limitPage,
              onChange: (page: number, pageSize: number) => {
                dispatch(doGetUsers({ currentPage: page, limitPage: pageSize, t: t }));
              },
            }}
          />
        </PermissionChecker>
      </div>
      <Modal title={t('user.modal.title')} open={isModalOpen} onOk={onFinish} onCancel={handleCancel} footer={null}>
        <Form
          name="user-form"
          onFinish={onFinish}
          layout="vertical"
          form={form}
          onValuesChange={(e) => {
            if (
              form.getFieldValue('firstName') &&
              form.getFieldValue('lastName') &&
              form.getFieldValue('email') &&
              form.getFieldValue('phoneNumber') &&
              form.getFieldValue('password') &&
              form.getFieldValue('confirm')
            ) {
              form
                .validateFields()
                .then((data) => {
                  setIsButtonDisabled(false);
                })
                .catch((err: any) => {
                  if (err.errorFields.length > 0) setIsButtonDisabled(true);
                  else setIsButtonDisabled(false);
                });
            }
          }}
        >
          <Form.Item
            name="firstName"
            rules={[
              {
                required: true,
                message: t('user.modal.message.user_message'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder={t('user.modal.input.placeholder_user')}
              onBlur={handleBlur('firstName')}
            />
          </Form.Item>
          <Form.Item
            name="lastName"
            rules={[
              {
                required: true,
                message: t('user.modal.message.user_message'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder={t('user.modal.input.placeholder_user')}
              onBlur={handleBlur('lastName')}
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: t('user.modal.message.email_message1'),
              },
              {
                type: 'email',
                message: t('user.modal.message.email_message2'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="Email"
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
                if (e.key === '+') {
                  e.preventDefault();
                }
              }}
              onBlur={handleBlur('email')}
            />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: t('user.modal.message.phone_message'),
              },
              {
                pattern: /^(\+[0-9]{11}|0[0-9]{9})$/,
                message: t('user.modal.message.phone_message1'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<PhoneOutlined className="site-form-item-icon" />}
              placeholder={t('user.modal.input.placeholder_phone')}
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
              }}
              onChange={(e) => {
                const value = e.target.value;
                const filteredValue = value.replace(/[^0-9+]/g, '');

                form.setFieldsValue({ phoneNumber: filteredValue });
              }}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              //password has to have at least one uppercase, one lowercase, one number and one special character
              {
                validator: (_, value) => {
                  if (!value) {
                    return Promise.reject(t('user.modal.message.password_message'));
                  } else if (value.length < 8) {
                    return Promise.reject(t('user.modal.message.password_message1'));
                  } else if (!/(?=.*[A-Z])/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message2'));
                  } else if (!/(?=.*[a-z])/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message3'));
                  } else if (!/(?=.*\d)/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message4'));
                  } else if (!/(?=.*[^\da-zA-Z])/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message5'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={t('user.modal.input.placeholder_password')}
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
              }}
              onBlur={handleBlur('password')}
            />
          </Form.Item>
          <Form.Item
            name="confirm"
            dependencies={['password']}
            rules={[
              {
                required: true,
                message: t('user.modal.message.password_retype_massager'),
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error(t('user.modal.message.password_retype_massager2')));
                },
              }),
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={t('user.modal.input.placeholder_password_retype')}
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
              }}
              onBlur={handleBlur('confirm')}
            />
          </Form.Item>

          <Form.Item>
            <div className="flex justify-center items-center gap-4">
              <BaseButton type="ghost" onClick={handleCancel}>
                {t('user.modal.button.cancel')}
              </BaseButton>
              <BaseButton type="primary" htmlType="submit" disabled={isButtonDisabled}>
                {t('user.modal.button.save')}
              </BaseButton>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default User;
