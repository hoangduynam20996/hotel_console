import React from 'react';
import { useTranslation } from 'react-i18next';
import { Error } from '@app/components/Error/Error';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import error403 from '@app/assets/images/error403.png';

const Error403Page: React.FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <PageTitle>{'403'}</PageTitle>
      <Error img={error403} msg={t('error403.message')} />
    </>
  );
};

export default Error403Page;
