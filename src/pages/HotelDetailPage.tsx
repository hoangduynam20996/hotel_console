import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import {
  doGetHotel,
  doCreateHotel,
  doUpdateHotel,
  doGetServicesAmenity,
  doGetProvinces,
  doGetHotelTypes,
} from '@app/store/slices/hotelSlice';
import { deleteImageById, doGetHotelImages, uploadImage } from '@app/store/slices/imageSlice';
import { Checkbox, Form, Input, InputNumber, Rate, Select, SelectProps, Upload, UploadFile, message } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { PlusOutlined } from '@ant-design/icons';
import { httpApi } from '@app/api/http.api';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

interface HotelDetailPageProps {}

interface FileItem {
  uid: string | number;
  name: string;
  status: string;
  url: string;
}

export const HotelDetailPage: React.FC<HotelDetailPageProps> = (props) => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const id = searchParams.get('id');
  const currentLocation = searchParams.get('location');
  const currentCheckIn = searchParams.get('checkin');
  const currentCheckout = searchParams.get('checkout');
  const currentAdults = searchParams.get('group_adults');
  const currentChildren = searchParams.get('group_children');
  const currentRooms = searchParams.get('group_rooms');
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { hotel, servicesAmenity, servicesHotelTypes, languageCode, provinces } = useSelector(
    (state: any) => state.hotel,
  );
  const [fileList, setFileList] = React.useState<FileItem[]>([]);
  const { images } = useSelector((state: any) => state.image);

  React.useEffect(() => {
    if (id) {
      const params = {
        hotelId: id,
      };
      dispatch(doGetHotel({ params }));
      dispatch(doGetHotelImages({ values: { hotelId: id, currentPage: 1, limitPage: 9999 }, t }));
    }
    dispatch(doGetHotelTypes());
  }, [dispatch, id]);

  React.useEffect(() => {
    if (hotel && id) {
      form.setFieldsValue({
        name: hotel?.name,
        streetAddress: hotel?.streetAddress,
        districtAddress: hotel?.districtAddress,
        city: hotel?.city,
        country: hotel?.country,
        status: hotel?.status,
        rating: hotel?.rating,
        description: hotel?.description,
        contactPersonName: hotel?.contactPerson,
        phoneNumber: hotel?.phoneNumber,
        phoneNumberTwo: hotel?.phoneNumberTwo,
        postalCode: hotel.postalCode,
      });
    }
  }, [hotel, form]);

  React.useEffect(() => {
    if (images) {
      setFileList(
        images.map((image: any, index: number) => ({
          id: image.id,
          name: image?.name,
          status: 'done',
          url: image?.urlImage,
        })),
      );
    } else {
      setFileList([]);
    }
  }, [images]);

  const optionsAmenity: SelectProps['options'] = [];
  if (servicesAmenity) {
    servicesAmenity.forEach((serviceAmenity: any) => {
      optionsAmenity.push({
        label: serviceAmenity?.name,
        value: serviceAmenity?.id,
      });
    });
  }

  const handleChange = (value: string[]) => {
    // console.log(`selected ${value}`);
  };

  const onFinish = async (values: any) => {
    if (id) {
      await dispatch(doUpdateHotel({ values: { hotelId: id, hotelTypeId: hotel?.hotelTypeId, ...values }, t }));
    }
    // else {
    //     await dispatch(doCreateHotel({ values: { ...data }, t }));
    // }
    await navigate('/hotels');
  };

  const handleUploadImage = (file: any) => {
    const formData = new FormData();
    formData.append('images', file);
    formData.append('hid', id);
    dispatch(uploadImage(formData));
  };

  const handleDeleteImage = (id: string) => {
    dispatch(deleteImageById(id));
    setFileList(fileList.filter((file: any) => file.id !== id));
  };

  const uploadButton = (
    <button style={{ border: 0, background: 'none' }} type="button">
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </button>
  );

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  type FieldType = {
    hotelName?: string;
    streetAddress?: string;
    districtAddress?: string;
    city?: string;
    description?: string;
    rating?: number;
    price?: number;
    serviceAmenity?: string[];
    images?: any;
  };

  return (
    <>
      <div className="flex justify-between item-center">
        <h3 className="text-2xl font-bold capitalize">Hotel Detail</h3>
        <Link to="/hotels" className="text-blue-500">
          Back to hotels
        </Link>
      </div>
      <Form
        name="basic"
        form={form}
        layout="vertical"
        // initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        {/* <Form.Item
                            name="hotelType"
                            label="Hotel Type"
                            rules={[{ required: true, message: 'Please input the hotel types!' }]}
                        >
                            <Select>
                            {
                                    servicesHotelTypes?.map((type: any) => (
                                        <Select.Option key={type?.id} value={type?.id}>
                                            {type?.name}
                                        </Select.Option>
                                    ))
                                }
                            </Select>
                </Form.Item> */}

        <Form.Item<FieldType>
          label={t('detailshotel.hotelName')}
          name="name"
          rules={[{ required: true, message: 'Please input your hotel name!' }]}
        >
          <Input onBlur={handleBlur('name')} />
        </Form.Item>
        <Form.Item<FieldType>
          label={`Street Address`}
          name="streetAddress"
          rules={[{ required: true, message: 'Please input your streetAddress!' }]}
        >
          <Input onBlur={handleBlur('streetAddress')} />
        </Form.Item>
        <Form.Item<FieldType>
          label={`District Address`}
          name="districtAddress"
          rules={[{ required: true, message: 'Please input your districtAddress!' }]}
        >
          <Input onBlur={handleBlur('districtAddress')} />
        </Form.Item>

        <Form.Item<FieldType>
          label={`City`}
          name="city"
          rules={[{ required: true, message: 'Please input your city!' }]}
        >
          <Input onBlur={handleBlur('city')} />
        </Form.Item>

        <Form.Item<FieldType>
          label={`Country`}
          name="country"
          rules={[{ required: true, message: 'Please input your country!' }]}
        >
          <Input onBlur={handleBlur('country')} />
        </Form.Item>
        <Form.Item<FieldType>
          label={`Postal Code`}
          name="postalCode"
          rules={[{ required: true, message: 'Please input your postalCode!' }]}
        >
          <InputNumber onBlur={handleBlur('postalCode')} />
        </Form.Item>
        <Form.Item<FieldType>
          label={`Contact Person`}
          name="contactPersonName"
          rules={[{ required: true, message: 'Please input Contact Person!' }]}
        >
          <Input onBlur={handleBlur('contactPersonName')} />
        </Form.Item>
        <Form.Item<FieldType>
          label={`Phone Number`}
          name="phoneNumber"
          rules={[
            {
              required: true,
              message: t('user.modal.message.phone_message'),
            },
            {
              pattern: /^(\+[0-9]{11}|0[0-9]{9})$/,
              message: t('user.modal.message.phone_message1'),
            },
          ]}
        >
          <Input type="number" className="w-full" onBlur={handleBlur('phoneNumber')} />
        </Form.Item>
        <Form.Item<FieldType>
          label={`Phone Number Two`}
          name="phoneNumberTwo"
          rules={[
            {
              required: true,
              message: t('user.modal.message.phone_message'),
            },
            {
              pattern: /^(\+[0-9]{11}|0[0-9]{9})$/,
              message: t('user.modal.message.phone_message1'),
            },
          ]}
        >
          <Input type="number" className="w-full" onBlur={handleBlur('phoneNumberTwo')} />
        </Form.Item>

        <Form.Item<FieldType>
          className="w-1/4"
          label={t('detailshotel.star')}
          name="rating"
          rules={[{ required: true, message: 'Please input your star!' }]}
        >
          <Rate style={{ fontSize: '30px' }} />
        </Form.Item>
        {id && (
          <PermissionChecker hasPermissions={['hotel_update_info']}>
            <Form.Item label="Images">
              <Upload
                action={(file: File) => {
                  handleUploadImage(file);
                }}
                beforeUpload={(file: File) => {
                  const acceptedTypes = ['image/jpeg', 'image/png'];
                  const isFileTypeAccepted = acceptedTypes.includes(file.type);
                  if (!isFileTypeAccepted) {
                    message.error('Only JPG and PNG files are allowed!');
                  }
                  return isFileTypeAccepted;
                }}
                onRemove={(file: any) => {
                  handleDeleteImage(file.id);
                }}
                headers={{
                  'Content-Type': 'multipart/form-data',
                }}
                maxCount={10}
                fileList={fileList as UploadFile<any>[]}
                multiple
                listType="picture-card"
              >
                {uploadButton}
              </Upload>
            </Form.Item>
          </PermissionChecker>
        )}

        <Form.Item<FieldType>
          label={t('detailshotel.description')}
          name="description"
          // rules={[{ required: true, message: 'Please input your description!' }]}
        >
          <Input.TextArea maxLength={255} />
        </Form.Item>

        <PermissionChecker hasPermissions={['hotel_update_info']}>
          <Form.Item>
            <BaseButton type="primary" htmlType="submit">
              {id ? 'Update' : 'Create'}
            </BaseButton>
          </Form.Item>
        </PermissionChecker>
      </Form>
    </>
  );
};

export default HotelDetailPage;
