import { httpApi } from '@app/api/http.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { doCreateContents, doGetContent, doUpdateContents } from '@app/store/slices/ContentSlice';
import {
  doCreateDescription,
  dodeleteDescription,
  dogetByParent,
  doupdateDescription,
} from '@app/store/slices/DescriptionSlice';
import { doCreateHeadings, doGetHeading, doGetHeadings, doUpdateHeadings } from '@app/store/slices/Headingslice';
import { Form, Input, InputNumber, Rate, Select, Upload, message } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useNavigate, useParams } from 'react-router-dom';

interface DescriptionDetailPopr {}

interface FileItem {
  uid: string | number;
  titleDescription: string;
  contentDescription: string;
  name: string;
}

type FieldType = {
  titleDescriptionVI?: string;
  titleDescriptionEN?: string;
  languageCodeVI?: string; // Language code for Vietnamese
  languageCodeEN?: string; // Language code for English
  contentDescriptionVI?: string;
  contentDescriptionEN?: string;
};

export const DescriptionDetails: React.FC<DescriptionDetailPopr> = (props) => {
  const { parentId } = useParams();
  const [form] = Form.useForm();
  const nivagate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { description, languageCode } = useSelector((state: any) => state.description);
  const lang = localStorage.getItem('lng');

  React.useEffect(() => {
    if (parentId) {
      dispatch(dogetByParent({ parentId, languageCode, t }));
    } else {
      form.resetFields();
    }
  }, [dispatch, parentId]);

  React.useEffect(() => {
    if (description && parentId) {
      description.map((item: any) => {
        item.languageCode === 'EN'
          ? form.setFieldsValue({
              titleDescriptionEN: item.titleDescription,
              languageCodeEN: item.languageCode,
              contentDescriptionEN: item.contentDescription,
            })
          : form.setFieldsValue({
              titleDescriptionVI: item.titleDescription,
              languageCodeVI: item.languageCode,
              contentDescriptionVI: item.contentDescription,
            });
      });
    }
  }, [description, lang, form]);

  const onFinish = async (values: any) => {
    console.log(values);
    const payload = {
      languageRequests: [
        {
          languageCode: 'EN',
          titleDescription: values.titleDescriptionEN,
          contentDescription: values.contentDescriptionEN,
        },
        {
          languageCode: 'VI',
          titleDescription: values.titleDescriptionVI,
          contentDescription: values.contentDescriptionVI,
        },
      ],
    };

    if (parentId) {
      await dispatch(doupdateDescription({ parentId, ...payload, t }));
    } else {
      await dispatch(doCreateDescription({ ...payload, t }));
    }

    await nivagate('/manage/descriptions');
  };

  return (
    <>
      <div className="flex justify-between item-center uppercase">
        <h3 className="text-2xl font-bold">{t('description_details.description')}</h3>
        <Link to="/manage/descriptions" className="text-blue-500">
          {t('description_details.back')}
        </Link>
      </div>
      <br />
      <Form
        name="basic"
        form={form}
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        className="uppercase"
      >
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label="Language"
            name="languageCodeEN"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="English"
          >
            <Input defaultValue="EN" disabled />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('description_details.language')}
            name="languageCodeVI"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="Việt Nam"
          >
            <Input defaultValue="VI" disabled />
          </Form.Item>
        </div>

        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label="Title Description"
            name="titleDescriptionEN"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('description_details.titleDescription')}
            name="titleDescriptionVI"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label="Content Description"
            name="contentDescriptionEN"
            rules={[{ required: true, message: 'Please input Content Description!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('description_details.contentDescription')}
            name="contentDescriptionVI"
            rules={[{ required: true, message: 'Please input Content Description!' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <br />
        <Form.Item>
          <BaseButton type="primary" htmlType="submit" className="uppercase">
            {parentId ? t('description_details.button.update') : t('description_details.button.create')}
          </BaseButton>
        </Form.Item>
      </Form>
    </>
  );
};

export default DescriptionDetails;
