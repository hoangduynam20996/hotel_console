import React, { useEffect, useState } from 'react';
import { Switch, Table } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { doChangeStatusDes, dodeleteDescription, dogetDescription } from '@app/store/slices/DescriptionSlice';
import { Link } from 'react-router-dom';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

type DescriptionProps = {};

const Descriptions: React.FC<DescriptionProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [data, setData] = React.useState<any>([]);
  const lang = localStorage.getItem('lng');
  const { descriptions, loading, totalPages, currentPage, limitPage } = useSelector((state: any) => state.description);

  const formatDate = (date: any) => {
    return dayjs(date).format('DD-MM-YYYY');
  };

  React.useEffect(() => {
    dispatch(
      dogetDescription({
        currentPage,
        limitPage,
        t,
      }),
    );
  }, [dispatch, currentPage, limitPage, lang]);

  const handleDelete = (item: any) => {
    dispatch(dodeleteDescription({ id: item?.id, t }));
    setData(descriptions);
  };

  React.useEffect(() => {
    const result = descriptions.map((item: any, index: number) => {
      return {
        index: -(-(currentPage - 1) * limitPage - (index + 1)),
        id: item.parentId,
        key: item.descriptionId,
        createdBy: item.createdBy,
        titleDescription: item.titleDescription,
        language: item.languageCode,
        contentDescription: item.contentDescription,
        lastModifiedBy: item.lastModifiedBy,
        createdDateL: item.createdDate,
        status: item.status,
        edit: false,
      };
    });
    setData(result);
  }, [descriptions]);

  const columns = [
    {
      title: t('description.title_description'),
      dataIndex: 'titleDescription',
      key: 'titleDescription',
    },
    {
      title: t('description.language'),
      dataIndex: 'language',
      key: 'language',
    },

    {
      title: t('description.createDate'),
      dataIndex: 'createdDateL',
      key: 'createdDateL',
      render: (value: any) => {
        return formatDate(value);
      },
    },

    {
      title: t('description.content_descripton'),
      dataIndex: 'contentDescription',
      key: 'contentDescription',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text: string, record: any) => (
        <div className="flex justify-between">
          {record.status === 'ACTIVE' ? (
            <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}

          <Switch
            className={`ml-2 ${record.status === 'ACTIVE' ? 'bg-blue-500' : 'bg-gray-500'}`}
            checked={record.status === 'ACTIVE' ? true : false}
            loading={loading}
            onChange={(e) => {
              dispatch(
                doChangeStatusDes({
                  values: {
                    parentId: record.id,
                    status: e ? 'ACTIVE' : 'UN_ACTIVE',
                  },
                  t,
                }),
              );
            }}
          />
        </div>
      ),
    },
    {
      title: t('description.funciton'),
      dataIndex: 'function',
      key: 'function',
      render: (value: string, index: any) => (
        <div className="flex justify-center items-center gap-4">
          <PermissionChecker hasPermissions={['content_update']}>
            <Link to={`/manage/des/parent/${index.id}`}>
              <BaseButton type="ghost">
                <EditOutlined />
              </BaseButton>
            </Link>
          </PermissionChecker>
          <PermissionChecker hasPermissions={['content_delete']}>
            <BaseButton
              type="primary"
              onClick={() => {
                handleDelete(index);
              }}
            >
              <DeleteOutlined />
            </BaseButton>
          </PermissionChecker>
        </div>
      ),
    },
  ];

  return (
    <div className="uppercase">
      <h3 className="text-2xl font-bold uppercase">{t('description.descriptions')}</h3>
      <br />
      <div className="flex flex-wrap md:flex-nowrap items-center ">
        <div className="flex flex-wrap md:flex-nowrap items-center w-full gap-4">
          <div className="w-full flex flex-nowrap gap-5">
            <PermissionChecker hasPermissions={['content_write']}>
              <div className="ml-auto">
                <Link to={'/manage/des/add/'}>
                  <BaseButton className="uppercase" type="primary">
                    {t('description.button.add_description')}
                  </BaseButton>
                </Link>
              </div>
            </PermissionChecker>
          </div>
        </div>
      </div>
      <Table
        dataSource={data}
        columns={columns}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(dogetDescription({ currentPage: page, limitPage: pageSize, t: t }));
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
    </div>
  );
};

export default Descriptions;
