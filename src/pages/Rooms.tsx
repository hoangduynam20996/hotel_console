import React, { useEffect } from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { doGetRooms, setRoomId } from '@app/store/slices/roomSlice';
import FilterRoomsPage from './uiComponentsPages/forms/FilterRoomsPage';
import RoomList from './uiComponentsPages/dataDisplay/RoomList';
import AddRoom from './uiComponentsPages/forms/AddRoom';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import { doGetHotelImages } from '@app/store/slices/imageSlice';
import { doGetServicesAmenity } from '@app/store/slices/serviceAmenitySlice';
import { AddServiceAmenityRoom } from './uiComponentsPages/forms/AddServiceAmenityRoom';

export const Rooms: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { id } = useParams();
  const { rooms, roomId, loading, totalPages, currentPage, limitPage, languageCode } = useSelector(
    (state: any) => state.room,
  );
  const [isModalOpenRoom, setIsModalOpenRoom] = React.useState(false);
  const [isModalOpenServiceAmenity, setIsModalOpenServiceAmenity] = React.useState(false);
  const [status, setStatus] = React.useState('ACTIVE');
  const [roomName, setRoomName] = React.useState('');

  React.useEffect(() => {
    if (id) {
      dispatch(doGetRooms({ values: { hotelId: id, roomName, status, currentPage, limitPage }, t }));
      dispatch(doGetHotelImages({ values: { hotelId: id, status, currentPage, limitPage }, t }));
    }
  }, [dispatch, id, currentPage, limitPage, status, roomName]);

  useEffect(() => {
    dispatch(doGetServicesAmenity());
  }, [dispatch]);

  return (
    <>
      <h3 className="text-2xl font-bold capitalize">Room List</h3>
      <div className="flex flex-wrap md:flex-nowrap items-center my-3">
        <div className="flex flex-wrap md:flex-nowrap items-center w-full">
          <FilterRoomsPage
            hotelId={id}
            status={status}
            roomName={roomName}
            setRoomName={setRoomName}
            setStatus={setStatus}
          />
          <div className="w-full flex flex-nowrap gap-1 ml-auto justify-end">
            <PermissionChecker hasPermissions={['room_create_info']}>
              <BaseButton
                type="primary"
                onClick={() => {
                  dispatch(setRoomId(''));
                  setIsModalOpenRoom(true);
                }}
              >
                Add room
              </BaseButton>
            </PermissionChecker>
            <PermissionChecker hasPermissions={['room_create_info']}>
              <BaseButton
                type="primary"
                onClick={() => {
                  setIsModalOpenServiceAmenity(true);
                }}
              >
                Add Service Amenity
              </BaseButton>
            </PermissionChecker>
          </div>
        </div>
      </div>
      <RoomList isModalOpen={isModalOpenRoom} setIsModalOpen={setIsModalOpenRoom} hotelId={id} />
      <AddRoom
        isModalOpen={isModalOpenRoom}
        setIsModalOpen={setIsModalOpenRoom}
        roomId={roomId}
        hotelId={id}
        language={languageCode}
      />

      <AddServiceAmenityRoom isModalOpen={isModalOpenServiceAmenity} setIsModalOpen={setIsModalOpenServiceAmenity} />
    </>
  );
};

export default Rooms;
