import { httpApi } from '@app/api/http.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { doCreateSlogan, doGetParent, doUpdateSlogan } from '@app/store/slices/sloganSlice';
import { Form, Input, InputNumber, Rate, Select, Upload, message } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useNavigate, useParams } from 'react-router-dom';

interface SloganPopr {}

interface FileItem {
  uid: string | number;
  titleSlogan: string;
  quote: string;
  name: string;
}

type FieldType = {
  titleSloganVI?: string;
  titleSloganEN?: string;
  languageCodeVI?: string; // Language code for Vietnamese
  languageCodeEN?: string; // Language code for English
  quoteVI?: string;
  quoteEN?: string;
};

export const SloganDetails: React.FC<SloganPopr> = (props) => {
  const { parentId } = useParams();
  const [form] = Form.useForm();
  const nivagate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { slogan, languageCode } = useSelector((state: any) => state.slogan);
  const lang = localStorage.getItem('lng');

  React.useEffect(() => {
    if (parentId) {
      dispatch(doGetParent({ parentId, languageCode }));
    } else {
      form.resetFields();
    }
  }, [dispatch, parentId]);

  React.useEffect(() => {
    if (slogan && parentId) {
      slogan.map((item: any) => {
        item.languageCode === 'EN'
          ? form.setFieldsValue({
              titleSloganEN: item.titleSlogan,
              languageCodeEN: item.languageCode,
              quoteEN: item.quote,
            })
          : form.setFieldsValue({
              titleSloganVI: item.titleSlogan,
              languageCodeVI: item.languageCode,
              quoteVI: item.quote,
            });
      });
    }
  }, [slogan, lang, form]);

  const onFinish = async (values: any) => {
    console.log(values);
    const payload = {
      languageRequests: [
        {
          languageCode: 'EN',
          titleSlogan: values.titleSloganEN,
          quote: values.quoteEN,
        },
        {
          languageCode: 'VI',
          titleSlogan: values.titleSloganVI,
          quote: values.quoteVI,
        },
      ],
    };

    if (parentId) {
      await dispatch(doUpdateSlogan({ parentId, ...payload, t }));
    } else {
      await dispatch(doCreateSlogan({ ...payload, t }));
    }

    await nivagate('/manage/slogans');
  };

  return (
    <>
      <div className="flex justify-between item-center uppercase">
        <h3 className="text-2xl font-bold">{t('slogan_details.slogan')}</h3>
        <Link to="/manage/slogans" className="text-blue-500 ">
          {t('slogan_details.back')}
        </Link>
      </div>
      <br />
      <Form
        name="basic"
        form={form}
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        className="uppercase"
      >
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label="Language"
            name="languageCodeEN"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="English"
          >
            <Input defaultValue="EN" disabled />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('slogan_details.language')}
            name="languageCodeVI"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="Việt Nam"
          >
            <Input defaultValue="VI" disabled />
          </Form.Item>
        </div>
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label="Quote"
            name="quoteEN"
            rules={[{ required: true, message: 'Please input quote!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('slogan_details.Quote')}
            name="quoteVI"
            rules={[{ required: true, message: 'Please input quote !' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label="Title Slogan"
            name="titleSloganEN"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('slogan_details.titleSlogan')}
            name="titleSloganVI"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <Form.Item>
          <BaseButton type="primary" htmlType="submit" className="uppercase">
            {parentId ? t('slogan_details.button.update') : t('slogan_details.button.create')}
          </BaseButton>
        </Form.Item>
      </Form>
    </>
  );
};

export default SloganDetails;
