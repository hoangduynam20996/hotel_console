import { useEffect, useState } from 'react';
import { Input } from 'antd';
import useDebounce from '@app/hooks/useDebounce';
import SelectCommon from '@app/components/commonAnt/SelectCommon';
import { useDispatch, useSelector } from 'react-redux';
import { setIsOpen } from '@app/store/slices/modelSlice';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { useTranslation } from 'react-i18next';
import { TDataTable, TPermission, TRoot } from '@app/types/TypeGroup';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import ModelCommon from '@app/components/commonAnt/ModelCommon';
import AddRoleCommon from '@app/components/commonAnt/AddRoleCommon';
import TableCommon from '@app/components/commonAnt/TableCommon';
import { doGetPermissionGroups } from '@app/store/slices/permissionGroupSlice';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

interface ResponseObject {
  name: string;
}

const Group = () => {
  const [searchKey, setSearchKey] = useState('');
  const [dataTable, setDataTable] = useState<any>([]);
  const [originTable, setOriginTable] = useState([]);
  const [dataSelectActive, setDataSelectActive] = useState([]);
  const [selected, setSelected] = useState('');

  const useDebouce = useDebounce(searchKey, 1000);
  const isOpen = useAppSelector((state) => state.model.isOpen);
  const isOpenAddRole = useAppSelector((state) => state.groupPermission.IsOpen);
  const { groups, limitPage, currentPage, totalPages, loading } = useSelector((state: any) => state.permissionGroup);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(doGetPermissionGroups({ currentPage, limitPage }));
  }, [dispatch, currentPage, limitPage]);

  useEffect(() => {
    const data = groups;

    if (data) {
      const newData = (data as any)?.map((item: TRoot) => ({
        key: Math.random(),
        name: item?.groupName,
        description: item?.description,
        status: !item?.isDeleted ? 'ACTIVE' : 'IN ACTIVE',
        Addrole: item?.groupId,
      }));
      const dataNewSelect = (data as TPermission | any)?.map((item: TRoot) => ({
        value: item?.groupName,
        label: item?.groupName,
      }));
      setDataTable(newData);
      setOriginTable(newData);
      setDataSelectActive(dataNewSelect);
    }
  }, [groups, isOpen, isOpenAddRole, dispatch]);

  useEffect(() => {
    const filteredData = dataTable?.filter((item: TDataTable) =>
      item?.name.toLowerCase().includes(useDebouce.toLowerCase()),
    );

    setDataTable(filteredData);
    if (!useDebouce) setDataTable(originTable);

    if (selected) {
      const filteredDataForSelected = originTable?.filter((item) => {
        return ((item ?? {}) as ResponseObject)?.name === selected;
      });
      setDataTable(filteredDataForSelected);
    }
  }, [useDebouce, selected]);

  const handleAddPermissionGroup = () => {
    dispatch(setIsOpen(!isOpen));
  };

  return (
    <div>
      <div className="flex justify-between mb-4 items-center">
        <div className="w-[50%] flex gap-12">
          <div className="flex flex-col w-[50%]">
            <div className="mb-2">
              <span className="font-semibold">{t('group.input.search')}</span>
            </div>
            <Input placeholder="Search Group..." onChange={(e) => setSearchKey(e.target.value)} />
          </div>
        </div>
        <div className="w-[30%] mt-4 flex gap-8 right-0">
          <div className="flex right-0 gap-4 justify-end">
            <PermissionChecker hasPermissions={['admin_write']}>
              <BaseButton className="h-14" type="primary" onClick={handleAddPermissionGroup} size="small">
                <span>{t('group.button.add_group')}</span>
              </BaseButton>
            </PermissionChecker>
          </div>
        </div>
      </div>
      <div>
        <TableCommon
          dataSource={dataTable}
          totalPages={totalPages}
          currentPage={currentPage}
          limitPage={limitPage}
          loading={loading}
        ></TableCommon>
      </div>
      <ModelCommon initialOpen={isOpen}></ModelCommon>
      <AddRoleCommon initialOpen={isOpenAddRole}></AddRoleCommon>
    </div>
  );
};

export default Group;
