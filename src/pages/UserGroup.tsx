import React, { useEffect } from 'react';
import { Card, Input, Select, Form } from 'antd';
import type { SelectProps } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useNavigate, useParams } from 'react-router-dom';
import { doGetUser, doAddUserForGroups } from '@app/store/slices/userSlice';
import { doGetPermissionGroups } from '@app/store/slices/permissionGroupSlice';
import { useTranslation } from 'react-i18next';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

export const UserGroup = () => {
  const { id } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { userById, groupsByUser } = useSelector((state: any) => state.user);
  const { groups, currentPage, loading } = useSelector((state: any) => state.permissionGroup);
  const navigate = useNavigate();
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(doGetUser({ id }));
    dispatch(doGetPermissionGroups({ currentPage, limitPage: 100 }));
  }, [dispatch, id]);

  useEffect(() => {
    if (userById && id) {
      form.setFieldValue('email', userById.email);
      form.setFieldValue('phoneNumber', userById.phoneNumber);
      form.setFieldValue(
        'groupIds',
        groupsByUser?.map((group: any) => {
          return group.groupId;
        }),
      );
    }
  }, [form, userById, id]);

  const options: SelectProps['options'] = [];
  if (groups) {
    groups.forEach((permissionGroup: any) => {
      options.push({
        label: permissionGroup.groupName,
        value: permissionGroup.groupId,
      });
    });
  }

  const onFinish = (values: any) => {
    values.userId = id;
    values.groupIds = values.groupIds.map((groupId: string) => {
      return {
        groupId: groupId,
      };
    });

    dispatch(doAddUserForGroups({ values, t }));

    console.log('Success:', values);
  };

  const handleChange = (value: string[]) => {
    console.log(`selected ${value}`);
  };

  return (
    <Form form={form} name="basic" onFinish={onFinish}>
      <div className="flex justify-between items-center">
        <h3 className="text-2xl font-bold capitalize">{t('user_group.title')}</h3>
        <div className="flex gap-5">
          <PermissionChecker hasPermissions={['admin_write']}>
            <BaseButton type="primary" htmlType="submit" loading={loading}>
              {t('user_group.button.save')}
            </BaseButton>
          </PermissionChecker>
          <BaseButton type="ghost" onClick={() => navigate(-1)}>
            {t('user_group.button.back')}
          </BaseButton>
        </div>
      </div>
      <div className="flex flex-wrap md:flex-nowrap items-center gap-5">
        <div className="w-1/2">
          <h6 className="text-base text-sky-700">Email</h6>
          <Form.Item name="email" rules={[{ required: true, message: 'Please input your email!' }]}>
            <Input disabled />
          </Form.Item>
        </div>
        <div className="w-1/2">
          <h6 className="text-base text-sky-700">{t('user_group.input.phone_number')}</h6>
          <Form.Item name="phoneNumber" rules={[{ required: true, message: 'Please input your phone number!' }]}>
            <Input disabled />
          </Form.Item>
        </div>
      </div>
      <div className="flex flex-wrap md:flex-nowrap items-start gap-5 mt-5">
        <div className="w-1/2">
          <h6 className="text-base text-sky-700">{t('user_group.input.select_group')}</h6>
          <Form.Item name="groupIds" rules={[{ required: true, message: 'Please input your permission group!' }]}>
            <Select
              mode="multiple"
              allowClear
              style={{ width: '100%' }}
              placeholder="Please select"
              onChange={handleChange}
              options={options}
            />
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default UserGroup;
