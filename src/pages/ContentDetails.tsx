import { httpApi } from '@app/api/http.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { doCreateContents, doGetContent, doUpdateContents } from '@app/store/slices/ContentSlice';
import { doCreateHeadings, doGetHeading, doGetHeadings, doUpdateHeadings } from '@app/store/slices/Headingslice';
import { Form, Input, InputNumber, Rate, Select, Upload, message } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useNavigate, useParams } from 'react-router-dom';

interface ContentDetailPops {}

interface FileItem {
  uparentId: string | number;
  titleContent: string;
  descriptionContent: string;
  headingContent: string;
  name: string;
}

type FieldType = {
  titleContentVI?: string;
  titleContentEN?: string;
  languageCodeVI?: string; // Language code for Vietnamese
  languageCodeEN?: string; // Language code for English
  headingContentVI?: string;
  headingContentEN?: string;
  descriptionContentVI?: string;
  descriptionContentEN?: string;
};

export const ContentDetails: React.FC<ContentDetailPops> = (props) => {
  const { parentId } = useParams();
  const [form] = Form.useForm();
  const nivagate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { content, languageCode } = useSelector((state: any) => state.content);

  React.useEffect(() => {
    if (parentId) {
      dispatch(doGetContent({ parentId, languageCode, t }));
    } else {
      form.resetFields();
    }
  }, [dispatch, parentId]);

  React.useEffect(() => {
    if (content && parentId) {
      content.map((item: any) => {
        item.languageCode === 'EN'
          ? form.setFieldsValue({
              titleContentEN: item.titleContent,
              languageCodeEN: item.languageCode,
              headingContentEN: item.headingContent,
              descriptionContentEN: item.descriptionContent,
            })
          : form.setFieldsValue({
              titleContentVI: item.titleContent,
              languageCodeVI: item.languageCode,
              headingContentVI: item.headingContent,
              descriptionContentVI: item.descriptionContent,
            });
      });
    }
  }, [content, form]);

  const onFinish = async (values: any) => {
    console.log(values);
    const payload = {
      languageRequests: [
        {
          languageCode: 'EN',
          headingContent: values.headingContentEN,
          titleContent: values.titleContentEN,
          descriptionContent: values.descriptionContentEN,
        },
        {
          languageCode: 'VI',
          headingContent: values.headingContentVI,
          titleContent: values.titleContentVI,
          descriptionContent: values.descriptionContentVI,
        },
      ],
    };

    if (parentId) {
      await dispatch(doUpdateContents({ parentId, ...payload, t }));
    } else {
      await dispatch(doCreateContents({ ...payload, t }));
    }

    await nivagate('/manage/contents');
  };

  return (
    <>
      <div className="flex justify-between item-center uppercase">
        <h3 className="text-2xl font-bold">{t('content_details.content')}</h3>
        <Link to="/manage/contents" className="text-blue-500">
          {t('content_details.back')}
        </Link>
      </div>
      <br />
      <Form
        name="basic"
        form={form}
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        className="uppercase"
      >
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('content_details.language')}
            name="languageCodeVI"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="Việt Nam"
          >
            <Input defaultValue="VI" disabled />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Language"
            name="languageCodeEN"
            rules={[{ required: true, message: 'Please input your language!' }]}
            initialValue="English"
          >
            <Input defaultValue="EN" disabled />
          </Form.Item>
        </div>

        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('content_details.title_conent')}
            name="titleContentVI"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Title Content"
            name="titleContentEN"
            rules={[{ required: true, message: 'Please input titlle!' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('content_details.heading')}
            name="headingContentVI"
            rules={[{ required: true, message: 'Please input link Heading Content!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Heading Content"
            name="headingContentEN"
            rules={[{ required: true, message: 'Please input link Heading Content!' }]}
          >
            <Input />
          </Form.Item>
        </div>

        <div className="flex gap-40">
          <Form.Item<FieldType>
            className="w-1/4"
            label={t('content_details.description')}
            name="descriptionContentVI"
            rules={[{ required: true, message: 'Please input link Description!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item<FieldType>
            className="w-1/4"
            label="Description Content"
            name="descriptionContentEN"
            rules={[{ required: true, message: 'Please input link Description!' }]}
          >
            <Input />
          </Form.Item>
        </div>
        <Form.Item>
          <BaseButton type="primary" htmlType="submit" className="uppercase">
            {parentId ? t('content_details.button.update') : t('content_details.button.create')}
          </BaseButton>
        </Form.Item>
      </Form>
    </>
  );
};
export default ContentDetails;
