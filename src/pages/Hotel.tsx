import React from 'react';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { doGetHotels, doChangeHotelStatus, setHotelId, setLanguage, setContactId } from '@app/store/slices/hotelSlice';
import { useTranslation } from 'react-i18next';
import { Table, Rate, Switch } from 'antd';
import { FilterHotelsPage } from '@app/pages/uiComponentsPages/forms/FilterHotelsPage';
import { AddHotelInfo } from '@app/pages/uiComponentsPages/forms/AddHotelInfo';
import { AddHotelContact } from '@app/pages/uiComponentsPages/forms/AddHotelContact';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';

type HotelProps = {
  // Define your component's props here
};

const Hotel: React.FC<HotelProps> = () => {
  const currentDate = new Date();
  const nextDay = new Date(currentDate);
  nextDay.setDate(currentDate.getDate() + 1);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [data, setData] = React.useState<any>([]);
  const [isModalOpenInfo, setIsModalOpenInfo] = React.useState(false);
  const [isModalOpenContact, setIsModalOpenContact] = React.useState(false);

  const {
    hotels,
    hotelId,
    loading,
    totalPages,
    currentPage,
    limitPage,
    hotelName,
    languageCode,
    language,
    hotelContactId,
    status,
  } = useSelector((state: any) => state.hotel);

  React.useEffect(() => {
    dispatch(
      doGetHotels({
        hotelName,
        currentPage,
        limitPage,
        status,
      }),
    );
  }, [dispatch, currentPage, limitPage, status]);

  React.useLayoutEffect(() => {
    const result = hotels.map((item: any, index: number) => {
      return {
        index: -(-(currentPage - 1) * limitPage - (index + 1)),
        key: item?.id,
        id: item?.id,
        hotelName: item?.name,
        rating: item?.rating,
        address: `${item?.streetAddress} ${item?.districtAddress}`,
        city: item?.city,
        country: item?.country,
        countView: item?.countView,
        status: item?.status,
        hotelContactId: item?.id,
        edit: false,
      };
    });

    let filteredResult = result;
    if (hotelName) {
      filteredResult = hotels
        .filter((hotel: any) => hotel.name.toLowerCase().includes(hotelName.toLowerCase()))
        .map((item: any, index: number) => {
          return {
            index: -(-(currentPage - 1) * limitPage - (index + 1)),
            key: item?.id,
            id: item?.id,
            hotelName: item?.name,
            rating: item?.rating,
            address: `${item?.streetAddress} ${item?.districtAddress}`,
            city: item?.city,
            country: item?.country,
            countView: item?.countView,
            status: item?.status,
            hotelContactId: item?.id,
            edit: false,
          };
        });
    }

    setData(filteredResult);
  }, [hotels, hotelName]);

  const columns = [
    {
      title: '#',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: t('columns.hotelName'),
      dataIndex: 'hotelName',
      key: 'hotel_name',
    },
    {
      title: t('columns.star'),
      dataIndex: 'rating',
      key: 'rating',
      render: (rating: any) => {
        return (
          <div className="flex justify-start items-start">
            <p>{rating}</p>
            <Rate disabled value={1} count={1} />
          </div>
        );
      },
    },
    {
      title: t('columns.address'),
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: t('columns.city'),
      dataIndex: 'city',
      key: 'city',
    },
    {
      title: t('columns.currency'),
      dataIndex: 'country',
      key: 'country',
    },
    {
      title: 'Views',
      dataIndex: 'countView',
      key: 'countView',
    },
    {
      title: t('columns.status'),
      dataIndex: 'status',
      key: 'status',
      render: (status: any, id: any) => {
        return (
          <div className="flex justify-between">
            {status === 'ACTIVE' ? (
              <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
            ) : (
              <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
            )}
            <PermissionChecker hasPermissions={['hotel_update_status']}>
              <Switch
                className={`ml-2 ${status === 'ACTIVE' ? 'bg-blue-500' : 'bg-gray-500'}`}
                checked={status === 'ACTIVE'}
                loading={loading}
                onChange={(e) => {
                  const newStatus = status === 'ACTIVE' ? 'NO_ACTIVE' : 'ACTIVE';
                  dispatch(
                    doChangeHotelStatus({
                      values: {
                        hotelId: id.id,
                        status: newStatus,
                      },
                      t,
                    }),
                  );
                }}
              />
            </PermissionChecker>
          </div>
        );
      },
    },
    {
      title: t('columns.function'),
      dataIndex: 'function',
      key: 'function',
      render: (_: any, item: any) => (
        <div className="flex justify-center items-center gap-4">
          <Link
            to={`/hotel/detail?id=${item.id}&location=${item.city}&checkin=${format(
              currentDate,
              'yyyy-MM-dd',
            )}&checkout=${format(nextDay, 'yyyy-MM-dd')}&group_adults=1&group_children=0&group_rooms=1`}
          >
            <BaseButton type="ghost">{t('columns.button.details')}</BaseButton>
          </Link>

          <PermissionChecker hasPermissions={['room_view_info']}>
            <Link to={`/rooms/list/${item.id}`}>
              <BaseButton type="primary">Rooms</BaseButton>
            </Link>
          </PermissionChecker>
          {/* <PermissionChecker
                        hasPermissions={['hotel_view_contact', 'hotel_create_contact', 'hotel_update_contact']}
                    >
                        <BaseButton
                            type="ghost"
                            onClick={() => {
                                setIsModalOpenContact(true);
                                dispatch(setHotelId(item.id));
                                item.hotelContactId ?
                                    dispatch(setContactId(item.hotelContactId))
                                    :
                                    dispatch(setContactId(''));
                            }}
                        >
                            Contact
                        </BaseButton>
                    </PermissionChecker> */}
        </div>
      ),
    },
  ];
  return (
    <>
      <h3 className="text-2xl font-bold capitalize">{t('columns.hotelList')}</h3>
      <div className="flex flex-wrap md:flex-nowrap items-center my-3">
        <div className="flex flex-wrap md:flex-nowrap items-center w-full">
          <FilterHotelsPage />
          <div className="w-full flex flex-nowrap gap-5">
            <div className="ml-auto">
              {/* <PermissionChecker
                                hasPermissions={['hotel_create_info']}
                            >

                                <Link
                                    to="/hotel/add"
                                >
                                    <BaseButton type="primary">Add hotel</BaseButton>
                                </Link>
                            </PermissionChecker> */}
            </div>
          </div>
        </div>
      </div>
      <Table
        dataSource={data}
        columns={columns}
        loading={loading}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          defaultPageSize: limitPage,
          defaultCurrent: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(doGetHotels({ currentPage: page, limitPage: pageSize, languageCode, hotelName }));
          },
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
        }}
      />
      <AddHotelInfo
        isModalOpen={isModalOpenInfo}
        setIsModalOpen={setIsModalOpenInfo}
        hotelId={hotelId}
        language={language}
      />
      <AddHotelContact
        isModalOpen={isModalOpenContact}
        setIsModalOpen={setIsModalOpenContact}
        hotelId={hotelId}
        hotelContactId={hotelContactId}
      />
    </>
  );
};

export default Hotel;
