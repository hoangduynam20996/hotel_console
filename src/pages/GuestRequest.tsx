import React, { useEffect } from 'react';
import { Badge, Select, Table } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import {
  doGetGuestRequests,
  doUpdateStatusGuestRequest,
  doDeleteGuestRequest,
} from '@app/store/slices/guestRequestSlice';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { DeleteOutlined } from '@ant-design/icons';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';
import { BasePopconfirm } from '@app/components/common/BasePopconfirm/BasePopconfirm';

interface GuestRequestProps {
  // Define the props for the component here
}

const GuestRequest: React.FC<GuestRequestProps> = (props) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [data, setData] = React.useState<any>([]);
  const { guestRequests, loading, totalPages, currentPage, limitPage } = useSelector(
    (state: any) => state.guestRequest,
  );

  useEffect(() => {
    dispatch(doGetGuestRequests({ currentPage: currentPage, limitPage: limitPage }));
  }, [dispatch, currentPage, limitPage]);

  useEffect(() => {
    const result = guestRequests.map((item: any, index: number) => {
      return {
        index: -(-(currentPage - 1) * limitPage - (index + 1)),
        key: item.guestRequestId,
        id: item.guestRequestId,
        name: item.name,
        email: item.email,
        phoneNumber: item.phoneNumber,
        status: item.status,
        description: item.description,
        createdBy: item.createdBy,
        lastModifiedBy: item.lastModifiedBy,
        createdDate: item.createdDate,
        lastModifiedDate: item.lastModifiedDate,
        edit: false,
      };
    });
    setData(result);
  }, [guestRequests]);

  const handleChange = (value: string, item: any) => {
    dispatch(doUpdateStatusGuestRequest({ id: item.id, status: value }));
    item.status = value;
    item.edit = false;
    setData([...data]);
  };

  const handleDelete = (item: any) => {
    dispatch(doDeleteGuestRequest({ id: item.id }));
    setData(guestRequests);
  };

  const columns = [
    {
      title: 'No',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (status: any, item: any) => {
        return (
          <div className="flex items-center">
            {!item.edit ? (
              <div
                className="cursor-pointer"
                onClick={() => {
                  const newData = data.map((i: any) => {
                    if (i.id === item.id) {
                      return { ...i, edit: true };
                    }
                    return i;
                  });
                  setData(newData);
                }}
              >
                {status === 'receive' ? (
                  <Badge status="warning" text="Tiếp nhận" />
                ) : status === 'in progress' ? (
                  <Badge status="processing" text="Đang xử lý" />
                ) : (
                  <Badge status="success" text="Đã xử lý" />
                )}
              </div>
            ) : (
              <Select
                defaultValue={status}
                onChange={(value: string) => handleChange(value, item)}
                options={[
                  {
                    value: 'receive',
                    label: 'Tiếp nhận',
                  },
                  {
                    value: 'in progress',
                    label: 'Đang xử lý',
                  },
                  {
                    value: 'done',
                    label: 'Đã xử lý',
                  },
                ]}
              />
            )}
          </div>
        );
      },
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: string, record: any) => (
        <PermissionChecker hasPermissions={['guest_delete']}>
          <div className="flex justify-center items-center gap-4">
            <BasePopconfirm
              title="Are you sure delete the data?"
              onConfirm={() => {
                handleDelete(record);
              }}
              okText="Yes"
              cancelText="No"
            >
              <BaseButton type="ghost" htmlType="submit">
                <DeleteOutlined />
              </BaseButton>
            </BasePopconfirm>
          </div>
        </PermissionChecker>
      ),
    },
  ];

  return (
    <div>
      <h3 className="text-2xl font-bold capitalize">Guest Request</h3>

      <Table
        dataSource={data}
        columns={columns}
        loading={loading}
        pagination={{
          showSizeChanger: true,
          pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
          pageSize: limitPage,
          current: currentPage,
          total: totalPages * limitPage,
          onChange: (page: number, pageSize: number) => {
            dispatch(doGetGuestRequests({ currentPage: page, limitPage: pageSize }));
          },
        }}
      />
    </div>
  );
};

export default GuestRequest;
