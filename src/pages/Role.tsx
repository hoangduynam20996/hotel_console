import { useState, useEffect } from 'react';
import RoleTree from '@app/components/role-component/RoleTree';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getAllPermission } from '@app/api/permissionGroupRole.api';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { notificationController } from '@app/controllers/notificationController';
import { Divider, Space, Tag } from 'antd';

const Role = () => {
  // const { t } = useTranslation();
  // const dispatch = useDispatch();
  // const groupId = useAppSelector((state) => state.groupPermission.groupId);
  // const listRole = useAppSelector((state) => state.groupPermission.listRole);
  const [permissions, setPermissions] = useState([]);
  const currentPage = 1;
  const pageSize = 99;

  // Convert Data from api to show in FE

  const getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  // Call api to get permission group
  useEffect(() => {
    (async () => {
      try {
        const allPermission = await getAllPermission({ currentPage, pageSize });
        setPermissions(allPermission.data);
      } catch (error) {}
    })();
  }, []);

  return (
    <>
      <div className="w-54 mb-4">
        <Divider orientation="left">List permission</Divider>
        <Space size={[0, 8]} wrap>
          {permissions.length >= 0 &&
            permissions.map((item: any) => (
              <div className="my-4">
                <Tag color={getRandomColor()}>{item.permissionCode}</Tag>
              </div>
            ))}
        </Space>
      </div>
    </>
  );
};

export default Role;
