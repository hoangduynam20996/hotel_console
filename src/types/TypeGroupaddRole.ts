export interface PermissionGroup {
  permissionGroupId: string;
  permissionGroupName: string;
  isDeleted: boolean;
  description: string;
  permissions: Permission[];
}

export interface Permission {
  permissionId: string;
  permissionCode: string;
  isDeleted: boolean;
  description: string;
}

export interface SelectList {
  key: string;
  title: string;
  children: SelectData[];
}

export interface SelectData {
  key: string;
  title: string;
}
