export interface TRoot {
  permissionGroupId: string;
  groupName: string;
  groupId: string;
  isDeleted: boolean;
  description: string;
  permissions: TPermission[];
}

export interface TPermission {
  permissionId: string;
  permissionCode: string;
  isDeleted: boolean;
  description: string;
  data: TRoot;
}

export interface TDataTable {
  key: number;
  name: string;
  description: string;
  isDelelete: string;
}
