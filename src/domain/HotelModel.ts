export interface HotelModel {
  hotelId: string;
  star: number;
  price: number;
  currency: string;
  status: string;
  languageCode: string;
  languages: Array<string>;
  hotelInfo: HotelInfo;
  hotelContactId: string;
}

export interface HotelInfo {
  name: string;
  address: string;
  city: string;
  description: string;
  languageCode: string;
}
