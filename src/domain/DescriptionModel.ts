export interface DescriptionModel {
  descriptionId: string;
  parentId: string;
  languageCode: string;
  titleDescription: string;
  contentDescription: string;
  isDeleted: boolean;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  status: string;
}
