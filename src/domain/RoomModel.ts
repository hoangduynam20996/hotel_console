export interface RoomModel {
  roomId: string;
  quantityRoom: number;
  quantityRoomRemain: number;
  price: number;
  acreage: number;
  capacity: number;
  adultQuantity: number;
  childrenQuantity: number;
  extraBedQuantity: number;
  singleBedQuantity: number;
  twinBedQuantity: number;
  status: string;
  roomInfos: RoomInfo[];
  type: Type;
  services: Service[];
}

export interface RoomInfo {
  name: string;
  description: string;
  languageCode: string;
}

export interface Type {
  id: string;
  code: string;
}

export interface Service {
  id: string;
  code: string;
  type: string;
}
