export interface HeadingModel {
  headingId: string;
  parentId: string;
  urlHeading: string;
  languageCode: string;
  titleHeading: string;
  isDeleted: boolean;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  status: string;
}
