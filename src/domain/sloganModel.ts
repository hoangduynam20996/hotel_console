export interface SloganModel {
  sloganId: string;
  parentId: string;
  languageCode: string;
  titleSlogan: string;
  quote: string;
  isDeleted: boolean;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  status: string;
}
