export interface GuestRequestModel {
  guestRequestId: string;
  name: string;
  email: string;
  status: string;
  phoneNumber: string;
  description: string;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
}
