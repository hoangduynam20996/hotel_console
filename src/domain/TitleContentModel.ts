export interface TitleContentModel {
  contentId: string;
  parentId: string;
  languageCode: string;
  headingContent: string;
  titleContent: string;
  descriptionContent: string;
  isDeleted: boolean;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  status: string;
}
