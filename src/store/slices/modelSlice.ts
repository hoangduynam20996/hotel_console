import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isOpen: false,
};

export const modelSlice = createSlice({
  name: 'model',
  initialState,
  reducers: {
    setIsOpen: (state, action) => {
      state.isOpen = action.payload;
    },
  },
});

export const { setIsOpen } = modelSlice.actions;

export default modelSlice.reducer;
