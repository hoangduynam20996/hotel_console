import {
  changeStatusSlogan,
  createSlogan,
  deleteSlogan,
  getSloganParent,
  getSlogans,
  updateSlogan,
} from '@app/api/Slogan.api';
import { notificationController } from '@app/controllers/notificationController';
import { SloganModel } from '@app/domain/sloganModel';
import Slogan from '@app/pages/Slogan';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { message, notification } from 'antd';
import { useTranslation } from 'react-i18next';

export interface SloganState {
  slogans: SloganModel[];
  slogan: SloganModel | null;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  titleSlogan: string;
  languageCode: string;
}

const initialState: SloganState = {
  slogans: [],
  slogan: null,
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 10,
  titleSlogan: '',
  languageCode: 'EN',
};

export const doGetSlogan = createAsyncThunk('slogan/getSlogan', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getSlogans(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: 'Error',
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetParent = createAsyncThunk('slogan/getParentSlogan', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getSloganParent(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_105'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doCreateSlogan = createAsyncThunk('slogan/createSlogan', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await createSlogan(payload);
    notificationController.success({
      message: payload.t('notification.MSG_103'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_107'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doUpdateSlogan = createAsyncThunk('slogan/updateSlogan', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await updateSlogan(payload);
    notificationController.success({
      message: payload.t('notification.MSG_102'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_106'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doDeleteSlogan = createAsyncThunk('slogan/deleteSlogan', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await deleteSlogan(payload);
    notificationController.success({
      message: payload.t('notification.MSG_104'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_108'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doChangeStatus = createAsyncThunk('slogan/changeStatus', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await changeStatusSlogan(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_109'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const sloganSlice = createSlice({
  name: 'slogan',
  initialState,
  reducers: {
    setTitleSlogan: (state, action) => {
      state.titleSlogan = action.payload;
    },
    setLanguageCode: (state, action) => {
      state.languageCode = action.payload;
    },
    setCurrentPage: (state, action) => {
      state.languageCode = action.payload;
    },
  },
  extraReducers(builder) {
    builder.addCase(doGetSlogan.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetSlogan.fulfilled, (state, action) => {
      state.slogans = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetSlogan.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doGetParent.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetParent.fulfilled, (state, action) => {
      state.slogan = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetParent.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doCreateSlogan.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateSlogan.fulfilled, (state, action) => {
      state.slogans = action.payload.data;
      state.loading;
    });
    builder.addCase(doCreateSlogan.rejected, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doUpdateSlogan.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateSlogan.fulfilled, (state, action) => {
      state.slogans = state.slogans.map((slogan) => {
        if (slogan.parentId === action.payload.data) {
          return action.payload.data;
        }
        return slogan;
      });
      state.loading = false;
    });
    builder.addCase(doUpdateSlogan.rejected, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doDeleteSlogan.pending, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doDeleteSlogan.fulfilled, (state, action) => {
      state.loading = false;
      const cleanId = state.slogans.findIndex((slogan) => slogan.parentId === action.meta.arg.id);
      if (cleanId !== -1) {
        state.slogans.splice(cleanId, 1);
      }
    });
    builder.addCase(doDeleteSlogan.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doChangeStatus.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangeStatus.fulfilled, (state, action) => {
      state.loading = false;
      state.slogans = state.slogans.map((slogan) => {
        if (slogan.parentId === action.meta.arg.values.parentId) slogan.status = action.meta.arg.values.status;
        return slogan;
      });
    });
  },
});

export default sloganSlice.reducer;
