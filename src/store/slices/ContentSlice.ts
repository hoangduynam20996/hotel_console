import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { notificationController } from '@app/controllers/notificationController';
import { TitleContentModel } from '@app/domain/TitleContentModel';
import {
  changStatusContent,
  createContent,
  deleteContent,
  getContentByParent,
  getContents,
  updateContents,
} from '@app/api/ContentApi';

export interface ContentState {
  contents: TitleContentModel[];
  content: TitleContentModel | null;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  titleContent: string;
  languageCode: string;
}

const initialState: ContentState = {
  contents: [],
  content: null,
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 10,
  titleContent: '',
  languageCode: 'EN',
};

export const doGetContents = createAsyncThunk('content/getContents', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getContents(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_101'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetContent = createAsyncThunk('content/getContent', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getContentByParent(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_101'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doCreateContents = createAsyncThunk(
  'content/createContents',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await createContent(payload);
      notificationController.success({
        message: payload.t('notification.MSG_103'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_107'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doUpdateContents = createAsyncThunk(
  'content/updateContents',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateContents(payload);
      notificationController.success({
        message: payload.t('notification.MSG_102'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_106'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doDeleteContents = createAsyncThunk(
  'content/deleteContents',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await deleteContent(payload);
      notificationController.success({
        message: payload.t('notification.MSG_104'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_108'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doChangStatusContent = createAsyncThunk(
  'content/changeStatus',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await changStatusContent(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: 'Can not change Status',
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const ContentSlice = createSlice({
  name: 'content',
  initialState,
  reducers: {
    setTitleContent: (state, action) => {
      state.titleContent = action.payload;
    },
    setLanguage: (state, action) => {
      state.languageCode = action.payload;
    },
  },
  extraReducers(builder) {
    builder.addCase(doGetContents.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetContents.fulfilled, (state, action) => {
      state.contents = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(doGetContents.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doCreateContents.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateContents.fulfilled, (state, action) => {
      state.loading = false;
      state.contents.unshift(action.payload.data);
    });
    builder.addCase(doCreateContents.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doUpdateContents.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doUpdateContents.fulfilled, (state, action) => {
      state.contents = state.contents.map((content) => {
        if (content.parentId === action.payload.data.parentId) {
          return action.payload.data;
        }
        return content;
      });
      state.loading = false;
    });

    builder.addCase(doGetContent.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doGetContent.fulfilled, (state, action) => {
      state.content = action.payload.data;
      state.loading = false;
    });

    builder.addCase(doGetContent.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doDeleteContents.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(doDeleteContents.fulfilled, (state, action) => {
      console.log(action);
      state.loading = false;
      const cleanId = state.contents.findIndex((content) => content.parentId === action.meta.arg.id);
      if (cleanId !== -1) {
        state.contents.splice(cleanId, 1);
      }
    });

    builder.addCase(doDeleteContents.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doChangStatusContent.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangStatusContent.fulfilled, (state, action) => {
      state.loading = false;
      state.contents = state.contents.map((content) => {
        if (content.parentId === action.meta.arg.values.parentId) {
          content.status = action.meta.arg.values.parentId;
        }
        return content;
      });
    });
    builder.addCase(doChangStatusContent.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export default ContentSlice.reducer;
