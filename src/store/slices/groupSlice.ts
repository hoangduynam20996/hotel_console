import { createAction, PrepareAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { UserModel } from '@app/domain/UserModel';
import { persistUser, readUser } from '@app/services/localStorage.service';
import { deleteToken, persistToken } from '@app/services/localStorage.service';
import { activeUser, deleteUser, addUser, getUser, getUsers } from '@app/api/userManager.api';
import { getProfileUser } from '@app/api/userProfile.api';
import { addPermissionGroup, addUserForGroups, getPermissionGroups } from '@app/api/permissonGroup.api';
import { notificationController } from '@app/controllers/notificationController';
import { getPermissionGroup } from '@app/api/permissionGroupRole.api';
export interface UserState {
  users: UserModel[];
  userById: UserModel | any;
  groupsByUser: [] | any;
  user: UserModel | any;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  roles: any;
}

const initialState: any = {
  groups: [],
  totalPages: 10,
  currentPage: 1,
  limitPage: 5,
};

export const doGetGroups = createAsyncThunk('groups/fetchGroups', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getPermissionGroups(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: `Lấy danh sách group thất bại`,
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doAddGroup = createAsyncThunk('groups/addGroup', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await addPermissionGroup(payload);
    notificationController.success({
      message: `Thêm người dùng thành công!`,
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: `Thêm người dùng thất bại!`,
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const groupSlice = createSlice({
  name: 'groups',
  initialState,
  reducers: {
    // Define any reducers if needed in the future
  },
  extraReducers: (builder) => {
    builder.addCase(doGetGroups.fulfilled, (state, action) => {
      state.groups = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });

    builder.addCase(doGetGroups.rejected, (state, action) => {
      // Handle rejection if needed
      state.loading = false;
    });

    builder.addCase(doAddGroup.fulfilled, (state, action) => {
      // Handle success if needed
    });

    builder.addCase(doAddGroup.rejected, (state, action) => {
      // Handle rejection if needed
    });
  },
});

export default groupSlice.reducer;
