import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { notificationController } from '@app/controllers/notificationController';
import { DescriptionModel } from '@app/domain/DescriptionModel';
import {
  DeleteDescription,
  UpdateDescription,
  changStatusDes,
  creatDescription,
  getDescription,
  getDescriptionByParent,
} from '@app/api/Descriptons.api';
import { message } from 'antd';
import { ActionsWrapper } from '@app/components/layouts/AuthLayout/AuthLayout.styles';
import { doUpdateContents } from './ContentSlice';

export interface DescriptitonState {
  descriptions: DescriptionModel[];
  description: DescriptionModel | null;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  titleDescription: string;
  languageCode: string;
}

const initialState: DescriptitonState = {
  descriptions: [],
  description: null,
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 10,
  titleDescription: '',
  languageCode: 'VN',
};

export const dogetDescription = createAsyncThunk(
  'description/getDescription',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getDescription(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_101'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const dogetByParent = createAsyncThunk('description/getByParent', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getDescriptionByParent(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_101'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doCreateDescription = createAsyncThunk(
  'description/createDescription',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await creatDescription(payload);
      notificationController.success({
        message: payload.t('notification.MSG_103'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_107'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doupdateDescription = createAsyncThunk(
  'description/updateDescription',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await UpdateDescription(payload);
      notificationController.success({
        message: payload.t('notification.MSG_102'),
        description: response.message,
        placement: 'topRight',
      });
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_106'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const dodeleteDescription = createAsyncThunk(
  'description/deleteDescription',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await DeleteDescription(payload);
      notificationController.success({
        message: payload.t('notification.MSG_104'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_108'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doChangeStatusDes = createAsyncThunk(
  'description/changeStats',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await changStatusDes(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: ' Can not change status',
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.message);
    }
  },
);

export const ContentSlice = createSlice({
  name: 'description',
  initialState,
  reducers: {
    setTitleContent: (state, action) => {
      state.titleDescription = action.payload;
    },
    setLanguage: (state, action) => {
      state.languageCode = action.payload;
    },
  },
  extraReducers(builder) {
    builder.addCase(dogetDescription.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(dogetDescription.fulfilled, (state, action) => {
      state.descriptions = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(dogetDescription.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(dogetByParent.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(dogetByParent.fulfilled, (state, action) => {
      state.description = action.payload.data;
      state.loading = false;
    });
    builder.addCase(dogetByParent.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doCreateDescription.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateDescription.fulfilled, (state, action) => {
      state.loading = false;
      state.descriptions.unshift(action.payload.data);
    });
    builder.addCase(doCreateDescription.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doUpdateContents.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateContents.fulfilled, (state, action) => {
      state.descriptions = state.descriptions.map((description) => {
        if (description.parentId === action.payload.data) {
          return action.payload.data;
        }
        return description;
      });
      state.loading = false;
    });
    builder.addCase(doUpdateContents.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(dodeleteDescription.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(dodeleteDescription.fulfilled, (state, action) => {
      state.loading = false;
      const index = state.descriptions.findIndex((description) => description.parentId === action.meta.arg.id);
      if (index !== -1) {
        state.descriptions.splice(index, 1);
      }
    });
    builder.addCase(dodeleteDescription.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doChangeStatusDes.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doChangeStatusDes.fulfilled, (state, action) => {
      state.loading = false;
      state.descriptions = state.descriptions.map((description) => {
        if (description.parentId === action.meta.arg.values.parentId) {
          description.status = action.meta.arg.values.status;
        }
        return description;
      });
    });
  },
});

export default ContentSlice.reducer;
