import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
  createServicesAmenity,
  deleteServicesAmenity,
  getServicesAmenity,
  updateServicesAmenity,
} from '@app/api/serviceAmenity.api';
import { notificationController } from '@app/controllers/notificationController';

export interface HotelState {
  servicesAmenity: any[];
  loading: boolean;
}

const initialState: HotelState = {
  servicesAmenity: [],
  loading: false,
};

export const doGetServicesAmenity = createAsyncThunk(
  'serviceAmenity/getServicesAmenity',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getServicesAmenity(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doCreateServiceAmenity = createAsyncThunk(
  'serviceAmenity/createServiceAmenity',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await createServicesAmenity(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doUpdateServiceAmenityRoom = createAsyncThunk(
  'serviceAmenity/updateAmenityRoom',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateServicesAmenity(payload);
      notificationController.success({
        message: payload.t('notification.MSG_102'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_106'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doDeleteServiceAmenity = createAsyncThunk(
  'serviceAmenity/deleteServiceAmenity',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await deleteServicesAmenity(payload);
      notificationController.success({
        message: payload.t('notification.MSG_104'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_108'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const serviceAmenitySlice = createSlice({
  name: 'serviceAmenity',
  initialState,
  reducers: {
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doCreateServiceAmenity.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateServiceAmenity.fulfilled, (state, action) => {
      state.servicesAmenity.unshift(action.payload.data);
      state.loading = false;
    });
    builder.addCase(doCreateServiceAmenity.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doUpdateServiceAmenityRoom.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateServiceAmenityRoom.fulfilled, (state, action) => {
      const updatedServiceAmenityRoom = action.payload.data;
      console.log(updatedServiceAmenityRoom);

      const index = state.servicesAmenity.findIndex((service) => service.id === updatedServiceAmenityRoom.id);

      if (index !== -1) {
        state.servicesAmenity.splice(index, 1);
      }
      state.servicesAmenity.splice(index, 0, updatedServiceAmenityRoom);
      state.loading = false;
    });
    builder.addCase(doUpdateServiceAmenityRoom.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doDeleteServiceAmenity.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doDeleteServiceAmenity.fulfilled, (state, action) => {
      const index = state.servicesAmenity.findIndex(
        (service) => service.id === action.meta.arg.values.serviceAmenityId,
      );
      state.servicesAmenity.splice(index, 1);
      state.loading = false;
    });
    builder.addCase(doDeleteServiceAmenity.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetServicesAmenity.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetServicesAmenity.fulfilled, (state, action) => {
      state.servicesAmenity = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetServicesAmenity.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export const { setLoading } = serviceAmenitySlice.actions;
export default serviceAmenitySlice.reducer;
