import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RoomModel } from '@app/domain/RoomModel';
import {
  getRooms,
  getRoom,
  createRoom,
  createRoomPolicy,
  updateRoom,
  updateRoomPolicy,
  deleteRoom,
  changeRoomStatus,
  getRoomTypes,
  getRoomServiceAmenityRoom,
  getRoomServiceSub,
  getPolicyByRoomId,
  updateServiceAmenityRoom,
} from '@app/api/room.api';
import { notificationController } from '@app/controllers/notificationController';

export interface RoomState {
  rooms: RoomModel[];
  room: RoomModel | any;
  roomName: string;
  roomId: string;
  roomIdPolicy: string;
  roomTypes: any;
  roomServiceAmenityRoom: any;
  roomServiceSub: any;
  policy: any;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  languageCode: string;
}

const initialState: RoomState = {
  rooms: [],
  room: null,
  roomName: '',
  roomId: '',
  roomIdPolicy: '',
  roomTypes: null,
  roomServiceAmenityRoom: {},
  roomServiceSub: null,
  policy: null,
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 5,
  languageCode: 'VI',
};

export const doGetRooms = createAsyncThunk('room/getRooms', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getRooms(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetRoom = createAsyncThunk('room/getRoom', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getRoom(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doCreateRoom = createAsyncThunk('room/createRoom', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await createRoom(payload);
    notificationController.success({
      message: payload.t('notification.MSG_099'),
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doUpdateRoom = createAsyncThunk('room/updateRoom', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await updateRoom(payload);
    notificationController.success({
      message: payload.t('notification.MSG_099'),
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doUpdateServiceAmenityRoom = createAsyncThunk(
  'room/updateServiceAmenityRoom',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateServiceAmenityRoom(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doDeleteRoom = createAsyncThunk('room/deleteRoom', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await deleteRoom(payload);
    notificationController.success({
      message: payload.t('notification.MSG_104'),
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_108'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doChangeRoomStatus = createAsyncThunk(
  'room/changeRoomStatus',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await changeRoomStatus(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetRoomTypes = createAsyncThunk('room/getRoomTypes', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getRoomTypes(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetRoomServiceAmenityRoom = createAsyncThunk(
  'room/roomServiceAmenityRoom',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getRoomServiceAmenityRoom(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetRoomServiceSub = createAsyncThunk(
  'room/getRoomServiceSub',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getRoomServiceSub(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetPolicyByRoomId = createAsyncThunk(
  'room/getPolicyByRoomId',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getPolicyByRoomId(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doCreateRoomPolicy = createAsyncThunk(
  'room/createRoomPolicy',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await createRoomPolicy(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doUpdateRoomPolicy = createAsyncThunk(
  'room/updateRoomPolicy',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateRoomPolicy(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const roomSlice = createSlice({
  name: 'room',
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
    setRoomName: (state, action) => {
      state.roomName = action.payload;
    },
    setRoomId: (state, action) => {
      state.roomId = action.payload;
    },
    setRoomIdPolicy: (state, action) => {
      state.roomIdPolicy = action.payload;
    },
    setLanguageCode: (state, action) => {
      state.languageCode = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doGetRooms.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetRooms.fulfilled, (state, action) => {
      state.rooms = action.payload.data;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
      state.loading = false;
    });
    builder.addCase(doGetRooms.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetRoom.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetRoom.fulfilled, (state, action) => {
      state.room = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetRoom.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doCreateRoom.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateRoom.fulfilled, (state, action) => {
      // add new room in rooms
      state.rooms.unshift(action.payload.data);
      state.loading = false;
    });
    builder.addCase(doCreateRoom.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doUpdateRoom.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateRoom.fulfilled, (state, action) => {
      const updatedRoom = action.payload.data;
      const index = state.rooms.findIndex((room) => room.id === updatedRoom.id);

      if (index !== -1) {
        state.rooms.splice(index, 1);
      }

      state.rooms.splice(index, 0, updatedRoom);
      state.loading = false;
    });
    builder.addCase(doUpdateRoom.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doUpdateServiceAmenityRoom.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateServiceAmenityRoom.fulfilled, (state, action) => {
      state.roomServiceAmenityRoom = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doUpdateServiceAmenityRoom.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doDeleteRoom.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doDeleteRoom.fulfilled, (state, action) => {
      const index = state.rooms.findIndex((room) => room.roomId === action.meta.arg.values.roomId);
      state.rooms.splice(index, 1);
      state.loading = false;
    });
    builder.addCase(doDeleteRoom.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doChangeRoomStatus.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangeRoomStatus.fulfilled, (state, action) => {
      state.rooms = state.rooms.map((room) => {
        if (room.roomId === action.meta.arg.values.roomId) {
          room.status = action.meta.arg.values.status;
        }
        return room;
      });
      state.loading = false;
    });
    builder.addCase(doChangeRoomStatus.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetRoomTypes.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetRoomTypes.fulfilled, (state, action) => {
      state.roomTypes = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetRoomTypes.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetRoomServiceAmenityRoom.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetRoomServiceAmenityRoom.fulfilled, (state, action) => {
      state.roomServiceAmenityRoom = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetRoomServiceAmenityRoom.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doGetRoomServiceSub.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetRoomServiceSub.fulfilled, (state, action) => {
      state.roomServiceSub = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetRoomServiceSub.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetPolicyByRoomId.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetPolicyByRoomId.fulfilled, (state, action) => {
      state.policy = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetPolicyByRoomId.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doCreateRoomPolicy.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateRoomPolicy.fulfilled, (state, action) => {
      state.policy = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doCreateRoomPolicy.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doUpdateRoomPolicy.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateRoomPolicy.fulfilled, (state, action) => {
      state.policy = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doUpdateRoomPolicy.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export const { setCurrentPage, setRoomName, setRoomId, setRoomIdPolicy, setLanguageCode } = roomSlice.actions;
export default roomSlice.reducer;
