import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { HotelModel } from '@app/domain/HotelModel';
import { notificationController } from '@app/controllers/notificationController';
import {
  getHotels,
  getHotel,
  getHotelContact,
  createHotel,
  createHotelContact,
  updateHotel,
  updateHotelContact,
  addHotelInfo,
  changeHotelStatus,
  getServicesAmenity,
  getProvinces,
  getHotelTypes,
  getHotelImages,
} from '@app/api/hotel.api';

export interface HotelState {
  hotels: HotelModel[];
  hotel: HotelModel | null;
  hotelId: string;
  hotelContactId: string;
  hotelContact: any;
  servicesAmenity: any[];
  servicesHotelTypes: any[];
  servicesHotelImage: any[];
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  hotelName: string;
  languageCode: string;
  language: string;
  status: string;
  provinces: any[];
}

const initialState: HotelState = {
  hotels: [],
  hotel: null,
  hotelId: '',
  hotelContactId: '',
  hotelContact: null,
  servicesAmenity: [],
  servicesHotelTypes: [],
  servicesHotelImage: [],
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 5,
  hotelName: '',
  languageCode: 'VI',
  language: '',
  status: 'ACTIVE',
  provinces: [],
};

export const doGetHotels = createAsyncThunk('hotel/getHotels', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHotels(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetHotel = createAsyncThunk('hotel/getHotel', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHotel(payload.params);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doCreateHotel = createAsyncThunk('hotel/createHotel', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await createHotel(payload);
    notificationController.success({
      message: payload.t('notification.MSG_099'),
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doCreateHotelContact = createAsyncThunk(
  'hotel/createHotelContact',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await createHotelContact(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetHotelContact = createAsyncThunk(
  'hotel/getHotelContact',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getHotelContact(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doAddHotelInfo = createAsyncThunk('hotel/addHotelInfo', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await addHotelInfo(payload);
    notificationController.success({
      message: payload.t('notification.MSG_099'),
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doUpdateHotel = createAsyncThunk('hotel/updateHotel', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await updateHotel(payload);
    notificationController.success({
      message: payload.t('notification.MSG_102'),
      description: response?.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doUpdateHotelContact = createAsyncThunk(
  'hotel/updateHotelContact',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateHotelContact(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response?.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doChangeHotelStatus = createAsyncThunk(
  'hotel/changeHotelStatus',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await changeHotelStatus(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetServicesAmenity = createAsyncThunk(
  'hotel/getServicesAmenity',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getServicesAmenity(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetHotelTypes = createAsyncThunk('hotel/getHotelTypes', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHotelTypes(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetHotelImages = createAsyncThunk('hotel/getHotelImages', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHotelImages(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetProvinces = createAsyncThunk('hotel/getProvinces', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getProvinces(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const hotelSlice = createSlice({
  name: 'hotel',
  initialState,
  reducers: {
    setHotelName: (state, action) => {
      state.hotelName = action.payload;
    },
    setLanguageCode: (state, action) => {
      state.languageCode = action.payload;
    },
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
    setLimitPage: (state, action) => {
      state.limitPage = action.payload;
    },
    setTotalPages: (state, action) => {
      state.totalPages = action.payload;
    },
    setHotelId: (state, action) => {
      state.hotelId = action.payload;
    },
    setLanguage: (state, action) => {
      state.language = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
    setContactId: (state, action) => {
      state.hotelContactId = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doGetHotels.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetHotels.fulfilled, (state, action) => {
      state.hotels = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(doGetHotels.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doChangeHotelStatus.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangeHotelStatus.fulfilled, (state, action) => {
      state.loading = false;
      // find and update the hotel status
      state.hotels = state.hotels.map((hotel) => {
        if (hotel.id === action.meta.arg.values.hotelId) {
          hotel.status = action.meta.arg.values.status;
        }
        return hotel;
      });
    });

    builder.addCase(doChangeHotelStatus.rejected, (state, _) => {
      state.loading = false;
    });

    builder.addCase(doGetHotel.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetHotel.fulfilled, (state, action) => {
      state.hotel = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetHotel.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doGetHotelContact.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doGetHotelContact.fulfilled, (state, action) => {
      state.hotelContact = action.payload.data;
      state.loading = false;
    });

    builder.addCase(doGetHotelContact.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doCreateHotel.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateHotel.fulfilled, (state, action) => {
      state.loading = false;
      state.hotels.unshift(action.payload.data);
      state.hotelContactId = '';
    });
    builder.addCase(doCreateHotel.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doCreateHotelContact.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateHotelContact.fulfilled, (state, action) => {
      console.log('action: ', action);
      state.hotelContact = action.payload.data;
      // update the hotel contact id
      state.hotels = state.hotels.map((hotel) => {
        if (hotel.hotelId === action.payload.data.hotelId) {
          hotel.hotelContactId = action.payload.data.hotelContactId;
        }
        return hotel;
      });
      state.loading = false;
    });
    builder.addCase(doCreateHotelContact.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doAddHotelInfo.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doAddHotelInfo.fulfilled, (state, action) => {
      // find and update the hotel languages list
      state.hotels = state.hotels.map((hotel) => {
        if (hotel.hotelId === action.meta.arg.values.id) {
          hotel.languages = [...hotel.languages, action.payload.data.languageCode];
        }
        return hotel;
      });
      state.loading = false;
    });
    builder.addCase(doAddHotelInfo.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doUpdateHotel.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateHotel.fulfilled, (state, action) => {
      state.loading = false;
      state.hotels = state.hotels.map((hotel) => {
        if (hotel.hotelId === action.payload.data.hotelId) {
          return action.payload.data;
        }
        return hotel;
      });
      // navigation /hotels
    });
    builder.addCase(doUpdateHotel.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doUpdateHotelContact.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doUpdateHotelContact.fulfilled, (state, action) => {
      state.hotelContact = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doUpdateHotelContact.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doGetServicesAmenity.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetServicesAmenity.fulfilled, (state, action) => {
      state.servicesAmenity = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetServicesAmenity.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doGetHotelTypes.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetHotelTypes.fulfilled, (state, action) => {
      state.servicesHotelTypes = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetHotelTypes.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetHotelImages.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetHotelImages.fulfilled, (state, action) => {
      state.servicesHotelImage = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetHotelImages.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetProvinces.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetProvinces.fulfilled, (state, action) => {
      state.provinces = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetProvinces.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export const {
  setHotelName,
  setLanguageCode,
  setCurrentPage,
  setLimitPage,
  setTotalPages,
  setHotelId,
  setLanguage,
  setContactId,
  setStatus,
} = hotelSlice.actions;
export default hotelSlice.reducer;
