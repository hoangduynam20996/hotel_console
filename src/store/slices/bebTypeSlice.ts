import { getBebTypes } from '@app/api/bebtype.api';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const doGetRooms = createAsyncThunk('bebtype/getBebTypes', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getBebTypes(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});
