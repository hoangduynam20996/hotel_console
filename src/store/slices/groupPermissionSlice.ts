import { createAction, PrepareAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { addPermission, getPermissionGroupById } from '@app/api/permissionGroupRole.api';
import { notificationController } from '@app/controllers/notificationController';
import { Permission } from '@app/domain/UserModel';

export interface GroupPermission {
  groupId: string;
  groupName: string;
  group: {};
  listRole: string[];
  listSelect: string[];
  IsOpen: boolean;
}

const initialState: GroupPermission = {
  groupId: '',
  groupName: '',
  group: {},
  listRole: [],
  listSelect: [],
  IsOpen: false,
};

export const setNewRole = createAction<PrepareAction<GroupPermission>>('Group/addRole', (newRole) => {
  return {
    payload: newRole,
  };
});

export const createRole = createAsyncThunk('Group/Add', async (request: any) =>
  addPermission(request)
    .then((data) => {
      if (data.status === 200) {
        notificationController.success({ message: data.message });
      }
      // waiting the new api to call set data
    })
    .catch(),
);

export const getPermissionById = createAsyncThunk('Group/Get', async (request: any) =>
  getPermissionGroupById(request)
    .then((data) => {
      return data;
    })
    .catch(),
);

const convertPermissionGroups = (allPermission: Permission[]): any => {
  return allPermission.map((permission: any, dataIndex: number) => {
    return permission.permissionId;
  });
};

export const GroupPermission = createSlice({
  name: 'groupPermission',
  initialState,
  reducers: {
    addNewRole: (state, action) => {
      if (action.payload.groupId) {
        state.groupId = action.payload.groupId;
      }
      if (action.payload.groupId === 'fresh') {
        state.groupId = '';
      }
      if (action.payload?.listCode) {
        state.listRole = action.payload?.listCode;
      }
      if (!action.payload?.listCode) {
        state.listRole = [];
      }
    },
    setIsOpen: (state, action) => {
      state.IsOpen = action.payload;
    },
    setIniteState: (state, action) => {
      state.groupId = '';
      state.groupName = '';
      state.group = {};
      state.listRole = [];
      state.listSelect = [];
      state.IsOpen = false;
    },
    setCheckList: (state, action) => {
      state.listSelect = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getPermissionById.fulfilled, (state, action) => {
        state.groupId = action.payload.data.groupId;
        state.groupName = action.payload.data.groupName;
        state.listSelect = convertPermissionGroups(action.payload.data.permissions);
      })
      .addCase(getPermissionById.rejected, (state, action) => {
        console.error(action.error);
      })
      .addCase(createRole.fulfilled, (state, action) => {
        state.IsOpen = false;
        state.listSelect = [];
        state.listRole = [];
      });
  },
});

export const { addNewRole, setIsOpen, setIniteState, setCheckList } = GroupPermission.actions;

export default GroupPermission.reducer;
