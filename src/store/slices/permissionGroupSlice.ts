import { createAction, PrepareAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { PermissionGroupModel } from '@app/domain/UserModel';
import { addPermissionGroup, getPermissionGroups } from '@app/api/permissonGroup.api';

export interface PermissionGroupState {
  groups: PermissionGroupModel[];
  group: PermissionGroupModel | any;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
}

const initialState: PermissionGroupState = {
  groups: [],
  group: {},
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 10,
};

export const doGetPermissionGroups = createAsyncThunk(
  'permissionGroup/getPermissionGroups',
  async (payload: any, { dispatch }) => {
    const response = await getPermissionGroups(payload);
    return response;
  },
);

export const doAddPermissionGroups = createAsyncThunk(
  'permissionGroup/addPermissionGroups',
  async (payload: any, { dispatch }) => {
    const response = await addPermissionGroup(payload);
    return response;
  },
);

export const permissionGroupSlice = createSlice({
  name: 'permissionGroup',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(doGetPermissionGroups.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetPermissionGroups.fulfilled, (state, action) => {
      state.groups = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(doGetPermissionGroups.rejected, (state, action) => {
      state.loading = false;
    });
    // add permission group
    builder.addCase(doAddPermissionGroups.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doAddPermissionGroups.fulfilled, (state, action) => {
      state.groups.unshift(action.payload?.data);
      state.loading = false;
    });
    builder.addCase(doAddPermissionGroups.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export default permissionGroupSlice.reducer;
