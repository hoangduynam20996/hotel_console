import userReducer from '@app/store/slices/userSlice';
import authReducer from '@app/store/slices/authSlice';
import nightModeReducer from '@app/store/slices/nightModeSlice';
import themeReducer from '@app/store/slices/themeSlice';
import pwaReducer from '@app/store/slices/pwaSlice';
import modelSlice from '@app/store/slices/modelSlice';
import groupPermissionSlice from './groupPermissionSlice';
import permissionGroupReducer from './permissionGroupSlice';
import guestRequestSlice from './guestRequestSlice';
import hotelSlice from './hotelSlice';
import Headingslice from './Headingslice';
import ContentSlice from './ContentSlice';
import DescriptionSlice from './DescriptionSlice';
import sloganSlice from './sloganSlice';
import imageSlice from './imageSlice';
import roomSlice from './roomSlice';
import bookingSlice from './bookingSlice';
import serviceAmenitySlice from './serviceAmenitySlice';

export default {
  user: userReducer,
  auth: authReducer,
  nightMode: nightModeReducer,
  theme: themeReducer,
  pwa: pwaReducer,
  model: modelSlice,
  groupPermission: groupPermissionSlice,
  permissionGroup: permissionGroupReducer,
  guestRequest: guestRequestSlice,
  hotel: hotelSlice,
  heading: Headingslice,
  content: ContentSlice,
  description: DescriptionSlice,
  slogan: sloganSlice,
  image: imageSlice,
  serviceAmenity: serviceAmenitySlice,
  room: roomSlice,
  booking: bookingSlice,
};
