import { createAction, PrepareAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { GuestRequestModel } from '@app/domain/GuestRequestModel';
import { getGuestRequests, updateStatusGuestRequest, deleteGuestRequest } from '@app/api/guestRequest.api';
import { notificationController } from '@app/controllers/notificationController';

export interface GuestRequestState {
  guestRequests: GuestRequestModel[];
  guestRequest: GuestRequestModel | any;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
}

const initialState: GuestRequestState = {
  guestRequests: [],
  guestRequest: {},
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 5,
};

export const doGetGuestRequests = createAsyncThunk(
  'guestRequest/getGuestRequests',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getGuestRequests(payload);
      return response;
    } catch (error: any) {
      notificationController.error({
        message: `Lấy danh sách yêu cầu khách thất bại!`,
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doUpdateStatusGuestRequest = createAsyncThunk(
  'guestRequest/updateStatusGuestRequest',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateStatusGuestRequest(payload);
      notificationController.success({
        message: `Cập nhật trạng thái thành công!`,
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: `Cập nhật trạng thái thất bại!`,
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doDeleteGuestRequest = createAsyncThunk(
  'guestRequest/deleteGuestRequest',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await deleteGuestRequest(payload);
      notificationController.success({
        message: `Xóa yêu cầu khách thành công!`,
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: `Xóa yêu cầu khách thất bại!`,
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const guestRequestSlice = createSlice({
  name: 'guestRequest',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(doGetGuestRequests.pending, (state) => {
        state.loading = true;
      })
      .addCase(doGetGuestRequests.fulfilled, (state, action) => {
        state.loading = false;
        state.guestRequests = action.payload.data;
        state.totalPages = action.payload.meta.totalPage;
        state.currentPage = action.payload.meta.pageable.pageNumber + 1;
        state.limitPage = action.payload.meta.pageable.pageSize;
      })
      .addCase(doGetGuestRequests.rejected, (state, action) => {
        state.loading = false;
      })

      .addCase(doUpdateStatusGuestRequest.pending, (state) => {
        state.loading = true;
      })
      .addCase(doUpdateStatusGuestRequest.fulfilled, (state, action) => {
        state.loading = false;
        state.guestRequest = action.payload.data;
      })
      .addCase(doUpdateStatusGuestRequest.rejected, (state, action) => {
        state.loading = false;
      })

      .addCase(doDeleteGuestRequest.pending, (state) => {
        state.loading = true;
      })
      .addCase(doDeleteGuestRequest.fulfilled, (state, action) => {
        state.loading = false;
        const index = state.guestRequests.findIndex(
          (guestRequest) => guestRequest.guestRequestId === action.payload.data.guestRequestId,
        );
        if (index !== -1) {
          state.guestRequests.splice(index, 1);
        }
      })
      .addCase(doDeleteGuestRequest.rejected, (state, action) => {
        state.loading = false;
      });
  },
});

export const { actions: guestRequestActions } = guestRequestSlice;
export default guestRequestSlice.reducer;
