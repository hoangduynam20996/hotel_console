import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getBookings, getBooking, changeStatusBooking, changeStatusPayment } from '@app/api/booking.api';
import { notificationController } from '@app/controllers/notificationController';

export interface BookingState {
  bookings: any[];
  booking: any;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
}

const initialState: BookingState = {
  bookings: [],
  booking: [],
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 5,
};

export const doGetBookings = createAsyncThunk('booking/getBookings', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getBookings(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.response.data.message,
    });
    return rejectWithValue(error.response);
  }
});

export const doGetBooking = createAsyncThunk('booking/getBooking', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getBooking(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.response.data.message,
    });
    return rejectWithValue(error.response);
  }
});

export const doChangeStatusBooking = createAsyncThunk(
  'booking/changeStatusBooking',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await changeStatusBooking(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.response.data.message,
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doChangeStatusPayment = createAsyncThunk(
  'booking/changeStatusPayment',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await changeStatusPayment(payload);
      notificationController.success({
        message: payload.t('notification.MSG_099'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_100'),
        description: error.response.data.message,
      });
      return rejectWithValue(error.response);
    }
  },
);

export const bookingSlice = createSlice({
  name: 'booking',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(doGetBookings.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetBookings.fulfilled, (state, action) => {
      state.loading = false;
      state.bookings = action.payload.data;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(doGetBookings.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetBooking.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetBooking.fulfilled, (state, action) => {
      state.loading = false;
      state.booking = action.payload.data;
    });
    builder.addCase(doGetBooking.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doChangeStatusBooking.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangeStatusBooking.fulfilled, (state, action) => {
      state.loading = false;
      // find and update booking
      state.bookings = state.bookings.map((booking) => {
        if (booking.bookingId === action.meta.arg.values.bookingId) {
          booking.status = action.meta.arg.values.status;
        }
        return booking;
      });
    });
    builder.addCase(doChangeStatusBooking.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doChangeStatusPayment.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangeStatusPayment.fulfilled, (state, action) => {
      state.loading = false;
      console.log(action);
      // find and update booking
      state.bookings = state.bookings.map((booking) => {
        if (booking.bookingId === action.meta.arg.values.bookingId) {
          booking.payment.status = action.payload.data.status;
        }
        return booking;
      });
    });
    builder.addCase(doChangeStatusPayment.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export const {} = bookingSlice.actions;

export default bookingSlice.reducer;
