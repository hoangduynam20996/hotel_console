import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { notificationController } from '@app/controllers/notificationController';
import { uploadImages, deleteImage, getHotelImage } from '@app/api/images.api';

export interface ImageState {
  loading: boolean;
  images: any[];
}

const initialState: ImageState = {
  loading: false,
  images: [],
};

export const doGetHotelImages = createAsyncThunk('image/getHotelImages', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHotelImage(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const uploadImage = createAsyncThunk('image/uploadImage', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await uploadImages(payload);
    console.log(response);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const deleteImageById = createAsyncThunk('image/deleteImage', async (id: string, { rejectWithValue }) => {
  try {
    const response = await deleteImage(id);

    return response;
  } catch (error: any) {
    notificationController.error({
      message: 'Error',
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

const imageSlice = createSlice({
  name: 'image',
  initialState,
  reducers: {
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doGetHotelImages.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetHotelImages.fulfilled, (state, action) => {
      state.images = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetHotelImages.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(uploadImage.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(uploadImage.fulfilled, (state, action) => {
      state.images.unshift(action.payload.data[0]);
      state.loading = false;
    });
    builder.addCase(uploadImage.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(deleteImageById.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteImageById.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(deleteImageById.rejected, (state) => {
      state.loading = false;
    });
  },
});

export const { setLoading } = imageSlice.actions;
export default imageSlice.reducer;
