import { createAction, PrepareAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { UserModel } from '@app/domain/UserModel';
import { persistUser, readUser } from '@app/services/localStorage.service';
import { deleteToken, persistToken } from '@app/services/localStorage.service';
import { activeUser, deleteUser, addUser, getUser, getUsers, updateUser } from '@app/api/userManager.api';
import { getProfileUser, getUserPermission } from '@app/api/userProfile.api';
import { addUserForGroups } from '@app/api/permissonGroup.api';
import { notificationController } from '@app/controllers/notificationController';
export interface UserState {
  users: UserModel[];
  userById: UserModel | any;
  groupsByUser: [] | any;
  user: UserModel | any;
  permissions: any;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  roles: any;
}

const initialState: UserState = {
  users: [],
  userById: {},
  groupsByUser: [],
  user: readUser(),
  permissions: [],
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 10,
  roles: [],
};

export const setUser = createAction<PrepareAction<UserModel>>('user/setUser', (newUser) => {
  persistUser(newUser);

  return {
    payload: newUser,
  };
});

export const doGetUsers = createAsyncThunk('user/getUsers', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getUsers(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_001'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetUser = createAsyncThunk('user/getUser', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getUser(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_002'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doAddUser = createAsyncThunk('user/addUser', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await addUser(payload);
    notificationController.success({
      message: payload.t('notification.MSG_003'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_004'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doAddUserForGroups = createAsyncThunk(
  'permissionGroup/addUserForGroups',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await addUserForGroups(payload);
      notificationController.success({
        message: payload.t('notification.MSG_005'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_006'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doActiveUser = createAsyncThunk('user/activeUser', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await activeUser(payload);
    notificationController.success({
      message: payload.t('user.success.MSG_002'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_007'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doUpdateUser = createAsyncThunk('user/updateUser', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await updateUser(payload);
    notificationController.success({
      message: payload.t('notification.MSG_099'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_100'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doDeleteUser = createAsyncThunk('user/deleteUser', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await deleteUser(payload);
    notificationController.success({
      message: payload.t('user.error.message1'),
      description: response.message,
      placement: 'topRight',
    });
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_008'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doGetUserPermission = createAsyncThunk(
  'user/getUserPermission',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await getUserPermission();
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_009'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const setUserProfile = createAsyncThunk('user/setUser', async () =>
  getProfileUser().then((data) => {
    return data.data;
  }),
);

export const doGetUserProfile = createAsyncThunk('user/getUserProfile', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getProfileUser();
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_010'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(setUser, (state, action) => {
      state.user = action.payload;
    });
    builder.addCase(setUserProfile.fulfilled, (state, action) => {
      state.user = action.payload;

      if (action.payload && action.payload.permissionGroups) {
        state.roles = action.payload.permissionGroups;
      }
    });

    builder.addCase(doGetUsers.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetUsers.fulfilled, (state, action) => {
      state.users = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(doGetUsers.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetUser.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetUser.fulfilled, (state, action) => {
      state.userById = action.payload.data;
      state.groupsByUser = action.payload.data.groups;
      state.loading = false;
    });
    builder.addCase(doGetUser.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doAddUser.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doAddUser.fulfilled, (state, action) => {
      state.users.unshift(action.payload.data);
      state.loading = false;
    });
    builder.addCase(doAddUser.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doAddUserForGroups.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doAddUserForGroups.fulfilled, (state, action) => {
      state.groupsByUser = action.payload.data.groupIds;
      state.loading = false;
    });
    builder.addCase(doAddUserForGroups.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doActiveUser.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doActiveUser.fulfilled, (state, action) => {
      state.loading = false;
      const index = state.users.findIndex((user: UserModel) => user.email === action.payload.data.email);
      state.users[index] = action.payload.data;
    });
    builder.addCase(doUpdateUser.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doUpdateUser.fulfilled, (state, action) => {
      state.loading = false;
      const index = state.users.findIndex((user: UserModel) => user.id === action.meta.arg.values.userId);
      state.users[index] = action.payload.data;
    });
    builder.addCase(doUpdateUser.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doActiveUser.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doDeleteUser.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doDeleteUser.fulfilled, (state, action) => {
      state.loading = false;
      const index = state.users.findIndex((user: UserModel) => user.email === action.payload.data.email);
      state.users[index] = action.payload.data;
    });
    builder.addCase(doDeleteUser.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetUserPermission.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetUserPermission.fulfilled, (state, action) => {
      state.permissions = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetUserPermission.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doGetUserProfile.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetUserProfile.fulfilled, (state, action) => {
      state.user = action.payload.data;
      state.loading = false;
    });
    builder.addCase(doGetUserProfile.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export default userSlice.reducer;
