import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { HeadingModel } from '@app/domain/HeadingModel';
import {
  changeStatus,
  createHeadings,
  deleteHeadings,
  getHeadingParent,
  getHeadings,
  updateHeadings,
} from '@app/api/HeadingApi';
import { notificationController } from '@app/controllers/notificationController';

export interface HeadingState {
  headings: HeadingModel[];
  heading: HeadingModel | null;
  headingId: string;
  loading: boolean;
  totalPages: number;
  currentPage: number;
  limitPage: number;
  titleHeading: string;
  languageCode: string;
}

const initialState: HeadingState = {
  headings: [],
  heading: null,
  headingId: '',
  loading: false,
  totalPages: 0,
  currentPage: 1,
  limitPage: 10,
  titleHeading: '',
  languageCode: 'EN',
};

export const doGetHeadings = createAsyncThunk('heading/getHeadings', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHeadings(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_101'),
      description: error.options.message,
      placement: 'topRight',
    });

    return rejectWithValue(error.response);
  }
});

export const doDeleteHeadings = createAsyncThunk(
  'heading/deleteHeadings',
  async (payload: any, { rejectWithValue }) => {
    try {
      console.log(payload);
      const response = await deleteHeadings(payload);
      notificationController.success({
        message: payload.t('notification.MSG_104'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_108'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doUpdateHeadings = createAsyncThunk(
  'heading/updateHeadings',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await updateHeadings(payload);
      notificationController.success({
        message: payload.t('notification.MSG_102'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_106'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doCreateHeadings = createAsyncThunk(
  'heading/createHeadings',
  async (payload: any, { rejectWithValue }) => {
    try {
      const response = await createHeadings(payload);
      notificationController.success({
        message: payload.t('notification.MSG_103'),
        description: response.message,
        placement: 'topRight',
      });
      return response;
    } catch (error: any) {
      notificationController.error({
        message: payload.t('notification.MSG_107'),
        description: error.options.message,
        placement: 'topRight',
      });
      return rejectWithValue(error.response);
    }
  },
);

export const doGetHeading = createAsyncThunk('heading/getHeadingParent', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await getHeadingParent(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: payload.t('notification.MSG_101'),
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const doChangeStatus = createAsyncThunk('heading/changeStatus', async (payload: any, { rejectWithValue }) => {
  try {
    const response = await changeStatus(payload);
    return response;
  } catch (error: any) {
    notificationController.error({
      message: 'can not change status',
      description: error.options.message,
      placement: 'topRight',
    });
    return rejectWithValue(error.response);
  }
});

export const HeadingSlice = createSlice({
  name: 'heading',
  initialState,
  reducers: {
    setTitleHeading: (state, action) => {
      state.titleHeading = action.payload;
    },
    setLanguageCode: (state, action) => {
      state.languageCode = action.payload;
    },
    setCurrentPage: (state, action) => {
      state.languageCode = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doGetHeadings.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doGetHeadings.fulfilled, (state, action) => {
      state.headings = action.payload.data;
      state.loading = false;
      state.totalPages = action.payload.meta.totalPage;
      state.currentPage = action.payload.meta.pageable.pageNumber + 1;
      state.limitPage = action.payload.meta.pageable.pageSize;
    });
    builder.addCase(doGetHeadings.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(doCreateHeadings.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doCreateHeadings.fulfilled, (state, action) => {
      state.loading = false;
      state.headings.unshift(action.payload.data);
    });
    builder.addCase(doCreateHeadings.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doUpdateHeadings.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doUpdateHeadings.fulfilled, (state, action) => {
      state.headings = state.headings.map((heading) => {
        if (heading.parentId === action.payload.data.parentId) {
          return action.payload.data;
        }
        return heading;
      });
      state.loading = false;
    });

    builder.addCase(doUpdateHeadings.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doGetHeading.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(doGetHeading.fulfilled, (state, action) => {
      state.heading = action.payload.data;
      state.loading = false;
    });

    builder.addCase(doGetHeading.rejected, (state, action) => {
      state.loading = false;
    });

    builder.addCase(doChangeStatus.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(doChangeStatus.fulfilled, (state, action) => {
      state.loading = false;
      // find and update the hotel status
      state.headings = state.headings.map((heading) => {
        if (heading.parentId === action.meta.arg.values.parentId) {
          heading.status = action.meta.arg.values.status;
        }
        return heading;
      });
    });
    builder.addCase(doChangeStatus.rejected, (state, _) => {
      state.loading = false;
    });

    builder.addCase(doDeleteHeadings.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(doDeleteHeadings.fulfilled, (state, action) => {
      console.log(action);
      state.loading = false;
      const index = state.headings.findIndex((heading) => heading.parentId === action.meta.arg.id);
      if (index !== -1) {
        state.headings.splice(index, 1);
      }
    });
    builder.addCase(doDeleteHeadings.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export const { setTitleHeading, setLanguageCode, setCurrentPage } = HeadingSlice.actions;
export default HeadingSlice.reducer;
