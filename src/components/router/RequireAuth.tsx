import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { WithChildrenProps } from '@app/types/generalTypes';

const RequireAuth: React.FC<WithChildrenProps> = ({ children }) => {
  const location = useLocation();
  const currentPath = location.pathname;
  const token = useAppSelector((state) => state.auth.token);

  if (currentPath === '/auth/login') {
    return token ? <Navigate to="/" replace /> : <>{children}</>;
  } else {
    return token ? <>{children}</> : <Navigate to="/auth/login" replace />;
  }
};

export default RequireAuth;
