import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

// no lazy loading for auth pages to avoid flickering
const AuthLayout = React.lazy(() => import('@app/components/layouts/AuthLayout/AuthLayout'));
import LoginPage from '@app/pages/LoginPage';
import SignUpPage from '@app/pages/SignUpPage';
import ForgotPasswordPage from '@app/pages/ForgotPasswordPage';
import SecurityCodePage from '@app/pages/SecurityCodePage';
import NewPasswordPage from '@app/pages/NewPasswordPage';
import LockPage from '@app/pages/LockPage';
import MainLayout from '@app/components/layouts/main/MainLayout/MainLayout';
import ProfileLayout from '@app/components/profile/ProfileLayout';
import RequireAuth from '@app/components/router/RequireAuth';
import { withLoading } from '@app/hocs/withLoading.hoc';
import NftDashboardPage from '@app/pages/DashboardPages/NftDashboardPage';
import MedicalDashboardPage from '@app/pages/DashboardPages/MedicalDashboardPage';

const User = React.lazy(() => import('@app/pages/User'));
const UserGroup = React.lazy(() => import('@app/pages/UserGroup'));
const Group = React.lazy(() => import('@app/pages/Group'));
const Role = React.lazy(() => import('@app/pages/Role'));
const Error404Page = React.lazy(() => import('@app/pages/Error404Page'));
const Error403Page = React.lazy(() => import('@app/pages/Error403Page'));
const PersonalInfoPage = React.lazy(() => import('@app/pages/PersonalInfoPage'));
const SecuritySettingsPage = React.lazy(() => import('@app/pages/SecuritySettingsPage'));
const NotificationsPage = React.lazy(() => import('@app/pages/NotificationsPage'));
const PaymentsPage = React.lazy(() => import('@app/pages/PaymentsPage'));
const GuestRequestPage = React.lazy(() => import('@app/pages/GuestRequest'));
const HotelPage = React.lazy(() => import('@app/pages/Hotel'));

const HeadingRequestPage = React.lazy(() => import('@app/pages/Heading'));
const ContentRequest = React.lazy(() => import('@app/pages/tittleContent'));
const DescriptionsRequest = React.lazy(() => import('@app/pages/Descriptions'));

const HotelDetailPage = React.lazy(() => import('@app/pages/HotelDetailPage'));
const RoomsPage = React.lazy(() => import('@app/pages/Rooms'));
const Bookings = React.lazy(() => import('@app/pages/Bookings'));

const Logout = React.lazy(() => import('./Logout'));
const HeadingDetailsPage = React.lazy(() => import('@app/pages/HeadingDetails'));
const ContentDetailsPage = React.lazy(() => import('@app/pages/ContentDetails'));
const DescriptionsDetialsPage = React.lazy(() => import('@app/pages/DescriptionDetails'));
const SloganPage = React.lazy(() => import('@app/pages/Slogan'));
const SloganDetails = React.lazy(() => import('@app/pages/SloganDetail'));

export const NFT_DASHBOARD_PATH = '/';
export const MEDICAL_DASHBOARD_PATH = '/medical-dashboard';

const MedicalDashboard = withLoading(MedicalDashboardPage);
const NftDashboard = withLoading(NftDashboardPage);
const UserDashboard = withLoading(User);
const UserGroupDashboard = withLoading(UserGroup);
const GroupDashboard = withLoading(Group);
const RoleDashboard = withLoading(Role);
const Error404 = withLoading(Error404Page);
const Error403 = withLoading(Error403Page);

// Profile
const PersonalInfo = withLoading(PersonalInfoPage);
const SecuritySettings = withLoading(SecuritySettingsPage);
const Notifications = withLoading(NotificationsPage);
const Payments = withLoading(PaymentsPage);
const AuthLayoutFallback = withLoading(AuthLayout);
const LogoutFallback = withLoading(Logout);

// Hotel
const HotelDashboard = withLoading(HotelPage);
const HotelDetail = withLoading(HotelDetailPage);

// Rooms
const Rooms = withLoading(RoomsPage);

// Bookings
const BookingsDashboard = withLoading(Bookings);

// Guest
const GuestDashboard = withLoading(GuestRequestPage);

//Content
const ContentBoard = withLoading(ContentRequest);
const HeadingBoard = withLoading(HeadingRequestPage);
const DescriptionsBoard = withLoading(DescriptionsRequest);
const HeadingDetailsBoard = withLoading(HeadingDetailsPage);
const ContentDetailsBoard = withLoading(ContentDetailsPage);
const DescriptionsDetailsBoard = withLoading(DescriptionsDetialsPage);
const SloganBoard = withLoading(SloganPage);
const SloganDetailsBoard = withLoading(SloganDetails);

export const AppRouter: React.FC = () => {
  const protectedLayout = (
    <RequireAuth>
      <MainLayout />
    </RequireAuth>
  );

  return (
    <BrowserRouter>
      <Routes>
        <Route path={NFT_DASHBOARD_PATH} element={protectedLayout}>
          <Route index element={<NftDashboard />} />
          <Route path={MEDICAL_DASHBOARD_PATH} element={<MedicalDashboard />} />
          <Route path="manage">
            <Route path="users" element={<UserDashboard />} />
            <Route path="users/user-group/:id" element={<UserGroupDashboard />} />
            <Route path="Group" element={<GroupDashboard />} />
            <Route path="Role" element={<RoleDashboard />} />
          </Route>
          <Route path="profile" element={<ProfileLayout />}>
            <Route path="personal-info" element={<PersonalInfo />} />
            <Route path="security-settings" element={<SecuritySettings />} />
            <Route path="notifications" element={<Notifications />} />
            <Route path="payments" element={<Payments />} />
          </Route>

          <Route path="manage">
            <Route path="headings" element={<HeadingBoard />} />
            <Route path="contents" element={<ContentBoard />} />
            <Route path="heading/add" element={<HeadingDetailsBoard />} />
            <Route path="heading/parent/:parentId" element={<HeadingDetailsBoard />} />
            <Route path="content/add" element={<ContentDetailsBoard />} />
            <Route path="content/content/:parentId" element={<ContentDetailsBoard />} />
            <Route path="descriptions" element={<DescriptionsBoard />} />
            <Route path="des/parent/:parentId" element={<DescriptionsDetailsBoard />} />
            <Route path="des/add" element={<DescriptionsDetailsBoard />} />
            <Route path="slogans" element={<SloganBoard />} />
            <Route path="slogan/:parentId" element={<SloganDetailsBoard />} />
            <Route path="slogans/add" element={<SloganDetailsBoard />} />
          </Route>
          <Route path="guest_request" element={<GuestDashboard />} />
          <Route path="hotels" element={<HotelDashboard />} />
          <Route path="hotel/add" element={<HotelDetail />} />
          <Route path="hotel/detail" element={<HotelDetail />} />
          <Route path="rooms/list/:id" element={<Rooms />} />
          <Route path="bookings" element={<BookingsDashboard />} />
          <Route path="*" element={<Error404 />} />
          <Route path="/forbidden" element={<Error403 />} />
        </Route>
        <Route path="/auth" element={<AuthLayoutFallback />}>
          <Route
            path="login"
            element={
              <RequireAuth>
                <LoginPage />
              </RequireAuth>
            }
          />
          <Route path="sign-up" element={<SignUpPage />} />
          <Route
            path="lock"
            element={
              <RequireAuth>
                <LockPage />
              </RequireAuth>
            }
          />
          <Route path="forgot-password" element={<ForgotPasswordPage />} />
          <Route path="security-code" element={<SecurityCodePage />} />
          <Route path="new-password" element={<NewPasswordPage />} />
        </Route>
        <Route path="/logout" element={<LogoutFallback />} />
      </Routes>
    </BrowserRouter>
  );
};
