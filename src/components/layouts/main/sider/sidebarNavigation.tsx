import React from 'react';
import {
  AppstoreOutlined,
  BookOutlined,
  ClusterOutlined,
  HomeOutlined,
  IdcardOutlined,
  PhoneOutlined,
} from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

export interface SidebarNavigationItem {
  title: string;
  key: string;
  url?: string;
  children?: SidebarNavigationItem[];
  icon?: React.ReactNode;
  hasPermissions?: string[];
}

export const sidebarNavigation: SidebarNavigationItem[] = [
  {
    title: 'User',
    key: 'management',
    icon: <IdcardOutlined />,
    url: '/manage/users',
    hasPermissions: ['admin_read'],
  },
  {
    title: 'Group',
    key: 'Group',
    icon: <ClusterOutlined />,
    url: '/manage/group',
    hasPermissions: ['admin_read'],
  },
  {
    title: 'Hotels',
    key: 'hotels',
    icon: <HomeOutlined />,
    url: '/hotels',
    hasPermissions: ['hotel_create_info', 'hotel_view_info', 'hotel_update_info', 'hotel_update_status'],
  },
  {
    title: 'Bookings',
    key: 'bookings',
    icon: <BookOutlined />,
    url: '/bookings',
    hasPermissions: ['admin_read'],
  },
  // {
  //     title: 'Content management',
  //     key: 'contentmana',
  //     icon: <AppstoreOutlined />,
  //     hasPermissions: ['content_read'],
  //     children: [
  //         {
  //             title: 'Heading Content',
  //             key: 'heading',
  //             url: '/manage/headings',
  //             hasPermissions: ['content_read'],
  //         },
  //         {
  //             title: 'Content',
  //             key: 'content',
  //             url: '/manage/contents',
  //             hasPermissions: ['content_read'],
  //         },
  //         {
  //             title: 'Description Content',
  //             key: 'descripton',
  //             url: '/manage/descriptions',
  //             hasPermissions: ['content_read'],
  //         },
  //         {
  //             title: 'Slogan',
  //             key: 'slogan',
  //             url: '/manage/slogans',
  //             hasPermissions: ['content_read'],
  //         },
  //     ],
  // },
  // {
  //     title: 'Guest Request',
  //     key: 'guest_request',
  //     icon: <PhoneOutlined />,
  //     url: '/guest_request',
  //     hasPermissions: ['guest_read'],
  // },
];
