import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import * as S from './SiderMenu.styles';
import { sidebarNavigation, SidebarNavigationItem } from '../sidebarNavigation';
import { useSelector } from 'react-redux';

interface SiderContentProps {
  setCollapsed: (isCollapsed: boolean) => void;
}

const sidebarNavFlat = sidebarNavigation.reduce(
  (result: SidebarNavigationItem[], current) =>
    result.concat(current.children && current.children.length > 0 ? current.children : current),
  [],
);

const SiderMenu: React.FC<SiderContentProps> = ({ setCollapsed }) => {
  const { t } = useTranslation();
  const location = useLocation();
  const { permissions } = useSelector((state: any) => state.user);

  const currentMenuItem = sidebarNavFlat.find(({ url }) => url === location.pathname);
  const defaultSelectedKeys = currentMenuItem ? [currentMenuItem.key] : [];

  const openedSubmenu = sidebarNavigation.find(({ children }) =>
    children?.some(({ url }) => url === location.pathname),
  );
  const defaultOpenKeys = openedSubmenu ? [openedSubmenu.key] : [];

  return (
    <S.Menu
      mode="inline"
      defaultSelectedKeys={defaultSelectedKeys}
      defaultOpenKeys={defaultOpenKeys}
      onClick={() => setCollapsed(true)}
      items={sidebarNavigation.map((nav) => {
        const isSubMenu = nav.children?.length;
        const hasAdminMasterPermission = permissions.some((p: any) => p.permissionCode === 'admin_master');
        const hasSuperAdminPermission = permissions.some((p: any) => p.permissionCode === 'admin_admin');
        const hasPermissions = nav.hasPermissions || [];
        const hasAllPermissions = hasPermissions.some((code) => {
          return permissions.some((p: any) => p.permissionCode === code);
        });
        if (hasAdminMasterPermission || hasSuperAdminPermission || hasAllPermissions) {
          return {
            key: nav.key,
            title: t(nav.title),
            label: isSubMenu ? t(nav.title) : <Link to={nav.url || ''}>{t(nav.title)}</Link>,
            icon: nav.icon,
            children:
              isSubMenu &&
              nav.children &&
              nav.children.map((childNav) => {
                const hasAdminMasterPermission = permissions.some((p: any) => p.permissionCode === 'admin_master');
                const hasSuperAdminPermission = permissions.some((p: any) => p.permissionCode === 'admin_admin');
                const hasPermissions = childNav.hasPermissions || [];
                const hasAllPermissions = hasPermissions.some((code) => {
                  return permissions.some((p: any) => p.permissionCode === code);
                });
                if (hasAdminMasterPermission || hasSuperAdminPermission || hasAllPermissions) {
                  return {
                    key: childNav.key,
                    title: t(childNav.title),
                    label: <Link to={childNav.url || ''}>{t(childNav.title)}</Link>,
                  };
                }
                return null;
              }),
          };
        }
        return null;
      })}
    />
  );
};

export default SiderMenu;
