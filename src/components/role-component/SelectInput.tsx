import React from 'react';
import { Select } from 'antd';

interface selectProps {
  values: any[];
  onSelect: (value: any) => void;
}

const onChange = (value: string) => {
  console.log(`selected ${value}`);
};

const onSearch = (value: string) => {
  console.log('search:', value);
};

// Filter `option.label` match the user type `input`
const filterOption = (input: string, option?: { label: string; value: string }) =>
  (option?.label ?? '').toLowerCase().includes(input.toLowerCase());

const App: React.FC<selectProps> = ({ values, onSelect }) => (
  <div className="ml-4 w-1/5 inline-block">
    <Select
      showSearch
      allowClear
      placeholder="Select a person"
      optionFilterProp="children"
      onChange={onSelect}
      onSearch={onSearch}
      filterOption={filterOption}
      options={values}
    />
  </div>
);

export default App;
