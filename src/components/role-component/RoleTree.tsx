import { Tree } from 'antd';
import type { TreeDataNode } from 'antd';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addNewRole, setCheckList } from '@app/store/slices/groupPermissionSlice';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { useTranslation } from 'react-i18next';

interface TreeProps {
  treeData: TreeDataNode[];
}

const formatTreeData = (data: TreeDataNode[]) => {
  return data.map((node: TreeDataNode) => {
    return {
      ...node,
      title: formatTitle(node.title),
    };
  });
};

const formatTitle = (title: any) => {
  return title
    .split('_')
    .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
};

const TreeComp: React.FC<TreeProps> = ({ treeData }) => {
  const dispatch = useDispatch();
  const [expandedKeys, setExpandedKeys] = useState<React.Key[]>([]);
  const [checkedKeys, setCheckedKeys] = useState<React.Key[] | any>([]);
  const [autoExpandParent, setAutoExpandParent] = useState<boolean>(true);
  const checkedList = useAppSelector((state) => state.groupPermission.listSelect);
  const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([]);
  const { t } = useTranslation();
  const formattedTreeData = formatTreeData(treeData);

  useEffect(() => {
    setCheckedKeys(checkedList);
  }, [checkedList]);

  const onExpand = (expandedKeysValue: React.Key[]) => {
    setExpandedKeys(expandedKeysValue);
    setAutoExpandParent(false);
  };

  const onCheck = (checkedKeysValue: React.Key[] | any) => {
    dispatch(setCheckList(checkedKeysValue));
    setCheckedKeys(checkedKeysValue);

    const check = extractPermissionCodes(checkedKeysValue);

    dispatch(
      addNewRole({
        listCode: check,
      }),
    );
  };

  const onSelect = (selectedKeysValue: React.Key[], info: any) => {
    setSelectedKeys(selectedKeysValue);
  };

  // extract the key to get code permission
  const extractPermissionCodes = (inputList: any) => {
    if (inputList.length === 0) {
      // Handle the case when the input list is empty
      return null;
    }
    const listCode: any = [];
    inputList.forEach((item: string) => {
      const getLastIndex = item.lastIndexOf('permissionId-');
      if (getLastIndex !== -1) {
        const code = item.substring(getLastIndex + 'permissionId-'.length);
        listCode.push({ permissionId: code });
      }
    });

    return listCode;
  };
  console.log('treeData', treeData);

  return (
    <div className="rounded-2xl bg-white shadow-2xl p-6 max-h-[40rem] overflow-x-auto">
      <div className="text-xl font-bold">{t('group.addrole.list_permission')}:</div>
      <Tree
        checkable
        onExpand={onExpand}
        expandedKeys={expandedKeys}
        autoExpandParent={autoExpandParent}
        onCheck={onCheck}
        checkedKeys={checkedKeys}
        onSelect={onSelect}
        selectedKeys={selectedKeys}
        treeData={formattedTreeData}
      />
    </div>
  );
};

export default TreeComp;
