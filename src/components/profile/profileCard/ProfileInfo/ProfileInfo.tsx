import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { UserModel } from '@app/domain/UserModel';
import * as S from './ProfileInfo.styles';
import { BaseAvatar } from '@app/components/common/BaseAvatar/BaseAvatar';

interface ProfileInfoProps {
  profileData: UserModel | any;
}

export const ProfileInfo: React.FC<ProfileInfoProps> = ({ profileData }) => {
  const { t } = useTranslation();

  console.log(profileData);

  return profileData ? (
    <S.Wrapper>
      <S.ImgWrapper>
        <BaseAvatar shape="circle" src="https://cdn-icons-png.flaticon.com/512/9187/9187604.png" alt="Profile" />
      </S.ImgWrapper>
      <S.Title>{profileData?.fullName}</S.Title>
    </S.Wrapper>
  ) : null;
};
