import React, { ReactNode } from 'react';
import { useSelector } from 'react-redux';

interface Permission {
  permissionCode: string;
}

interface PermissionCheckerProps {
  hasPermissions: string[];
  children: ReactNode;
}

export const PermissionChecker: React.FC<PermissionCheckerProps> = ({ hasPermissions, children }) => {
  const { permissions } = useSelector((state: any) => state.user);
  const hasAdminMasterPermission = permissions.some((p: any) => p.permissionCode === 'admin_master');
  const hasSuperAdminPermission = permissions.some((p: any) => p.permissionCode === 'admin_admin');
  const hasAllPermissions = hasPermissions.some((code) => {
    return permissions.some((p: any) => p.permissionCode === code);
  });
  if (hasAdminMasterPermission || hasSuperAdminPermission || hasAllPermissions) {
    return <>{children}</>;
  }
  return null;
};

export default PermissionChecker;
