import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { useAppDispatch } from '@app/hooks/reduxHooks';
import { doLogin } from '@app/store/slices/authSlice';
import { notificationController } from '@app/controllers/notificationController';
import { ReactComponent as FacebookIcon } from '@app/assets/icons/facebook.svg';
import { ReactComponent as GoogleIcon } from '@app/assets/icons/google.svg';
import * as S from './LoginForm.styles';
import * as Auth from '@app/components/layouts/AuthLayout/AuthLayout.styles';

interface LoginFormData {
  email: string;
  password: string;
}

export const initValues: LoginFormData = {
  email: '',
  password: '',
};

export const LoginForm: React.FC = () => {
  const [form] = BaseForm.useForm();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  const [isLoading, setLoading] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const handleSubmit = (values: LoginFormData) => {
    setLoading(true);
    dispatch(doLogin(values))
      .unwrap()
      .then(() => navigate('/'))
      .catch((err) => {
        notificationController.error({
          message: t('login.messages.login_fail'),
          description: err.message,
          placement: 'topRight',
        });
        setLoading(false);
      });
  };

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  return (
    <Auth.FormWrapper>
      <BaseForm
        layout="vertical"
        onFinish={handleSubmit}
        requiredMark="optional"
        initialValues={initValues}
        form={form}
        onValuesChange={() => {
          const { email, password } = form.getFieldsValue();
          const emailPattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
          setIsButtonDisabled(!emailPattern.test(email) || !email || !password);
        }}
      >
        <Auth.FormTitle>{t('login.title')}</Auth.FormTitle>
        <S.LoginDescription>{t('login.loginInfo')}</S.LoginDescription>
        <Auth.FormItem
          name="email"
          label={t('login.email')}
          rules={[
            { required: true, message: t('user.modal.message.email_message1') },
            {
              type: 'email',
              message: t('user.modal.message.email_message2'),
            },
          ]}
        >
          <Auth.FormInput placeholder={t('login.email')} onBlur={handleBlur('email')} />
        </Auth.FormItem>
        <Auth.FormItem
          name="password"
          label={t('login.password')}
          rules={[{ required: true, message: t('user.modal.message.password_message') }]}
        >
          <Auth.FormInputPassword placeholder={t('login.password')} onBlur={handleBlur('password')} />
        </Auth.FormItem>
        <Auth.ActionsWrapper>
          {/* <BaseForm.Item name="rememberMe" valuePropName="checked" noStyle>
                        <Auth.FormCheckbox>
                            <S.RememberMeText>{t('login.remember_me')}</S.RememberMeText>
                        </Auth.FormCheckbox>
                    </BaseForm.Item> */}
          <Link to="/auth/forgot-password">
            <S.ForgotPasswordText>{t('login.forgot_password')}</S.ForgotPasswordText>
          </Link>
        </Auth.ActionsWrapper>
        <BaseForm.Item noStyle>
          <Auth.SubmitButton type="primary" htmlType="submit" loading={isLoading} disabled={isButtonDisabled}>
            {t('login.login_btn')}
          </Auth.SubmitButton>
        </BaseForm.Item>
      </BaseForm>
    </Auth.FormWrapper>
  );
};
