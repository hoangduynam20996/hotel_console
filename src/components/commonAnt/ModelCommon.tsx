import React, { useState, useEffect } from 'react';
import { Modal } from 'antd';
import { Input } from 'antd';
import { useDispatch } from 'react-redux';
import { setIsOpen } from '@app/store/slices/modelSlice';
import { BaseButton } from '../common/BaseButton/BaseButton';
import { useTranslation } from 'react-i18next';

import { doAddPermissionGroups } from '@app/store/slices/permissionGroupSlice';
interface AppProps {
  initialOpen?: boolean;
}

const ModelCommon: React.FC<AppProps> = ({ initialOpen }) => {
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(initialOpen);
  const disPatch = useDispatch();
  const { t } = useTranslation();

  const [dataPermission, setDataPermission] = useState({
    groupName: '',
    description: '',
  });

  useEffect(() => {
    setOpen(initialOpen);
  }, [initialOpen]);
  const handleOk = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setOpen(false);
    }, 3000);
  };

  const handleCancel = () => {
    setOpen(false);
    disPatch(setIsOpen(false));
  };

  const handleAddPermissionGroup = async () => {
    disPatch(doAddPermissionGroups(dataPermission));
    disPatch(setIsOpen(false));
  };

  return (
    <>
      <Modal
        visible={open}
        title="Title"
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <div className="flex justify-center">
            <BaseButton key="back" onClick={handleCancel}>
              Cancel
            </BaseButton>
            <BaseButton type="primary" loading={loading} onClick={handleAddPermissionGroup}>
              Add Group
            </BaseButton>
          </div>,
        ]}
      >
        <h3 className="font-bold mb-2 text-[32px]">Add Group</h3>
        <div className="mb-2">
          <span className="font-semibold">Group Name :</span>
          <Input
            onChange={(e) => setDataPermission({ ...dataPermission, groupName: e.target.value })}
            placeholder="Group Name"
          />
        </div>
        <div className="mb-2">
          <span className="font-semibold">Description :</span>
          <Input
            onChange={(e) => setDataPermission({ ...dataPermission, description: e.target.value })}
            placeholder="Description"
          />
        </div>
      </Modal>
    </>
  );
};

export default ModelCommon;
