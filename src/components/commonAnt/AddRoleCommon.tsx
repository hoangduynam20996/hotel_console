import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'antd';
import RoleTree from '@app/components/role-component/RoleTree';
import { useDispatch } from 'react-redux';
import { setIsOpen } from '@app/store/slices/groupPermissionSlice';

import { useTranslation } from 'react-i18next';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { getAllPermission } from '@app/api/permissionGroupRole.api';
import { createRole, setCheckList } from '@app/store/slices/groupPermissionSlice';
import { Permission } from '@app/domain/UserModel';
import { notificationController } from '@app/controllers/notificationController';
import { Divider, Space, Tag } from 'antd';
import PermissionChecker from '../auth/PermissionChecker/PermissionChecker';

interface AppProps {
  initialOpen?: boolean;
}

interface ResponseObject {
  status: number;
}

const AddRoleCommon: React.FC<AppProps> = ({ initialOpen }) => {
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(initialOpen);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const groupName = useAppSelector((state) => state.groupPermission.groupName);
  const groupId = useAppSelector((state) => state.groupPermission.groupId);
  const selectRole = useAppSelector((state) => state.groupPermission.listRole);
  const selectList = useAppSelector((state) => state.groupPermission.listSelect);
  const [treeData, setTreeData] = useState([]);
  const currentPage = 1;
  const pageSize = 99;

  const [dataPermission, setDataPermission] = useState({
    permissionGroupName: '',
    description: '',
  });

  useEffect(() => {
    setOpen(initialOpen);
  }, [initialOpen]);

  const handleOk = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setOpen(false);
    }, 3000);
  };

  const handleCancel = () => {
    setOpen(false);
    dispatch(setCheckList([]));
    dispatch(setIsOpen(false));
  };

  const handleAddPermissionGroup = async () => {
    const listMap = selectList.map((item) => {
      return {
        permissionId: item,
      };
    });

    const request = {
      groupId: groupId,
      permissionIds: listMap,
    };

    if (listMap.length <= 0) {
      notificationController.warning({ message: t('user_group.waring.message1') });
    } else {
      dispatch(createRole(request));
    }
  };

  const convertPermissionGroups = (allPermission: Permission[]): any => {
    return allPermission.map((permission: any, dataIndex: number) => {
      return {
        title: permission.permissionCode,
        key: `${permission.permissionId}`,
      };
    });
  };

  // Call api to get permission group
  useEffect(() => {
    (async () => {
      try {
        const allPermission = await getAllPermission({ currentPage, pageSize });
        const convertedData = convertPermissionGroups(allPermission.data);

        setTreeData(convertedData);
      } catch (error) {}
    })();
  }, [open]);

  return (
    <>
      <Modal
        visible={open}
        title={`${t('group.addrole.title')} ${groupName}`}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <PermissionChecker hasPermissions={['admin_write']}>
            <Button key="back" onClick={handleCancel}>
              {t('group.button.cancel')}
            </Button>
            ,
            <Button loading={loading} onClick={handleAddPermissionGroup}>
              {t('group.button.add_role')}
            </Button>
            ,
          </PermissionChecker>,
        ]}
      >
        <div>
          <RoleTree treeData={treeData} />
        </div>
      </Modal>
    </>
  );
};

export default AddRoleCommon;
