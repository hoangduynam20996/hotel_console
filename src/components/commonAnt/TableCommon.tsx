import React from 'react';
import { Table } from 'antd';
import { useDispatch } from 'react-redux';
import type { TableColumnsType } from 'antd';
import { BaseButton } from '../common/BaseButton/BaseButton';
import { setIsOpen } from '@app/store/slices/groupPermissionSlice';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { getPermissionById } from '@app/store/slices/groupPermissionSlice';
import { useTranslation } from 'react-i18next';
import { doGetPermissionGroups } from '@app/store/slices/permissionGroupSlice';
import PermissionChecker from '../auth/PermissionChecker/PermissionChecker';

interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}

interface AppProps {
  dataSource: DataType[];
  limitPage: number;
  currentPage: number;
  totalPages: number;
  loading: boolean;
}

const TableCommon: React.FC<AppProps> = ({ dataSource, limitPage, currentPage, totalPages, loading }) => {
  const dispatch = useDispatch();

  const isOpen = useAppSelector((state) => state.groupPermission.IsOpen);
  const { t } = useTranslation();
  const handleAddRoleClick = (id: string) => {
    dispatch(getPermissionById(id));
    dispatch(setIsOpen(!isOpen));
  };

  const columns: TableColumnsType<DataType> = [
    {
      title: t('group.table.name'),
      dataIndex: 'name',
      key: 'name',
      render: (text) => <a>{text}</a>,
      width: 350,
    },
    {
      title: t('group.table.description'),
      dataIndex: 'description',
      key: 'description',
      width: 350,
    },
    {
      title: t('group.table.status'),
      dataIndex: 'status',
      key: 'status',
      ellipsis: {
        showTitle: false,
      },
    },
    {
      title: '',
      dataIndex: 'Addrole',
      key: 'Addrole',
      render: (record) => (
        <PermissionChecker hasPermissions={['admin_write']}>
          <BaseButton
            className="h-14"
            type="ghost"
            size="small"
            onClick={() => handleAddRoleClick(record)} // Pass the name to the function
          >
            <span>{t('group.button.add_role')}</span>
          </BaseButton>
        </PermissionChecker>
      ),
    },
  ];

  return (
    <Table
      columns={columns}
      dataSource={dataSource}
      pagination={{
        showSizeChanger: true,
        pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
        defaultPageSize: limitPage,
        defaultCurrent: currentPage,
        total: totalPages * limitPage,
        onChange: (page: number, pageSize: number) => {
          dispatch(doGetPermissionGroups({ currentPage: page, limitPage: pageSize }));
        },
      }}
    />
  );
};

export default TableCommon;
